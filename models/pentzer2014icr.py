from __future__ import annotations

from typing import Dict, NamedTuple

import numpy as np
import pandas as pd
from numpy import linalg
from tqdm import tqdm

import utils as u
from utils import ssmr_utils as ssmru
from utils import ugv_utils as ugvu
from utils import imu_processing as imup

__MODEL_NAME__ = "PENTZER2014ICR"


class PENTZ2014ICR_CONSTANTS:
    "Constants for Pentzer 2014 ICR model"

    # N° Wheels
    N = 4

    def __init__(self, metadata: ugvu.RunMetadata) -> None:
        self.r = metadata.ugv_wr
        self.W = metadata.ugv_wb
        self.L = metadata.ugv_wl

        # Half distances
        self.halfL = self.L / 2
        self.halfW = self.W / 2

    def __str__(self) -> str:
        return str(self.value)


def pentzer2014icr(abbr: str, df: pd.DataFrame | None = None, verbose: bool = True) -> pd.DataFrame:
    """
    Pentzer 2014 :
    Model-based Prediction of Skid-steer Robot Kinematics Using Online Estimation of Track Instantaneous Centers of Rotation
    https://doi.org/10.1002/rob.21509

    SSMR ICR EKF Model
    """
    if df is None:
        df = ugvu.read_df_rename_columns(abbr)

    MODCONST = PENTZ2014ICR_CONSTANTS(df.meta)

    df["vL"] = df.wL * MODCONST.r
    df["vR"] = df.wR * MODCONST.r
    # df["dv"] = df.vR - df.vL

    df["cosp"] = np.cos(df.phi)
    df["sinp"] = np.sin(df.phi)

    # Pentzer 2014 ICR EKF
    uk = df[["vL", "vR"]].copy().to_numpy().T
    # Process noise
    w = np.array([0.1, 0.1, 2 * np.pi / 180, 0.01, 0.01, 0.01])
    Qk = np.diag(np.square(w))
    # Measurement noise
    sigma = np.array([0.01, 0.01, 0.5 * np.pi / 180])
    Rk = np.diag(np.square(sigma))

    # return df

    class IterDat(NamedTuple):
        """Save Iteration data"""

        P: Dict[float, np.array]
        x: Dict[float, np.array]
        K: Dict[float, np.array]
        t: 0

        def results(self) -> pd.DataFrame:
            n = len(self.t) - 1
            x = {idx: np.asarray(v).ravel() for idx, v in self.x.items()}
            P_diag = {idx: v.diagonal() for idx, v in self.P.items()}
            P_diag = {idx: np.asarray(diag).ravel() for idx, diag in P_diag.items()}
            d = {
                "px": pd.Series([x[idx][1] for idx in range(n)]),
                "py": pd.Series([x[idx][0] for idx in range(n)]),
                "phi": pd.Series([x[idx][2] for idx in range(n)]),
                "ICRyL": pd.Series([x[idx][3] for idx in range(n)]),
                "ICRyR": pd.Series([x[idx][4] for idx in range(n)]),
                "ICRx": pd.Series([x[idx][5] for idx in range(n)]),
            }
            return pd.DataFrame(data=d)

    def cov_mat():
        C = 1000000 * np.eye(6)
        # C[1, 1] = 20
        return C

    iterations = IterDat(
        t=[0],
        x={-1: np.array([0, 0, 0, 1, -1, 1])},
        P={-1: cov_mat()},
        K={-1: -1},
    )

    def EKF(i: int, df_data: pd.Series, iteration_data: IterDat):
        P = iteration_data.P[i - 1]
        x = iteration_data.x[i - 1]
        dt = df_data.time - iteration_data.t[-1]

        py, px, phi = x[:3]
        yL, yR, xV = x[3:]

        vL = df_data.vL
        vR = df_data.vR

        # dv : vR - vL
        dV = vR - vL
        # dy = yR - yL
        dy = -np.abs(yR - yL)

        cosp = np.cos(phi)
        sinp = np.sin(phi)

        # TODO: double check with sympy
        # TODO: enforce that dy < 0

        vx = (yR * vL - yL * vR) / dy
        vy = (dV * xV) / dy
        wz = -dV / dy

        # [dvx_dyL, dvx_dyR, dvx_dxV]
        dvx = np.array(
            [
                -yR * dV / (dy**2),
                yL * dV / (dy**2),
                0,
            ]
        )
        # [dvy_dyL, dvy_dyR, dvy_dxV]
        dvy = np.array(
            [
                dV * xV / (dy**2),
                -dV * xV / (dy**2),
                dV / dy,
            ]
        )
        dwz = np.array([-dV / dy**2, dV / dy**2, 0])

        dvxvy = np.vstack([dvx, dvy])
        dNE = dt * np.array([[sinp, cosp], [cosp, -sinp]]) @ dvxvy

        # Fk
        Fk = np.eye(6)
        Fk[:2, 3:] = dNE
        Fk[2, 3:] = dt * dwz
        Fk[0, 2] = dt * (vx * cosp - vy * sinp)
        Fk[1, 2] = -dt * (vx * sinp + vy * cosp)

        # xk
        xpred = x + dt * np.array(
            [
                vx * sinp + vy * cosp,
                vx * cosp - vy * sinp,
                wz,
                0,
                0,
                0,
            ]
        )
        xpred[2] = imup.wrap_to_pi(xpred[2])
        Lk = dt * np.eye(6)
        Ppred = Fk @ P @ Fk.T + Lk @ Qk @ Lk.T

        Hk = np.hstack([np.eye(3), 0 * np.eye(3)])
        Mk = np.eye(3)
        Kk = Ppred @ Hk.T @ linalg.inv(Hk @ Ppred @ Hk.T + Mk @ Rk @ Mk.T)

        # yk
        zpred = xpred[:3]
        yk = np.array(
            [
                df_data.py,
                df_data.px,
                df_data.phi,
            ]
        )
        # yk[2] = imup.wrap_to_pi(yk[2])

        # xk / Pk
        innov = yk - zpred
        innov[2] = imup.wrap_to_pi(innov[2])
        xk = xpred + Kk @ innov
        Pk = (np.eye(6) - Kk @ Hk) @ Ppred

        iteration_data.P[i] = Pk
        iteration_data.x[i] = xk
        iteration_data.K[i] = Kk
        iteration_data.t.append(df_data.time)

    for idx in tqdm(df.index, desc=abbr):
        EKF(idx, df.iloc[idx], iterations)

    icrdf = iterations.results()

    fdf = df.join(icrdf, rsuffix="_ekf")
    fdf.meta = df.meta

    fdf.Bs = fdf.ICRyL - fdf.ICRyR
    fdf.ICRyave = (fdf.ICRyL + fdf.ICRyR) / 2

    # coeffs = fdf[["m_asym", "G_asym", "m_sym", "G_sym"]].copy()
    # model_data = {}
    # model_data["coefficients"] = {
    #     col: {
    #         "min": coeffs[col].min(),
    #         "max": coeffs[col].max(),
    #         "idxmin": str(coeffs[col].argmin()),
    #         "idxmax": str(coeffs[col].argmax()),
    #         "last": coeffs[col].iloc[-1],
    #     }
    #     for col in coeffs.columns.values
    # }
    # model_data["errors"] = {
    #     "casym": ssmru.energy_err(
    #         fdf.energy_Wh,
    #         fdf.Epredc_asym_Wh,
    #     ),
    #     "csym": ssmru.energy_err(
    #         fdf.energy_Wh,
    #         fdf.Epredc_sym_Wh,
    #     ),
    #     "tasym": ssmru.energy_err(
    #         fdf.energy_Wh,
    #         fdf.Epredt_asym_Wh,
    #     ),
    #     "tsym": ssmru.energy_err(
    #         fdf.energy_Wh,
    #         fdf.Epredt_sym_Wh,
    #     ),
    # }

    # ssmru.model_values_to_json(abbr, __MODEL_NAME__, model_data)

    # Export csv
    model_name = __MODEL_NAME__.lower()

    filename = u.get_filename(abbr)
    basename = u.get_basename(filename)
    model_path = u.get_rundir(basename, subdir="models")
    export_csv_path = model_path / f"{basename}_{model_name}.csv"
    fdf.to_csv(export_csv_path, index=False)
    if verbose:
        print(f"{abbr} : Exported {__MODEL_NAME__} CSV in {export_csv_path}")

    fdf.meta.models.append(__MODEL_NAME__)

    return fdf
