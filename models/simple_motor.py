from __future__ import annotations

import numpy as np
import pandas as pd

import utils as u
from utils import electrical_processing as elecp
from utils import ssmr_utils as ssmru
from utils import ugv_utils as ugvu

__MODEL_NAME__ = "SIMPLEMOTOR"


class SIMPLEMOTOR_CONSTANTS:
    "SIMPLEMOTOR constants"

    # Gravity constant
    g = 9.81
    # N° Wheels
    N = 4

    def __init__(self, metadata: ugvu.RunMetadata) -> None:
        self.m = metadata.ugv_mass
        self.r = metadata.ugv_wr
        self.W = metadata.ugv_wb
        self.L = metadata.ugv_wl

        # Half distances
        self.halfL = self.L / 2
        self.halfW = self.W / 2

        # Weight
        self.weight = self.m * self.g

        # Motor model
        self.Kt = metadata.motor_Kt
        self.Ke = metadata.motor_Ke
        self.motL = metadata.motor_L
        self.motR = metadata.motor_R
        self.gear_ratio = metadata.gear_ratio
        self.gear_eta = metadata.gear_eta

    def __str__(self) -> str:
        return str(self.value)

    # Measurement noise in Watt
    measurement_noise = 25
    # EWMA lambda
    EWMlambda = 0.05


def simple_motor(abbr: str, df: pd.DataFrame | None = None, verbose: bool = True) -> pd.DataFrame:
    """
    Simple motor model
    """

    if df is None:
        df = ugvu.read_df_rename_columns(abbr)

    MODCONST = SIMPLEMOTOR_CONSTANTS(df.meta)

    df["v"] = np.sqrt(df.vx**2 + df.vy**2 + df.vz**2)
    df["a"] = np.sqrt(df.ax_corr**2 + df.ay_corr**2 + df.az_corr**2)

    df["vL"] = df.wL * MODCONST.r
    df["vR"] = df.wR * MODCONST.r

    elecp.motion_power(df)

    df["Ploss_L"] = df.PE_L - df.PM_L
    df["Rloss_L"] = df.Ploss_L / (df.I_L) ** 2
    df["Ploss_R"] = df.PE_R - df.PM_R
    df["Rloss_R"] = df.Ploss_R / (df.I_R) ** 2

    df["eta_L"] = df.PM_L / df.PE_L
    df["eta_R"] = df.PM_R / df.PE_R

    df["P_mot"] = df.P_motion
    df["P_log"] = df.P_logical

    df["dI_L"] = df.I_L.diff().fillna(0)
    df["dI_R"] = df.I_R.diff().fillna(0)
    df["dt"] = df.time.diff()
    df.dt.iloc[0] = df.dt.iloc[1]

    df["dILdt"] = df.dI_L / df.dt
    df["dIRdt"] = df.dI_R / df.dt

    df["wmotL"] = df.wL / MODCONST.gear_ratio
    df["wmotR"] = df.wR / MODCONST.gear_ratio

    df["U_L"] = MODCONST.Ke * df.wmotL + MODCONST.motR * df.I_L + MODCONST.motL * df.dILdt
    df["U_R"] = MODCONST.Ke * df.wmotR + MODCONST.motR * df.I_R + MODCONST.motL * df.dIRdt

    fdf = df.copy()
    fdf.meta = df.meta

    # Export csv
    fdf.meta.models.append(__MODEL_NAME__)
    model_name = "-".join([mn.lower() for mn in fdf.meta.models])

    filename = u.get_filename(abbr)
    basename = u.get_basename(filename)
    model_path = u.get_rundir(basename, subdir="models")
    export_csv_path = model_path / f"{basename}_{model_name}.csv"
    fdf.to_csv(export_csv_path, index=False)
    if verbose:
        print(f"{abbr} : Exported {__MODEL_NAME__} CSV in {export_csv_path}")

    return fdf
