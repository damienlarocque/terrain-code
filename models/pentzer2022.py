from __future__ import annotations

from typing import Dict, NamedTuple, Tuple

import numpy as np
import pandas as pd
from scipy.integrate import cumulative_trapezoid
from tqdm import tqdm

import utils as u
from utils import electrical_processing as elecp
from utils import ssmr_utils as ssmru
from utils import ugv_utils as ugvu

__MODEL_NAME__ = "PENTZER2022POW"


class PENTZ2022_CONSTANTS:
    "SSMR constants for Pentzer 2022"

    # Gravity constant
    g = 9.81
    # N° Wheels
    N = 4

    MEASUREMENT_NOISES = {
        "warthog": 50,
        "husky": 25,
    }

    def __init__(self, metadata: ugvu.RunMetadata) -> None:
        self.m = metadata.ugv_mass
        self.r = metadata.ugv_wr
        self.W = metadata.ugv_wb
        self.L = metadata.ugv_wl

        # Half distances
        self.halfL = self.L / 2
        self.halfW = self.W / 2

        # Weight
        self.weight = self.m * self.g

        # UGV name
        self.name = metadata.ugv_name

        # Motor model
        self.Kt = metadata.motor_Kt
        self.gear_ratio = metadata.gear_ratio

        # Measurement noise in Watt
        self.measurement_noise = self.MEASUREMENT_NOISES[self.name]
        self.error_thresh = 2 * self.measurement_noise

    def __str__(self) -> str:
        return str(self.value)

    # EWMA lambda
    EWMlambda = 0.05


def pentzer2022(
    abbr: str,
    df: pd.DataFrame | None = None,
    compute_errors: bool = True,
    detect_terrains: bool = False,
    verbose: bool = True,
) -> pd.DataFrame:
    """
    Pentzer 2022 :
    On-line estimation of power model parameters for skid-steer robots with applications in mission energy use prediction
    https://doi.org/10.1002/rob.22076

    SSMR Power Model with 3 converged coefficients (mu, G, beta)
    """

    if df is None:
        df = ugvu.read_df_rename_columns(abbr)

    MODCONST = PENTZ2022_CONSTANTS(df.meta)

    df["v"] = np.sqrt(df.vx**2 + df.vy**2 + df.vz**2)
    df["a"] = np.sqrt(df.ax_corr**2 + df.ay_corr**2 + df.az_corr**2)

    df["vL"] = df.wL * MODCONST.r
    df["vR"] = df.wR * MODCONST.r

    elecp.motion_power(df)

    df = ssmru.naive_icr(df)

    time_a = df.head(1).time.item()
    time_b = df.tail(1).time.item()
    df["time_prop"] = (df.time - time_a) / (time_b - time_a)
    df["time_rel"] = df.time - time_a
    rel_time = df.time_rel

    # df["normal_force"] = SSMR_CONST.weight * np.cos(df.theta) / SSMR_CONST.N
    df["normal_force"] = MODCONST.weight * np.cos(df.theta) / MODCONST.N

    df["dx_front"] = MODCONST.halfL - df.ICRx
    df["dx_rear"] = MODCONST.halfL + df.ICRx
    df["dy_left"] = MODCONST.halfW - df.ICRyL
    df["dy_right"] = MODCONST.halfW + df.ICRyR

    # Distance
    # * front-left
    # * front-right
    # * rear-left
    # * rear-right
    df["d_fl"] = np.sqrt(df.dx_front**2 + df.dy_left**2)
    df["d_fr"] = np.sqrt(df.dx_front**2 + df.dy_right**2)
    df["d_rl"] = np.sqrt(df.dx_rear**2 + df.dy_left**2)
    df["d_rr"] = np.sqrt(df.dx_rear**2 + df.dy_right**2)

    df["dist_asym"] = df.d_fl + df.d_fr + df.d_rl + df.d_rr
    df["dist_sym"] = np.sqrt(MODCONST.L**2 + (MODCONST.W - df.Bs) ** 2)

    df["JS_asym"] = df.wz.abs() * df.normal_force * df.dist_asym
    df["JS_sym"] = 2 * df.wz.abs() * df.normal_force * df.dist_sym

    df["JG"] = df.vL.abs() + df.vR.abs()
    df["JB"] = MODCONST.weight * np.sin(df.theta) * df.vx

    # Pentzer 2022 RLS
    H_sym = df[["JS_sym", "JG", "JB"]].copy().to_numpy()
    H_asym = df[["JS_asym", "JG", "JB"]].copy().to_numpy()

    y_meas = df.P_motion.copy()

    class IterDat(NamedTuple):
        """Save Iteration data"""

        mode: str
        P: Dict[float, np.array]
        x: Dict[float, np.array]
        H: np.array
        K: Dict[float, np.array]

        def results(self) -> pd.DataFrame:
            n = self.H.shape[0]
            x = {idx: np.asarray(v).ravel() for idx, v in self.x.items()}
            P_diag = {idx: v.diagonal() for idx, v in self.P.items()}
            P_diag = {idx: np.asarray(diag).ravel() for idx, diag in P_diag.items()}
            K = {idx: np.asarray(v).ravel() for idx, v in self.K.items()}
            d = {
                "m": pd.Series([x[idx][0] for idx in range(n)]),
                "G": pd.Series([x[idx][1] for idx in range(n)]),
                "B": pd.Series([x[idx][2] for idx in range(n)]),
                "sm": pd.Series([P_diag[idx][0] for idx in range(n)]),
                "sG": pd.Series([P_diag[idx][1] for idx in range(n)]),
                "sB": pd.Series([P_diag[idx][2] for idx in range(n)]),
                "Km": pd.Series([K[idx][0] for idx in range(n)]),
                "KG": pd.Series([K[idx][1] for idx in range(n)]),
                "KB": pd.Series([K[idx][2] for idx in range(n)]),
            }
            return pd.DataFrame(data=d)

        def reset(self) -> Tuple[np.array]:
            """Get initial values

            Returns:
                Tuple[np.array]: Tuple of initial values for x, P
            """
            return (
                self.x[-1],
                self.P[-1],
            )

    def cov_mat(cov: float | None = 100):
        C = cov * np.eye(3)
        C[1, 1] = 20
        return C

    if MODCONST.name == "warthog":
        sym_iters = IterDat(
            mode="sym",
            P={-1: cov_mat()},
            x={-1: np.matrix([0.5, 150, 1]).T},
            H=H_sym,
            K={-1: -1},
        )
        asym_iters = IterDat(
            mode="asym",
            P={-1: cov_mat()},
            x={-1: np.matrix([0.5, 150, 1]).T},
            H=H_asym,
            K={-1: -1},
        )
    elif MODCONST.name == "husky":
        sym_iters = IterDat(
            mode="sym",
            P={-1: cov_mat()},
            x={-1: np.matrix([0.4, 50, 1]).T},
            H=H_sym,
            K={-1: -1},
        )
        asym_iters = IterDat(
            mode="asym",
            P={-1: cov_mat()},
            x={-1: np.matrix([0.4, 50, 1]).T},
            H=H_asym,
            K={-1: -1},
        )

    # x_sym = x_asym = np.array([[0.5, 10, 1]]).T
    # P_sym = P_asym = 100 * np.eye(3)

    def rls_estimator(i: int, iteration_data: IterDat):
        H = iteration_data.H
        P = iteration_data.P[i - 1]
        x = iteration_data.x[i - 1]
        rk = MODCONST.measurement_noise

        Hk = H[i, np.newaxis]
        yk = y_meas.iloc[i]

        if detect_terrains:
            perr = yk - Hk @ x
            if np.abs(perr) > MODCONST.error_thresh:
                x, P = iteration_data.reset()

        # Kk
        Kk = P @ Hk.T * (Hk @ P @ Hk.T + rk) ** -1

        # Pk
        IKH = np.eye(3) - Kk * Hk
        Pk = IKH @ P @ IKH.T + Kk @ np.array([[rk]]) @ Kk.T

        # xk
        xk = x + Kk @ (yk - Hk @ x)

        iteration_data.P[i] = Pk
        iteration_data.x[i] = xk
        iteration_data.K[i] = Kk

    for idx in tqdm(df.index, desc=abbr, colour="magenta"):
        rls_estimator(idx, sym_iters)
        rls_estimator(idx, asym_iters)

    sym_df = sym_iters.results()
    asym_df = asym_iters.results()

    fdf = sym_df.join(
        asym_df,
        lsuffix="_sym",
        rsuffix="_asym",
    )
    fdf = df.join(fdf)
    fdf.meta = df.meta

    if not compute_errors:
        fdf.meta.models.append(__MODEL_NAME__)
        return fdf

    coeffs = fdf[["m_asym", "G_asym", "B_asym", "m_sym", "G_sym", "B_sym"]].copy()
    model_data = {}
    model_data["coefficients"] = {
        col: {
            "min": coeffs[col].min(),
            "max": coeffs[col].max(),
            "idxmin": str(coeffs[col].argmin()),
            "idxmax": str(coeffs[col].argmax()),
            "last": coeffs[col].dropna().iloc[-1],
        }
        for col in coeffs.columns.values
    }

    # Predict power and compute power error
    # df[["JS_sym", "JG", "JB"]]
    # df[["JS_asym", "JG", "JB"]]
    for m in ("sym", "asym"):
        PS = fdf[f"m_{m}"] * fdf[f"JS_{m}"]
        PG = fdf[f"G_{m}"] * fdf.JG
        fdf[f"Ppredt_{m}"] = PS + PG
        # Then
        fdf[f"Perrt_{m}"] = fdf.P_motion - fdf[f"Ppredt_{m}"]
        fdf[f"EWMAt_{m}"] = fdf[f"Perrt_{m}"].ewm(alpha=MODCONST.EWMlambda).mean()
        fdf[f"estt_{m}"] = fdf[f"EWMAt_{m}"].cumsum()

        PS = fdf[f"m_{m}"].dropna().iloc[-10:].mean() * fdf[f"JS_{m}"]
        PG = fdf[f"G_{m}"].dropna().iloc[-10:].mean() * fdf.JG
        fdf[f"Ppredc_{m}"] = PS + PG
        # Then
        fdf[f"Perrc_{m}"] = fdf.P_motion - fdf[f"Ppredc_{m}"]
        fdf[f"EWMAc_{m}"] = fdf[f"Perrc_{m}"].ewm(alpha=MODCONST.EWMlambda).mean()
        fdf[f"estc_{m}"] = fdf[f"EWMAc_{m}"].cumsum()

    # Energy
    fdf["energy_Wh"] = cumulative_trapezoid(fdf.P_motion, rel_time, initial=0) / 3600
    fdf["Epredt_sym_Wh"] = cumulative_trapezoid(fdf.Ppredt_sym, rel_time, initial=0) / 3600
    fdf["Epredt_asym_Wh"] = cumulative_trapezoid(fdf.Ppredt_asym, rel_time, initial=0) / 3600
    fdf["Epredc_sym_Wh"] = cumulative_trapezoid(fdf.Ppredc_sym, rel_time, initial=0) / 3600
    fdf["Epredc_asym_Wh"] = cumulative_trapezoid(fdf.Ppredc_asym, rel_time, initial=0) / 3600

    fdf["pcerr_tsym"] = (fdf.Epredt_sym_Wh - fdf.energy_Wh) / fdf.energy_Wh
    fdf["pcerr_tasym"] = (fdf.Epredt_asym_Wh - fdf.energy_Wh) / fdf.energy_Wh
    fdf["pcerr_csym"] = (fdf.Epredc_sym_Wh - fdf.energy_Wh) / fdf.energy_Wh
    fdf["pcerr_casym"] = (fdf.Epredc_asym_Wh - fdf.energy_Wh) / fdf.energy_Wh

    model_data["errors"] = {
        "casym": ssmru.energy_err(
            fdf.energy_Wh,
            fdf.Epredc_asym_Wh,
        ),
        "csym": ssmru.energy_err(
            fdf.energy_Wh,
            fdf.Epredc_sym_Wh,
        ),
        "tasym": ssmru.energy_err(
            fdf.energy_Wh,
            fdf.Epredt_asym_Wh,
        ),
        "tsym": ssmru.energy_err(
            fdf.energy_Wh,
            fdf.Epredt_sym_Wh,
        ),
    }

    ssmru.model_values_to_json(abbr, __MODEL_NAME__, model_data)

    # Export csv
    fdf.meta.models.append(__MODEL_NAME__)

    model_name = "-".join([mn.lower() for mn in fdf.meta.models])

    filename = u.get_filename(abbr)
    basename = u.get_basename(filename)
    model_path = u.get_rundir(basename, subdir="models")
    export_csv_path = model_path / f"{basename}_{model_name}.csv"
    fdf.to_csv(export_csv_path, index=False)
    if verbose:
        print(f"{abbr} : Exported {__MODEL_NAME__} CSV in {export_csv_path}")

    return fdf


def open_model_df(abbr: str) -> pd.DataFrame:
    filename = u.get_filename(abbr)
    basename = u.get_basename(filename)
    model_path = u.get_rundir(basename, subdir="models")
    model_name = __MODEL_NAME__.lower()
    export_csv_path = model_path / f"{basename}_{model_name}.csv"
    fdf = pd.read_csv(export_csv_path)

    ugv_name = ugvu.get_ugv(abbr)
    fdf.meta = ugvu.RunMetadata()
    fdf.meta.ugv_name = ugv_name.lower()

    fdf = ugvu.add_ugv_metadata(fdf, ugv_name)

    return fdf


def naive_ssmrk(abbr: str):
    """(Obsolete) Naive computation of SSMRK parameters, uses Least Squares Correlation, but doesn't converge on some data"""

    raise NotImplementedError("Don't use this function")

    df = open_and_prepare_ssmrk_df(abbr)

    filename = u.get_filename(abbr)
    basename = u.get_basename(filename)
    rundir = u.get_rundir(basename)

    df["P_total"] = df.current * df.voltage
    df["v"] = np.sqrt(df.vx**2 + df.vy**2 + df.vz**2)
    df["a"] = np.sqrt(df.ax_corr**2 + df.ay_corr**2 + df.az_corr**2)

    P_Logical = get_logical_power(basename)
    df["P_motion"] = df.P_total - P_Logical

    # df["P_L"] = df.I_L * df.V_L
    # df["P_R"] = df.I_R * df.V_R

    df["ICRx"] = -df.vy / df.w_z
    df["ICRy"] = df.vx / df.w_z

    df["vL"] = df.wL * SSMR_CONST.r
    df["vR"] = df.wR * SSMR_CONST.r

    df["ICRyL"] = (df.vx - df.vL) / df.w_z
    df["ICRyR"] = (df.vx - df.vR) / df.w_z

    # df["normal_force"] = SSMR_CONST.weight * np.cos(df.theta) / SSMR_CONST.N
    df["normal_force"] = SSMR_CONST.weight * np.cos(df.theta) / SSMR_CONST.N

    df["Bs"] = (df.ICRyL - df.ICRyR).abs()

    df["dx_front"] = SSMR_CONST.halfL - df.ICRx
    df["dx_rear"] = SSMR_CONST.halfL + df.ICRx
    df["dy_left"] = SSMR_CONST.halfW - df.ICRyL
    df["dy_right"] = SSMR_CONST.halfW + df.ICRyR

    # Distance
    # * front-left
    # * front-right
    # * rear-left
    # * rear-right
    df["d_fl"] = np.sqrt(df.dx_front**2 + df.dy_left**2)
    df["d_fr"] = np.sqrt(df.dx_front**2 + df.dy_right**2)
    df["d_rl"] = np.sqrt(df.dx_rear**2 + df.dy_left**2)
    df["d_rr"] = np.sqrt(df.dx_rear**2 + df.dy_right**2)

    df["dist_asym"] = df.d_fl + df.d_fr + df.d_rl + df.d_rr
    df["dist_sym"] = np.sqrt(SSMR_CONST.L**2 + (SSMR_CONST.W - df.Bs) ** 2)

    df["JS_asym"] = df.w_z.abs() * df.normal_force * df.dist_asym
    df["JS_sym"] = 2 * df.w_z.abs() * df.normal_force * df.dist_sym

    df["JG"] = df.vL.abs() + df.vR.abs()
    df["JB"] = SSMR_CONST.weight * np.sin(df.theta) * df.vx

    Jasym_arr = df[["JS_asym", "JG", "JB"]].copy().to_numpy()
    Jsym_arr = df[["JS_sym", "JG", "JB"]].copy().to_numpy()

    P_Motion = df.P_motion.copy().to_numpy()

    # Jasym_arr = np.vstack([np.zeros((3,)), Jasym_arr, np.zeros((3,))])
    # Jsym_arr = np.vstack([np.zeros((3,)), Jsym_arr, np.zeros((3,))])
    # P_Motion = np.concatenate([[0], P_Motion, [0]])

    def solve_params(idx: int):
        A_asym = Jasym_arr[: idx + 1, :]
        A_sym = Jsym_arr[: idx + 1, :]
        B = P_Motion[: idx + 1]

        def try_solve(A_mat, B_mat):
            # try:
            #     X = np.linalg.solve(A_mat, B_mat)
            # except np.linalg.LinAlgError:
            #     X = np.linalg.lstsq(A_mat, B_mat, rcond=None)[0]
            # return X
            X, r, rk, s = np.linalg.lstsq(A_mat, B_mat, rcond=None)
            return X

        x_asym = try_solve(A_asym, B)
        x_sym = try_solve(A_sym, B)

        d_asym = dict(
            zip(
                ("mu_asym", "G_asym", "B_asym"),
                x_asym,
            )
        )
        d_sym = dict(
            zip(
                ("mu_sym", "G_sym", "B_sym"),
                x_sym,
            )
        )

        return {**d_asym, **d_sym}

    df["ssmr_coeffs"] = pd.Series({idx: solve_params(idx) for idx in df.index})
    coeffs_df = pd.DataFrame(df.ssmr_coeffs.to_dict()).T

    ssmrk_df = df.join(coeffs_df).drop(columns=["ssmr_coeffs"])
    ssmru.ssmrcoeffs_to_json(abbr, ssmrk_df)

    # Export csv
    merged_path = rundir / "ssmrk"
    ssmrk_csv_path = merged_path / f"{basename}_ssmrk.csv"
    ssmrk_df.to_csv(ssmrk_csv_path, index=False)
    print(f"{abbr} : Exported SSMR kinematics CSV in {ssmrk_csv_path}")

    return ssmrk_df
