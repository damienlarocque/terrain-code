from __future__ import annotations

from typing import TYPE_CHECKING

from sklearn.neural_network import MLPClassifier, MLPRegressor

from models.ML.base import TerrainClassifier, TerrainRegressor

if TYPE_CHECKING:
    from typing import Sequence


class TerrainMLPRegressor(TerrainRegressor):
    """Using Multi-Layer Perception Regressors to predict Terrain types based on their power prediction errors"""

    _model_name = "MLP Regressor"
    _type = MLPRegressor

    def __init__(
        self,
        terrains: Sequence[str],
        verbose: bool = False,
        ugv_name: str = "husky",
        **model_args,
    ) -> None:
        """Instantiate multiple models, based on the number of terrains

        Args:
            terrains (Sequence[str]): Sequence of terrain classes.
            verbose (bool | None, optional): Verbose. Defaults to False.
            ugv_name (str, optional): UGV Name. Defaults to "husky".
            max_iters (int | None, optional): Number of iterations per model. Defaults to 500.
            model_args: Refer to https://scikit-learn.org/stable/modules/generated/sklearn.neural_network.MLPRegressor.html.
        """
        super().__init__(
            terrains=terrains,
            model_name=self._model_name,
            ugv_name=ugv_name,
        )
        self.verbose = verbose
        self.ugv_name = ugv_name
        self._models = [
            MLPRegressor(
                verbose=self.verbose,
                **model_args,
            )
            for _ in self.terrains
        ]

    # def show_error_flock(
    #     self,
    #     X_test: pd.DataFrame,
    #     y_test: pd.DataFrame,
    #     mode: Literal["abs", "rel"] = "abs",
    # ) -> plt.Figure:
    #     """Show power prediction error based on the real power measurements

    #     Args:
    #         X_test (pd.DataFrame): Test dataset X
    #         y_test (pd.DataFrame): Test dataset y
    #         mode (Literal["abs", "rel"]): Power estimation mode.
    #             - abs for absolute power estimation error (predict - real)
    #             - rel for relative power estimation error (predict - real)/real

    #     Returns:
    #         pd.DataFrame: Power estimation error (predict - real) for all terrains

    #     """
    #     pred_proba = self.predict_proba(X_test, y_test.P_motion)

    #     pass


class TerrainMLPClassifier(MLPClassifier, TerrainClassifier):
    _model_name = "MLP Classifier"

    def __init__(self, ugv_name: str = "husky", **kwargs) -> None:
        super().__init__(**kwargs)
        self.ugv_name = ugv_name
