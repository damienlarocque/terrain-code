from __future__ import annotations

from typing import TYPE_CHECKING

from imblearn.ensemble import BalancedRandomForestClassifier
from sklearn.ensemble import RandomForestClassifier, RandomForestRegressor

from models.ML.base import TerrainClassifier, TerrainRegressor

if TYPE_CHECKING:
    from typing import Sequence


class TerrainRFRegressor(TerrainRegressor):
    """Using Random Forest Regressors to predict Terrain types based on their power prediction errors"""

    _model_name = "RF Regressor"
    _type = RandomForestRegressor

    def __init__(
        self,
        terrains: Sequence[str],
        verbose: bool = False,
        ugv_name: str = "husky",
        **model_args,
    ) -> None:
        """Instantiate multiple models, based on the number of terrains

        Args:
            terrains (Sequence[str]): Sequence of terrain classes.
            verbose (bool | None, optional): Verbose. Defaults to False.
            ugv_name (str, optional): UGV Name. Defaults to "husky".
            model_args: Refer to https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestRegressor.html.
        """
        super().__init__(
            terrains=terrains,
            model_name=self._model_name,
            ugv_name=ugv_name,
        )
        self.verbose = verbose
        self._models = [
            RandomForestRegressor(
                verbose=self.verbose,
                **model_args,
            )
            for _ in self.terrains
        ]

    # def show_error_flock(
    #     self,
    #     X_test: pd.DataFrame,
    #     y_test: pd.DataFrame,
    #     mode: Literal["abs", "rel"] = "abs",
    # ) -> plt.Figure:
    #     """Show power prediction error based on the real power measurements

    #     Args:
    #         X_test (pd.DataFrame): Test dataset X
    #         y_test (pd.DataFrame): Test dataset y
    #         mode (Literal["abs", "rel"]): Power estimation mode.
    #             - abs for absolute power estimation error (predict - real)
    #             - rel for relative power estimation error (predict - real)/real

    #     Returns:
    #         pd.DataFrame: Power estimation error (predict - real) for all terrains

    #     """
    #     pred_proba = self.predict_proba(X_test, y_test.P_motion)

    #     pass


class TerrainRFClassifier(RandomForestClassifier, TerrainClassifier):
    _model_name = "RF Classifier"

    def __init__(
        self,
        ugv_name: str = "husky",
    ) -> None:
        super().__init__()
        self.ugv_name = ugv_name


class BalancedTerrainRFClassifier(BalancedRandomForestClassifier, TerrainClassifier):
    _model_name = "Balanced RF Classifier"

    def __init__(
        self,
        ugv_name: str = "husky",
    ) -> None:
        super().__init__()
        self.ugv_name = ugv_name
