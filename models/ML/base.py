from __future__ import annotations

import json
from pathlib import Path
from typing import TYPE_CHECKING

import joblib
import numpy as np
import pandas as pd
from sklearn.metrics import (
    ConfusionMatrixDisplay,
    accuracy_score,
    confusion_matrix,
    f1_score,
    precision_score,
    r2_score,
    recall_score,
)
from sklearn.model_selection import cross_val_score
from tqdm import tqdm

import utils as u

if TYPE_CHECKING:
    from typing import Dict, List, Literal, Sequence, Type

    import numpy.typing as npt
    from sklearn.pipeline import Pipeline

    ClassLabel = pd.Series | np.ndarray | npt.ArrayLike | Sequence[str]


class TerrainRegressor:
    _model_name: str
    _type: Type[object]
    disp: ConfusionMatrixDisplay

    def __init__(
        self,
        terrains: Sequence[str],
        model_name: str,
        ugv_name: str = "husky",
    ) -> None:
        """Instantiate Regressor instance, based on the number of terrains

        Args:
            terrains (Sequence[str]): Sequence of terrain classes.
            model_name (str): Display model name, in title case (Model Name)
            ugv_name (str, optional): UGV Name. Defaults to "husky".
            model_args: Refer to https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestRegressor.html.
        """
        self._models: List[Pipeline]
        self.terrains = terrains
        self.ugv_name = ugv_name
        self._model_name = model_name

    @classmethod
    def from_models(cls, terrain_models: Dict[str, TerrainRegressor], **kwargs) -> TerrainRegressor:
        """Create an instance from trained_models dictionary

        Args:
            terrain_models (Dict[str, RandomForestRegressor]): Trained models

        Returns:
            TerrainRegressor: Instance of TerrainRegressor
        """
        terr_mdls = dict(sorted(terrain_models.items()))
        # Name of terrains
        regressor = cls(terrains=list(terr_mdls.keys()), **kwargs)
        # Assert that each model is of the right type
        models = list(terr_mdls.values())
        is_model = [isinstance(m, cls._type) for m in models]
        if not all(is_model):
            mtypes = [type(m) for m in models]
            raise TypeError(f"Wrong model type. [Expected {cls._type}; got {*mtypes,}]")
        regressor.models = models
        return regressor

    @classmethod
    def from_files(
        cls,
        model_dir: Path | str,
        **kwargs,
    ) -> TerrainRegressor:
        """Create an instance from model directory

        Args:
            model_dir (Path | str): Directory to model files

        Returns:
            TerrainRegressor: Instance of TerrainRegressor
        """
        model_dir = Path(model_dir)
        if not model_dir.exists() and model_dir.is_dir():
            raise FileNotFoundError(f"Path {model_dir} does not lead to an existing directory")
        mdl_files = [f.resolve() for f in model_dir.glob("*.mdl")]
        terrain_models = {}
        if not len(mdl_files) > 0:
            raise FileNotFoundError(f"No .mdl files in directory '{model_dir}'")
        for f in mdl_files:
            label = u.unslugify(f.stem)
            terrain_models[label] = joblib.load(f)
        print(f"Found topics {', '.join(terrain_models.keys())}")

        return cls.from_models(terrain_models, **kwargs)

    def len_classes(self) -> int:
        return len(self.terrains)

    # @property
    # def terrains(self):
    #     """The terrains property."""
    #     return self.terrains

    @property
    def models(self):
        """The models property."""
        return self._models

    @property
    def params(self) -> dict:
        m_params = [m.get_params() for m in self._models]
        m_check = m_params.count(m_params[0])
        # Number of models that differ from first parameters
        n_different = len(m_params) - m_check
        if n_different > 0:
            # Some models have different parameters
            raise ValueError("Models are not consistent. " f"{n_different} models are not consistent.")
        return m_params[0]

    @classmethod
    def filename(cls) -> str:
        return u.slugify(
            cls._model_name.lower(),
            sep="-",
        )

    @classmethod
    def dataname(cls) -> str:
        return "-".join(cls._model_name.lower().split()[::-1])

    @classmethod
    def model_name(cls) -> str:
        return u.slugify(
            cls._model_name.upper(),
            sep="",
        )

    @classmethod
    def display_name(cls) -> str:
        return cls._model_name

    def metrics_name(self) -> str:
        """Name for metrics export"""
        n = self.len_classes()
        n_feat = self.models[0].n_features_in_
        return f"{self.dataname()}-{n:02}c-{n_feat:02}f"

    @models.setter
    def models(self, value):
        if isinstance(value, list):
            self._models = value
        else:
            raise AttributeError(f"Cannot set models with type '{type(value)}'")

    def get_params(self, deep: bool = False) -> dict:
        """Get model params"""
        params = self.params
        params["terrains"] = self.terrains
        return params

    def fit(self, X: pd.DataFrame, y: pd.DataFrame):
        """
        Args:
            y : 2-columns DataFrame where
            column 'P_motion' : motion power measurements (float)
            column 'terrain' : terrain names (string)
        """
        X_train, y_train = X.copy(), y.copy()

        y_power = y_train.P_motion
        y_terrain = y_train.terrain

        pbar = tqdm(self._models)
        for idx, rgr in enumerate(pbar):
            terr = self.terrains[idx]
            pbar.set_description(terr.title())
            y_terr = y_power[y_terrain == terr]
            X_terr = X_train.loc[y_terr.index]
            rgr.fit(X_terr.to_numpy(), y_terr.to_numpy())

        return self

    def predict_idx(self, X: pd.DataFrame, y_power: pd.Series) -> np.ndarray:
        """Predict class index for every X

        Args:
            X (pd.DataFrame): values of X
            y_power (pd.Series): vector of power values

        Returns:
            np.ndarray: Vector of class indices
        """
        X_test = X.copy()
        meas_power = y_power.copy().to_numpy()

        rgrs = self._models
        pred_power = [rgr.predict(X_test.to_numpy()) for rgr in rgrs]
        pred_power = np.vstack(pred_power).T

        pred_err = np.abs(pred_power - meas_power[:, np.newaxis])
        best_clf_idx = pred_err.argmin(axis=1)

        return best_clf_idx

    def predict(self, X: pd.DataFrame, y_power: pd.Series) -> np.ndarray:
        """Predict class label for every X

        Args:
            X (pd.DataFrame): values of X
            y_power (pd.Series): vector of power values

        Returns:
            np.ndarray: Vector of class labels
        """
        idx_pred = self.predict_idx(X, y_power)
        return np.array(sorted(self.terrains))[idx_pred]

    def predict_proba(
        self,
        X: pd.DataFrame,
        y_power: pd.Series,
    ) -> pd.DataFrame:
        """Get power estimation error for each terrain regressor

        Args:
            X (np.ndarray): Test dataset X
            y_power (pd.Series): Power values for the test dataset

        Returns:
            pd.DataFrame: Power estimation error (predict - real) for all terrains
        """
        X_test = X.copy()
        meas_power = y_power.copy().to_numpy()

        rgrs = self._models
        pred_power = [rgr.predict(X_test.to_numpy()) for rgr in rgrs]
        pred_power = np.vstack(pred_power).T

        pred_err = pred_power - meas_power[:, np.newaxis]
        errors = pd.DataFrame(data=pred_err, columns=self.terrains)
        return errors

    def predict_power(
        self,
        X: pd.DataFrame,
    ) -> pd.DataFrame:
        """Get power estimations from each terrain regressor

        Args:
            X (np.ndarray): Test dataset X

        Returns:
            pd.DataFrame: Power estimations for all regressors
        """
        X_test = X.copy()

        rgrs = self._models
        pred_power = [rgr.predict(X_test.to_numpy()) for rgr in rgrs]
        pred_power = np.vstack(pred_power).T
        power = pd.DataFrame(data=pred_power, columns=self.terrains)
        return power

    def APE(
        self,
        X: pd.DataFrame,
        y: pd.DataFrame,
    ) -> pd.DataFrame:
        """
        y : 1-column DataFrame where
        column 'P_motion' : motion power measurements (float)
        """
        X, y = X.copy(), y.copy().reset_index(drop=True)

        y_power = y.P_motion

        power_pred = self.predict_power(X)
        ape = power_pred.apply(lambda s: (s - y_power).abs() / y_power)
        return ape

    def score(
        self,
        X: pd.DataFrame,
        y: pd.DataFrame,
    ) -> float:
        """
        y : 2-columns DataFrame where
        column 'P_motion' : motion power measurements (float)
        column 'terrain' : terrain names (string)
        """
        X, y = X.copy(), y.copy()

        y_power = y.P_motion
        y_terrain = y.terrain

        y_pred = self.predict_idx(X, y_power)
        y_true = np.array([self.terrains.index(a) for a in y_terrain])

        N = y_power.size
        correct = np.equal(y_true, y_pred)
        n_correct = np.count_nonzero(correct)
        return n_correct / N

    def metrics(
        self,
        y_true: ClassLabel,
        y_pred: ClassLabel,
        json_export: Path | str | None = None,
    ) -> pd.Series:
        """Estimate classification metrics

        Args:
            y_true (pd.Series | np.ndarray): True classes
            y_pred (pd.Series | np.ndarray): Predicted classes

        Returns:
            pd.Series: Classification metrics
        """
        metrics = pd.Series(
            {
                "accuracy": accuracy_score(
                    y_true,
                    y_pred,
                ),
                "precision": precision_score(
                    y_true,
                    y_pred,
                    labels=self.terrains,
                    average=None,
                ),
                "recall": recall_score(
                    y_true,
                    y_pred,
                    labels=self.terrains,
                    average=None,
                ),
                "f1-score": f1_score(
                    y_true,
                    y_pred,
                    labels=self.terrains,
                    average=None,
                ),
            }
        )

        if json_export is not None:
            outpath = Path(json_export)
        else:
            outpath = Path.cwd() / "metrics_ML.json"

        print(f"Exporting metrics to {outpath.resolve()}")

        exp_metrics = json.loads(metrics.to_json())
        exp_metrics["params"] = self.params
        exp_metrics["terrains"] = self.terrains

        with u.JSONExporter(outpath) as data:
            data[self.metrics_name()] = exp_metrics

        return metrics

    def power_metrics(
        self,
        X: pd.DataFrame,
        y: pd.DataFrame,
        json_export: Path | str | None = None,
    ) -> pd.Series:
        """
        y : 2-columns DataFrame where
        column 'P_motion' : motion power measurements (float)
        column 'terrain' : terrain names (string)
        """
        X, y = X.copy(), y.copy().reset_index(drop=True)

        y_power = y.P_motion
        y_terrain = y.terrain

        power_pred = self.predict_power(X)
        exp_metrics = {}

        for terr in power_pred.columns.values:
            terr_idx = y_terrain == terr
            P_pred = power_pred[terr][terr_idx]
            P_meas = y_power[terr_idx]
            exp_metrics[terr] = r2_score(P_pred, P_meas)

        if json_export is not None:
            outpath = Path(json_export)
        else:
            outpath = Path.cwd() / "metrics_ML.json"

        print(f"Exporting metrics to {outpath.resolve()}")

        with u.JSONExporter(outpath) as data:
            data[self.metrics_name()]["r2-score"] = exp_metrics

        return pd.Series(exp_metrics)

    def confusion_matrix(
        self,
        y_true: ClassLabel,
        y_pred: ClassLabel,
        figdir: Path | str | None = None,
        plot: bool = True,
        normalization: Literal["true", "pred", "all"] | None = None,
    ) -> ConfusionMatrixDisplay | np.ndarray:
        """Show confusion matrix

        Args:
            y_true (ClassLabel): y_true
            y_pred (ClassLabel): Predicted values of y
            figdir (Path | str | None, optional): Directory to save the confusion matrix figure. Defaults to None.
            plot (bool, optional): Flag to indicate whether to return the confusion matrix as a numpy array or a figure. Defaults to True.
            normalization (Literal['true', 'pred', 'all'] | None, optional): Normalization method. Defaults to None.

        Raises:
            FileNotFoundError: Raised if figdir is not an existing directory

        Returns:
            ConfusionMatrixDisplay | np.ndarray: Confusion matrix, be it a numpy array or a display
        """
        terrains = self.terrains
        cm = confusion_matrix(
            y_true,
            y_pred,
            labels=terrains,
            normalize=normalization,
        )

        if not plot:
            return cm

        disp = ConfusionMatrixDisplay.from_predictions(
            y_true,
            y_pred,
            labels=terrains,
            normalize=normalization,
            display_labels=terrains,
        )
        disp.ax_.set_title(f"Confusion matrix for {self._model_name}")
        self.disp = disp

        if figdir is not None:
            dirpath = Path(figdir)
            if not dirpath.exists() or not dirpath.is_dir():
                raise FileNotFoundError(f"Figdir {dirpath} does not exist")
            n = self.len_classes()
            n_feat = self.models[0].n_features_in_
            fstem = f"{self.dataname()}-{self.ugv_name}-{n}c-{n_feat}f"
            for ext in u.EXTS:
                disp.figure_.savefig(
                    dirpath / f"{fstem}.{ext}",
                    bbox_inches="tight",
                )

        return disp

    def show_cm(self) -> ConfusionMatrixDisplay | None:
        """Show confusion matrix

        Returns:
            ConfusionMatrixDisplay | None: Confusion matrix
        """
        return self.disp

    def export_models(self, export_path: Path | str) -> None:
        """Save models in .mdl files

        Args:
            export_path (Path | str): Export directory
        """
        exp_path = Path(export_path)
        print(f"Exporting models in {exp_path.resolve()}")

        exp_path.mkdir(parents=True, exist_ok=True)
        for terrain, mdl in zip(self.terrains, self._models):
            fname = exp_path / f"{u.slugify(terrain)}.mdl"
            joblib.dump(mdl, fname)


# =============================================
# =============================================
# =============================================
# =============================================
# =============================================
# =============================================
# =============================================
# =============================================
# =============================================
# =============================================


class TerrainClassifier:
    _model_name: str
    classes_: np.ndarray
    n_classes_: int
    n_features_in_: int
    disp: ConfusionMatrixDisplay | None

    def __init__(
        self,
        terrains: Sequence[str],
        model_name: str,
        ugv_name: str = "husky",
    ) -> None:
        """Instantiate Classifier instance, with on the number of terrains

        Args:
            terrains (Sequence[str]): Sequence of terrain classes.
            model_name (str): Display model name, in title case (Model Name)
            ugv_name (str, optional): UGV Name. Defaults to "husky".
            model_args: Refer to https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestRegressor.html.
        """
        self.ugv_name = ugv_name
        self._model_name = model_name

    def len_classes(self) -> int:
        return len(self.classes_)

    @classmethod
    def filename(cls) -> str:
        return u.slugify(
            cls._model_name.lower(),
            sep="-",
        )

    @classmethod
    def dataname(cls) -> str:
        return "-".join(cls._model_name.lower().split()[::-1])

    @classmethod
    def model_name(cls) -> str:
        return u.slugify(
            cls._model_name.upper(),
            sep="",
        )

    @classmethod
    def display_name(cls) -> str:
        return cls._model_name

    def predict_idx(self, X: pd.DataFrame) -> np.ndarray:
        """Predict class index for every X

        Args:
            X (pd.DataFrame): values of X

        Returns:
            np.ndarray: Vector of class indices
        """
        X_test = X.copy()
        y_pred = self.predict(X_test)

        # print(type(y_pred), type(self.classes_))

        clf_idx = np.nonzero(y_pred[:, None] == self.classes_)[1]

        return clf_idx

    def metrics_name(self) -> str:
        """Name for metrics export"""
        n = self.len_classes()
        n_feat = self.n_features_in_
        return f"{self.dataname()}-{n:02}c-{n_feat:02}f"

    def metrics(
        self,
        y_true: ClassLabel,
        y_pred: ClassLabel,
        json_export: Path | str | None = None,
    ) -> pd.Series:
        """Estimate classification metrics

        Args:
            y_true (pd.Series | np.ndarray): True classes
            y_pred (pd.Series | np.ndarray): Predicted classes

        Returns:
            pd.Series: Classification metrics
        """
        metrics = pd.Series(
            {
                "accuracy": accuracy_score(
                    y_true,
                    y_pred,
                ),
                "precision": precision_score(
                    y_true,
                    y_pred,
                    labels=self.classes_,
                    average=None,
                ),
                "recall": recall_score(
                    y_true,
                    y_pred,
                    labels=self.classes_,
                    average=None,
                ),
                "f1-score": f1_score(
                    y_true,
                    y_pred,
                    labels=self.classes_,
                    average=None,
                ),
            }
        )

        if json_export is not None:
            outpath = Path(json_export)
        else:
            outpath = Path.cwd() / "metrics_ML.json"

        print(f"Exporting metrics to {outpath.resolve()}")

        exp_metrics = json.loads(metrics.to_json())
        exp_metrics["params"] = self.get_params()
        exp_metrics["terrains"] = self.classes_.tolist()

        n = self.n_classes_ if hasattr(self, "n_classes_") else len(self.classes_)
        n_feat = self.n_features_in_

        modname = f"{self.dataname()}-{n:02}c-{n_feat:02}f"

        with u.JSONExporter(outpath) as data:
            data[modname] = exp_metrics

        return metrics

    def cross_val_metrics(
        self,
        X: pd.DataFrame,
        y: pd.DataFrame,
        json_export: Path | str | None = None,
    ) -> pd.Series:
        """
        y : 1-column DataFrame with
        column 'P_motion' : motion power measurements (float)
        """
        X, y = X.copy(), y.copy()

        cval_score = cross_val_score(self, X, y.to_numpy().ravel())
        exp_metrics = {terr: cv_sco for terr, cv_sco in zip(self.classes_, cval_score)}

        if json_export is not None:
            outpath = Path(json_export)
        else:
            outpath = Path.cwd() / "metrics_ML.json"

        print(f"Exporting metrics to {outpath.resolve()}")

        with u.JSONExporter(outpath) as data:
            data[self.metrics_name()]["cross-val"] = exp_metrics

        return pd.Series(exp_metrics)

    def confusion_matrix(
        self,
        y_true: ClassLabel,
        y_pred: ClassLabel,
        figdir: Path | str | None = None,
        plot: bool = True,
        normalization: Literal["true", "pred", "all"] | None = None,
    ) -> ConfusionMatrixDisplay | np.ndarray:
        """Show confusion matrix

        Args:
            y_true (ClassLabel): y_true
            y_pred (ClassLabel): Predicted values of y
            figdir (Path | str | None, optional): Directory to save the confusion matrix figure. Defaults to None.
            plot (bool, optional): Flag to indicate whether to return the confusion matrix as a numpy array or a figure. Defaults to True.
            normalization (Literal['true', 'pred', 'all'] | None, optional): Normalization method. Defaults to None.

        Raises:
            FileNotFoundError: Raised if figdir is not an existing directory

        Returns:
            ConfusionMatrixDisplay | np.ndarray: Confusion matrix, be it a numpy array or a display
        """
        terrains = self.classes_
        cm = confusion_matrix(
            y_true,
            y_pred,
            labels=terrains,
            normalize=normalization,
        )

        if not plot:
            return cm

        disp = ConfusionMatrixDisplay.from_predictions(
            y_true,
            y_pred,
            labels=terrains,
            normalize=normalization,
            display_labels=terrains,
        )
        disp.ax_.set_title(f"Confusion matrix for {self._model_name}")
        self.disp = disp

        if figdir is not None:
            dirpath = Path(figdir)
            if not dirpath.exists() or not dirpath.is_dir():
                raise FileNotFoundError(f"Figdir {dirpath} does not exist")
            n = self.n_classes_ if hasattr(self, "n_classes_") else len(self.classes_)
            n_feat = self.n_features_in_
            fstem = f"{self.dataname()}-{self.ugv_name}-{n}c-{n_feat}f"
            for ext in u.EXTS:
                disp.figure_.savefig(
                    dirpath / f"{fstem}.{ext}",
                    bbox_inches="tight",
                )

        return disp

    def show_cm(self) -> ConfusionMatrixDisplay | None:
        """Show confusion matrix

        Returns:
            ConfusionMatrixDisplay | None: Confusion matrix
        """
        return self.disp

    def export_model(self, export_path: Path | str) -> None:
        """Save model in .mdl file

        Args:
            export_path (Path | str): Export directory
        """
        exp_path = Path(export_path)
        print(f"Exporting models in {exp_path.resolve()}")
        n_feat = self.n_features_in_
        n = self.n_classes_ if hasattr(self, "n_classes_") else len(self.classes_)

        exp_path.mkdir(parents=True, exist_ok=True)
        slugfname = u.slugify(self.filename())
        fname = exp_path / f"{slugfname}_{n:02}c_{n_feat:02}f.mdl"
        joblib.dump(self, fname)
