from __future__ import annotations

from typing import TYPE_CHECKING

from models import pentzer2014pow

if TYPE_CHECKING:
    from typing import Sequence

    import pandas as pd


def get_unseen_data(columns: Sequence[str], abbr: str = "H74") -> pd.DataFrame:
    unseen = pentzer2014pow.open_model_df(abbr)
    unseen["mass"] = unseen.meta.ugv_mass
    unseen = unseen[columns].copy().dropna()
    return unseen.dropna()
