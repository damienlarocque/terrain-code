from __future__ import annotations

import numpy as np
import pandas as pd

import utils as u
from utils import ssmr_utils as ssmru
from utils import ugv_utils as ugvu

__MODEL_NAME__ = "ROTATIONNALITY"


class ROT_CONSTANTS:
    "Constants for LaRocque 2023"

    # Gravity constant
    g = 9.81
    # N° Wheels
    N = 4

    # Sigma gyroscope / processus
    s_gyro = 0.02
    s_proc = 1e-2

    def __init__(self, metadata: ugvu.RunMetadata) -> None:
        self.m = metadata.ugv_mass
        self.r = metadata.ugv_wr
        self.W = metadata.ugv_wb
        self.B = metadata.ugv_wl
        self.Bs = metadata.ugv_Bs

        # Half distances
        self.halfB = self.B / 2
        self.halfW = self.W / 2

        # Weight
        self.weight = self.m * self.g

    def __str__(self) -> str:
        return str(self.value)


def rotationnality(abbr: str, df: pd.DataFrame | None = None, verbose: bool = True):
    """
    LaRocque 2023 :
    A metric to determine how we drive a robot
    """
    if df is None:
        df = ugvu.read_df_rename_columns(abbr)

    MODCONST = ROT_CONSTANTS(df.meta)

    df["vL"] = df.wL * MODCONST.r
    df["vR"] = df.wR * MODCONST.r

    df["deltaV"] = df.vR - df.vL

    df["cmd_vL"] = df.cmd_v - MODCONST.B / 2 * df.cmd_w
    df["cmd_vR"] = df.cmd_v + MODCONST.B / 2 * df.cmd_w

    # Naive
    V_sum = df.cmd_vR.abs() + df.cmd_vL.abs()
    df["rhocmd_naive"] = (MODCONST.B * df.cmd_w.abs()) / V_sum
    df["rho_naive"] = (df.vR - df.vL).abs() / (df.vR.abs() + df.vL.abs())

    df = ssmru.naive_icr(df)

    time_a = df.head(1).time.item()
    time_b = df.tail(1).time.item()
    df["time_prop"] = (df.time - time_a) / (time_b - time_a)
    df["time_rel"] = df.time - time_a
    rel_time = df.time_rel

    # Better rho
    VRot = MODCONST.B * df.wz
    VLin = df.vx.abs()
    df["rho"] = VRot.abs() / (VLin + VRot.abs())

    VRotCmd = MODCONST.B * df.cmd_w
    VLinCmd = df.cmd_v.abs()
    df["cmd_rho"] = VRotCmd.abs() / (VLinCmd + VRotCmd.abs())

    df["lin"] = 1 - df.rho
    df["cmd_lin"] = 1 - df.cmd_rho

    df["eta_rho"] = df.rho / df.cmd_rho
    df["eta_lambda"] = df.lin / df.cmd_lin

    df.meta.models.append(__MODEL_NAME__)

    return df
