import pandas as pd
import utils as u


def unicycle_main(abbr: str):
    # Get data
    filename = u.get_filename(abbr)
    basename = u.get_basename(filename)
    rundir = u.get_rundir(basename)
    merged_path = rundir / "merged"
    df_path = merged_path / f"{basename}_mcf.csv"
    merged_df = pd.read_csv(df_path)
    abbr = u.abbreviate(basename)

    coi = [
        "time",
        "time_rel",
        "/imu_and_wheel_odom/lin_twi/x",
        "/imu_and_wheel_odom/lin_twi/y",
        "/imu_and_wheel_odom/lin_twi/z",
        "/mcu/status/current_battery",
        "/mcu/status/measured_battery",
        "/MTI_imu/data/lin_acc/x",
        "/MTI_imu/data/lin_acc/y",
        "/MTI_imu/data/lin_acc/z",
        "/imu_and_wheel_odom/orientation/pitch",
        "/MTI_imu/data/ang_vel/z",
        "/left_drive/velocity",
        "/right_drive/velocity",
    ]

    df = merged_df[coi].copy()

    rename_cols = {
        "/imu_and_wheel_odom/lin_twi/x": "vx",
        "/imu_and_wheel_odom/lin_twi/y": "vy",
        "/imu_and_wheel_odom/lin_twi/z": "vz",
        "/mcu/status/current_battery": "current",
        "/mcu/status/measured_battery": "voltage",
        "/imu_and_wheel_odom/orientation/pitch": "theta",
        "/MTI_imu/data/orientation/w": "qw",
        "/MTI_imu/data/orientation/x": "qx",
        "/MTI_imu/data/orientation/y": "qy",
        "/MTI_imu/data/orientation/z": "qz",
        "/MTI_imu/data/ang_vel/z": "w",
    }
    df.rename(columns=rename_cols, inplace=True, errors="raise")
