from __future__ import annotations

from typing import Dict, NamedTuple

import numpy as np
import pandas as pd
from scipy.integrate import cumulative_trapezoid
from tqdm import tqdm

import utils as u
from utils import ssmr_utils as ssmru
from utils import ugv_utils as ugvu

__MODEL_NAME__ = "RAW"


class RAW_CONSTANTS:
    "Raw constants"

    # Gravity constant
    g = 9.81
    # N° Wheels
    N = 4

    def __init__(self, metadata: ugvu.RunMetadata) -> None:
        self.m = metadata.ugv_mass
        self.r = metadata.ugv_wr
        self.W = metadata.ugv_wb
        self.L = metadata.ugv_wl

        # Half distances
        self.halfL = self.L / 2
        self.halfW = self.W / 2

        # Weight
        self.weight = self.m * self.g

    def __str__(self) -> str:
        return str(self.value)


def raw(abbr: str, df: pd.DataFrame | None = None, verbose: bool = True) -> pd.DataFrame:
    """
    Raw computation
    """
    if df is None:
        df = ugvu.read_df_rename_columns(abbr)

    MODCONST = RAW_CONSTANTS(df.meta)

    df["P_total"] = df.current * df.voltage
    df["v"] = np.sqrt(df.vx**2 + df.vy**2 + df.vz**2)
    df["a"] = np.sqrt(df.ax_corr**2 + df.ay_corr**2 + df.az_corr**2)

    df["vL"] = df.wL * MODCONST.r
    df["vR"] = df.wR * MODCONST.r

    P_Logical = ssmru.get_logical_power(abbr)
    df["P_logical"] = P_Logical
    df["P_motion"] = df.P_total - P_Logical

    # Energy
    time_a = df.head(1).time.item()
    time_b = df.tail(1).time.item()
    df["time_ratio"] = (df.time - time_a) / (time_b - time_a)
    df["time_rel"] = df.time - time_a

    # Export csv
    model_name = __MODEL_NAME__.lower()

    filename = u.get_filename(abbr)
    basename = u.get_basename(filename)
    model_path = u.get_rundir(basename, subdir="models")
    export_csv_path = model_path / f"{basename}_{model_name}.csv"
    df.to_csv(export_csv_path, index=False)
    if verbose:
        print(f"{abbr} : Exported {__MODEL_NAME__} CSV in {export_csv_path}")

    df.meta.models.append(__MODEL_NAME__)

    return df
