from enum import Enum
from typing import Optional

import numpy as np
import pandas as pd
import utils as u
from utils import imu_processing as imup
from utils import k1d_utils as k1du


class K1D_CONSTANTS(float, Enum):
    "k1d constants"

    def __str__(self) -> str:
        return str(self.value)

    # Gravity constant
    g = 9.81
    # m (kg) from Figure 7 of https://arxiv.org/pdf/2111.13981.pdf
    m = 590
    W = m * g
    # C_I (N) from Quann 2018
    C_I = 6
    RLS_lambda = 0.85


K1DCONST = K1D_CONSTANTS


def get_b_int(basename: str, b_int_idx: Optional[int] = None):
    """Get b int value"""

    # Get data
    df: pd.DataFrame = u.get_combinedf(basename)

    assert "current" in df.columns.values, "No column in df with name : 'current'"

    abbr = u.abbreviate(basename)
    sensors_energy = k1du.b_int_from_json(abbr)
    if sensors_energy is not None:
        return sensors_energy["power"]

    if b_int_idx is None:
        min_idx = df.current.argmin()
        min_curr = df.current[min_idx]
        min_volt = df.voltage[min_idx]

    else:
        assert b_int_idx <= len(df.index), ""
        min_curr = df.current[b_int_idx]
        min_volt = df.voltage[b_int_idx]

    min_pow = min_curr * min_volt

    k1du.b_int_to_json(
        abbr,
        {
            "current": min_curr,
            "voltage": min_volt,
            "power": min_pow,
            "index": str(b_int_idx or min_idx),
        },
    )

    return min_pow


def k1d_main(abbr: str):
    """Plot rolling resistance estimate"""
    # Get data
    filename = u.get_filename(abbr)
    basename = u.get_basename(filename)
    rundir = u.get_rundir(basename)
    merged_path = rundir / "merged"
    df_path = merged_path / f"{basename}_mcf.csv"
    merged_df = pd.read_csv(df_path)

    if "/cmd_vel/linear/x" not in merged_df.columns.values:
        rename_cols = {
            "/warthog_velocity_controller/cmd_vel/linear/x": "/cmd_vel/linear/x",
            "/warthog_velocity_controller/cmd_vel/angular/z": "/cmd_vel/angular/z",
        }
        merged_df.rename(columns=rename_cols, inplace=True, errors="raise")

    # Retrieve topics of interest
    if "back_altitude" in merged_df.columns.values:
        coi = [
            "time",
            "time_rel",
            "/imu_and_wheel_odom/lin_twi/x",
            "/imu_and_wheel_odom/lin_twi/y",
            "/imu_and_wheel_odom/lin_twi/z",
            "/mcu/status/current_battery",
            "/mcu/status/measured_battery",
            "/MTI_imu/data/lin_acc/x",
            "/MTI_imu/data/lin_acc/y",
            "/MTI_imu/data/lin_acc/z",
            "/imu_and_wheel_odom/orientation/pitch",
            "/MTI_imu/data/orientation/w",
            "/MTI_imu/data/orientation/x",
            "/MTI_imu/data/orientation/y",
            "/MTI_imu/data/orientation/z",
            "back_latitude",
            "back_longitude",
            "back_altitude",
            "/MTI_imu/data/ang_vel/z",
            "/cmd_vel/linear/x",
            "/cmd_vel/angular/z",
        ]
        df = merged_df[coi].copy()

        rename_cols = {
            "/imu_and_wheel_odom/lin_twi/x": "vx",
            "/imu_and_wheel_odom/lin_twi/y": "vy",
            "/imu_and_wheel_odom/lin_twi/z": "vz",
            "/mcu/status/current_battery": "current",
            "/mcu/status/measured_battery": "voltage",
            "/MTI_imu/data/lin_acc/x": "ax",
            "/MTI_imu/data/lin_acc/y": "ay",
            "/MTI_imu/data/lin_acc/z": "az",
            "/imu_and_wheel_odom/orientation/pitch": "theta",
            "/MTI_imu/data/orientation/w": "qw",
            "/MTI_imu/data/orientation/x": "qx",
            "/MTI_imu/data/orientation/y": "qy",
            "/MTI_imu/data/orientation/z": "qz",
            "back_latitude": "lat",
            "back_longitude": "long",
            "back_altitude": "alt",
            "/MTI_imu/data/ang_vel/z": "rot_vel",
            "/cmd_vel/linear/x": "cmd_v",
            "/cmd_vel/angular/z": "cmd_w",
        }
    else:
        coi = [
            "time",
            "time_rel",
            "/imu_and_wheel_odom/lin_twi/x",
            "/imu_and_wheel_odom/lin_twi/y",
            "/imu_and_wheel_odom/lin_twi/z",
            "/mcu/status/current_battery",
            "/mcu/status/measured_battery",
            "/MTI_imu/data/lin_acc/x",
            "/MTI_imu/data/lin_acc/y",
            "/MTI_imu/data/lin_acc/z",
            "/imu_and_wheel_odom/orientation/pitch",
            "/MTI_imu/data/orientation/w",
            "/MTI_imu/data/orientation/x",
            "/MTI_imu/data/orientation/y",
            "/MTI_imu/data/orientation/z",
            "/MTI_imu/data/ang_vel/z",
            "/cmd_vel/linear/x",
            "/cmd_vel/angular/z",
        ]
        df = merged_df[coi].copy()

        rename_cols = {
            "/imu_and_wheel_odom/lin_twi/x": "vx",
            "/imu_and_wheel_odom/lin_twi/y": "vy",
            "/imu_and_wheel_odom/lin_twi/z": "vz",
            "/mcu/status/current_battery": "current",
            "/mcu/status/measured_battery": "voltage",
            "/MTI_imu/data/lin_acc/x": "ax",
            "/MTI_imu/data/lin_acc/y": "ay",
            "/MTI_imu/data/lin_acc/z": "az",
            "/imu_and_wheel_odom/orientation/pitch": "theta",
            "/MTI_imu/data/orientation/w": "qw",
            "/MTI_imu/data/orientation/x": "qx",
            "/MTI_imu/data/orientation/y": "qy",
            "/MTI_imu/data/orientation/z": "qz",
            "/MTI_imu/data/ang_vel/z": "rot_vel",
            "/cmd_vel/linear/x": "cmd_v",
            "/cmd_vel/angular/z": "cmd_w",
        }
    df.rename(columns=rename_cols, inplace=True, errors="raise")

    df = df[df["cmd_v"].notna()]
    df = df.reset_index(drop=True)

    imu_cols = df[["ax", "ay", "az", "qw", "qx", "qy", "qz"]].to_numpy()
    imu_data = [([ax, ay, az], [qw, qx, qy, qz]) for ax, ay, az, qw, qx, qy, qz in imu_cols]
    corr_quat = [imup.remove_gravity(acc, imup.Quaternion.from_list(att)) for acc, att in imu_data]
    accelerations = np.array([cq.to_list()[1:] for cq in corr_quat])

    # Assert that all w components are close to zero
    zero_w = np.allclose([q.w for q in corr_quat], 0)
    assert zero_w, "All quaternions should have a w value close to 0"

    # Add corrected accelerations to dataframe
    corr_lab = ["ax_c", "ay_c", "az_c"]
    for idx, elem in enumerate(corr_lab):
        df[elem] = accelerations[:, idx]

    # Invert for GPS theta standard
    # + go up
    # - go down
    df["theta"] = -df.theta
    if abbr == "R11":
        df["theta"] = df.theta * np.sign(df.vx)

    df["P"] = df.current * df.voltage
    df["v"] = np.sqrt(df.vx**2 + df.vy**2 + df.vz**2)
    df["a"] = np.sqrt(df.ax_c**2 + df.ay_c**2 + df.az_c**2)

    b_int = get_b_int(basename)

    df["At"] = df.v * K1DCONST.W * np.cos(df.theta)
    df["Bt"] = df.v * (K1DCONST.W * np.sin(df.theta) + K1DCONST.m * df.a + K1DCONST.C_I) + b_int
    df["rr_simple"] = (df.P - df.Bt) / df.At

    print(df.index, len(df.index))

    # RLS
    df["P_adj"] = df.P - df.Bt
    df["At2"] = np.square(df.At)
    df["AtPadj"] = df.At * df.P_adj

    # Forgetting factors
    n = len(df.index)
    fffactors = np.fromfunction(lambda i, j: K1DCONST.RLS_lambda ** (i - j), (n, n), dtype=int)

    def rollres_rls(k: int):
        AtPadj = df.AtPadj[: k + 1]
        At2 = df.At2[: k + 1]
        ffactor = fffactors[k, : k + 1]

        return (AtPadj * ffactor).sum() / (At2 * ffactor).sum()

    df["rr_rls"] = pd.Series({idx: rollres_rls(idx) for idx in df.index})
    # if abbr == "R11":
    #     df = df.iloc[2:].copy()
    #     df.index = pd.RangeIndex(len(df.index))
    #     df["time_rel"] = df.time - df.time[0]
    k1du.rr_to_json(abbr, df)

    df["rotvel_mag"] = df.rot_vel.abs()

    # Export csv
    merged_path = rundir / "k1d"
    rr_csv_path = merged_path / f"{basename}_k1d.csv"
    df.to_csv(rr_csv_path, index=False)
    print(f"{abbr} : Exported rolling res CSV in {rr_csv_path}")
