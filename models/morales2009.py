from __future__ import annotations

import numpy as np
import pandas as pd
from scipy import optimize
from scipy.integrate import cumulative_trapezoid
from tqdm import tqdm

import utils as u
from utils import electrical_processing as elecp
from utils import ssmr_utils as ssmru
from utils import ugv_utils as ugvu

__MODEL_NAME__ = "MORALES2009"


class MORALES_CONSTANTS:
    "Constants for Morale2009"

    # Gravity constant
    g = 9.81
    # N° Wheels
    N = 4

    def __init__(self, metadata: ugvu.RunMetadata) -> None:
        self.m = metadata.ugv_mass
        self.r = metadata.ugv_wr
        self.W = metadata.ugv_wb
        self.L = metadata.ugv_wl
        self.D = metadata.ugv_wb

        # Half distances
        self.halfL = self.L / 2
        self.halfW = self.W / 2

        # Weight
        self.weight = self.m * self.g

        # UGV name
        self.name = metadata.ugv_name

        # Current bias
        self.I_bias = metadata.motor_I_bias
        self.Kt = metadata.motor_Kt

    def __str__(self) -> str:
        return str(self.value)


def morales2009(
    abbr: str,
    df: pd.DataFrame | None = None,
    compute_errors: bool = True,
    verbose: bool = True,
    window_size: int | None = None,
) -> pd.DataFrame:
    """
    Morales 2009 :
    Power Consumption Modeling of Skid-Steer Tracked Mobile Robots on Rigid Terrain
    https://doi.org/10.1109/TRO.2009.2026499

    SSMR Power Model with 2 coefficients (mu, G) found with simplex
    """
    if df is None:
        df = ugvu.read_df_rename_columns(abbr)

    MODCONST = MORALES_CONSTANTS(df.meta)

    df["v"] = np.sqrt(df.vx**2 + df.vy**2 + df.vz**2)
    df["a"] = np.sqrt(df.ax_corr**2 + df.ay_corr**2 + df.az_corr**2)

    df["vL"] = df.wL * MODCONST.r
    df["vR"] = df.wR * MODCONST.r

    time_a = df.head(1).time.item()
    time_b = df.tail(1).time.item()
    df["time_prop"] = (df.time - time_a) / (time_b - time_a)
    df["time_rel"] = df.time - time_a
    rel_time = df.time_rel

    df["xL"] = cumulative_trapezoid(df.vL, rel_time, initial=0)
    df["xR"] = cumulative_trapezoid(df.vR, rel_time, initial=0)
    df["ang"] = cumulative_trapezoid(df.wz, rel_time, initial=0)
    df["chi"] = MODCONST.D * df.ang / (df.xL - df.xR)

    elecp.motion_power(df)

    df["P_motion"] = df.P_motion.copy() - MODCONST.I_bias * MODCONST.Kt * (df.wL.abs() + df.wR.abs())

    df = ssmru.naive_icr(df)

    df["normal_force"] = MODCONST.weight * np.cos(df.theta) / MODCONST.N

    df["dx_front"] = MODCONST.halfL - df.ICRx
    df["dx_rear"] = MODCONST.halfL + df.ICRx
    df["dy_left"] = MODCONST.halfW - df.ICRyL
    df["dy_right"] = MODCONST.halfW + df.ICRyR

    # Distance
    # * front-left
    # * front-right
    # * rear-left
    # * rear-right
    df["d_fl"] = np.sqrt(df.dx_front**2 + df.dy_left**2)
    df["d_fr"] = np.sqrt(df.dx_front**2 + df.dy_right**2)
    df["d_rl"] = np.sqrt(df.dx_rear**2 + df.dy_left**2)
    df["d_rr"] = np.sqrt(df.dx_rear**2 + df.dy_right**2)

    df["dist_asym"] = df.d_fl + df.d_fr + df.d_rl + df.d_rr
    df["dist_sym"] = np.sqrt(MODCONST.L**2 + (MODCONST.W - df.Bs) ** 2)

    df["JS_asym"] = df.wz.abs() * df.normal_force * df.dist_asym
    df["JS_sym"] = 2 * df.wz.abs() * df.normal_force * df.dist_sym

    df["JG"] = df.vL.abs() + df.vR.abs()

    # Gora 2021
    H_sym = df[["JS_sym", "JG"]].copy()
    H_asym = df[["JS_asym", "JG"]].copy()
    P_meas = df.P_motion.copy()

    # msym, Gsym, errsym, masym, Gasym, errasym
    coefficients = np.empty((df.shape[0], 6))

    def power_estimate_error(x: np.ndarray, H: np.ndarray, P_measured: np.ndarray):
        P_est = H @ x
        P_err = P_est - P_measured
        P_err_squared = P_err**2
        return P_err_squared.sum()

    for idx in tqdm(df.index, desc=abbr, colour="blue"):
        if MODCONST.name == "warthog":
            x_sym = np.array([0.5, 150])
            x_asym = np.array([0.5, 150])
        elif MODCONST.name == "husky":
            x_sym = np.array([0.4, 50])
            x_asym = np.array([0.4, 50])

        min_bound = 0 if window_size is None else max(0, idx - window_size)
        Has_idx = H_asym.iloc[min_bound:idx].to_numpy()
        Hs_idx = H_sym.iloc[min_bound:idx].to_numpy()
        P_idx = P_meas.iloc[min_bound:idx].to_numpy()

        # Symmetrical
        optsym = optimize.fmin(
            power_estimate_error,
            x0=x_sym,
            args=(
                Hs_idx,
                P_idx,
            ),
            full_output=True,
            disp=verbose,
        )
        xopt, fopt, *_ = optsym
        coefficients[idx, :3] = np.array([*xopt, fopt])

        # Asymmetrical
        optasym = optimize.fmin(
            power_estimate_error,
            x0=x_asym,
            args=(
                Has_idx,
                P_idx,
            ),
            full_output=True,
            disp=verbose,
        )
        xopt, fopt, *_ = optasym
        coefficients[idx, 3:] = np.array([*xopt, fopt])

    df["m_sym"] = coefficients[:, 0]
    df["G_sym"] = coefficients[:, 1]
    df["err_sym"] = coefficients[:, 2]
    df["m_asym"] = coefficients[:, 3]
    df["G_asym"] = coefficients[:, 4]
    df["err_asym"] = coefficients[:, 5]

    if not compute_errors:
        df.meta.models.append(__MODEL_NAME__)
        return df

    # Predict power and compute power error
    # df[["JS_sym", "JG"]]
    # df[["JS_asym", "JG"]]
    for m in ("sym", "asym"):
        PS = df[f"m_{m}"] * df[f"JS_{m}"]
        PG = df[f"G_{m}"] * df.JG
        df[f"Ppredt_{m}"] = PS + PG
        # Then
        df[f"Perrt_{m}"] = df.P_motion - df[f"Ppredt_{m}"]

        PS = df[f"m_{m}"].dropna().iloc[-10:].mean() * df[f"JS_{m}"]
        PG = df[f"G_{m}"].dropna().iloc[-10:].mean() * df.JG
        df[f"Ppredc_{m}"] = PS + PG
        # Then
        df[f"Perrc_{m}"] = df.P_motion - df[f"Ppredc_{m}"]

    # Energy
    df["energy_Wh"] = cumulative_trapezoid(df.P_motion, rel_time, initial=0) / 3600
    df["Epredt_sym_Wh"] = cumulative_trapezoid(df.Ppredt_sym, rel_time, initial=0) / 3600
    df["Epredt_asym_Wh"] = cumulative_trapezoid(df.Ppredt_asym, rel_time, initial=0) / 3600
    df["Epredc_sym_Wh"] = cumulative_trapezoid(df.Ppredc_sym, rel_time, initial=0) / 3600
    df["Epredc_asym_Wh"] = cumulative_trapezoid(df.Ppredc_asym, rel_time, initial=0) / 3600

    coeffs = df[["m_asym", "G_asym", "m_sym", "G_sym"]].copy()
    model_data = {}
    model_data["coefficients"] = {
        col: {
            "min": coeffs[col].min(),
            "max": coeffs[col].max(),
            "idxmin": str(coeffs[col].argmin()),
            "idxmax": str(coeffs[col].argmax()),
            "last": coeffs[col].iloc[-1],
        }
        for col in coeffs.columns.values
    }
    model_data["errors"] = {
        "casym": ssmru.energy_err(
            df.energy_Wh,
            df.Epredc_asym_Wh,
        ),
        "csym": ssmru.energy_err(
            df.energy_Wh,
            df.Epredc_sym_Wh,
        ),
        "tasym": ssmru.energy_err(
            df.energy_Wh,
            df.Epredt_asym_Wh,
        ),
        "tsym": ssmru.energy_err(
            df.energy_Wh,
            df.Epredt_sym_Wh,
        ),
    }

    ssmru.model_values_to_json(abbr, __MODEL_NAME__, model_data)

    # Export csv
    model_name = __MODEL_NAME__.lower()

    filename = u.get_filename(abbr)
    basename = u.get_basename(filename)
    model_path = u.get_rundir(basename, subdir="models")
    export_csv_path = model_path / f"{basename}_{model_name}.csv"
    df.to_csv(export_csv_path, index=False)
    if verbose:
        print(f"{abbr} : Exported {__MODEL_NAME__} CSV in {export_csv_path}")

    df.meta.models.append(__MODEL_NAME__)

    return df


def open_model_df(abbr: str) -> pd.DataFrame:
    filename = u.get_filename(abbr)
    basename = u.get_basename(filename)
    model_path = u.get_rundir(basename, subdir="models")
    model_name = __MODEL_NAME__.lower()
    export_csv_path = model_path / f"{basename}_{model_name}.csv"
    fdf = pd.read_csv(export_csv_path)

    ugv_name = ugvu.get_ugv(abbr)
    fdf.meta = ugvu.RunMetadata()
    fdf.meta.ugv_name = ugv_name.lower()

    fdf = ugvu.add_ugv_metadata(fdf, ugv_name)

    return fdf
