from __future__ import annotations

import numpy as np
import pandas as pd

import utils as u
from utils import electrical_processing as elecp
from utils import ssmr_utils as ssmru
from utils import ugv_utils as ugvu

__MODEL_NAME__ = "ELECMOTMOD"


class ELECMOTMOD_CONSTANTS:
    "ELECMOTMOD constants"

    # Gravity constant
    g = 9.81
    # N° Wheels
    N = 4

    def __init__(self, metadata: ugvu.RunMetadata) -> None:
        self.m = metadata.ugv_mass
        self.r = metadata.ugv_wr
        self.W = metadata.ugv_wb
        self.L = metadata.ugv_wl

        # Half distances
        self.halfL = self.L / 2
        self.halfW = self.W / 2

        # Weight
        self.weight = self.m * self.g

        # Motor model
        self.Kt = metadata.motor_Kt
        self.gear_ratio = metadata.gear_ratio

    def __str__(self) -> str:
        return str(self.value)

    # Measurement noise in Watt
    measurement_noise = 25
    # EWMA lambda
    EWMlambda = 0.05


def emm_main(abbr: str, df: pd.DataFrame | None = None, verbose: bool = True) -> pd.DataFrame:
    """
    Electrical motor model
    """

    if df is None:
        df = ugvu.read_df_rename_columns(abbr)

    MODCONST = ELECMOTMOD_CONSTANTS(df.meta)

    df["v"] = np.sqrt(df.vx**2 + df.vy**2 + df.vz**2)
    df["a"] = np.sqrt(df.ax_corr**2 + df.ay_corr**2 + df.az_corr**2)

    df["vL"] = df.wL * MODCONST.r
    df["vR"] = df.wR * MODCONST.r

    elecp.motion_power(df)

    df["P_mot"] = df.P_motion
    df["P_log"] = df.P_logical

    df["Ploss_L"] = df.PE_L - df.PM_L
    df["Rloss_L"] = df.Ploss_L / (df.I_L) ** 2
    df["Ploss_R"] = df.PE_R - df.PM_R
    df["Rloss_R"] = df.Ploss_R / (df.I_R) ** 2

    df["eta_L"] = df.PM_L / df.PE_L
    df["eta_R"] = df.PM_R / df.PE_R

    df["dI_L"] = df.I_L.diff().fillna(0)
    df["dI_R"] = df.I_R.diff().fillna(0)
    df["dt"] = df.time.diff()
    df.dt.iloc[0] = df.dt.iloc[1]

    df["dILdt"] = df.dI_L / df.dt
    df["dIRdt"] = df.dI_R / df.dt

    motL = df[["dILdt", "I_L", "wL"]].copy().to_numpy()
    motR = df[["dIRdt", "I_R", "wR"]].copy().to_numpy()

    V_L = df.V_L.copy().to_numpy()
    V_R = df.V_R.copy().to_numpy()

    motL = np.vstack([motL, np.zeros((2, 3))])
    motR = np.vstack([motR, np.zeros((2, 3))])
    V_L = np.concatenate([V_L, np.zeros(2)])
    V_R = np.concatenate([V_R, np.zeros(2)])

    def solve_params(idx: int):
        A_L = motL[: idx + 3, :]
        A_R = motR[: idx + 3, :]
        B_L = V_L[: idx + 3]
        B_R = V_R[: idx + 3]

        def try_solve(A_mat, B_mat):
            # try:
            #     X = np.linalg.solve(A_mat, B_mat)
            # except np.linalg.LinAlgError:
            #     X = np.linalg.lstsq(A_mat, B_mat, rcond=None)[0]
            # return X
            X, r, rk, s = np.linalg.lstsq(A_mat, B_mat, rcond=None)
            return X

        x_L = try_solve(A_L, B_L)
        x_R = try_solve(A_R, B_R)

        d_L = dict(zip(("L_L", "R_L", "K_L"), x_L))
        d_R = dict(zip(("L_R", "R_R", "K_R"), x_R))

        return {**d_L, **d_R}

    # df["motor_coeffs"] = pd.Series({idx: solve_params(idx) for idx in df.index})
    # coeffs_df = pd.DataFrame(df.motor_coeffs.to_dict()).T

    # emm_df = df.join(coeffs_df).drop(columns=["motor_coeffs"])
    # fdf = emm_df.copy()
    fdf = df.copy()
    fdf.meta = df.meta

    # model_data = {}
    # coeffs = fdf[["L_L", "R_L", "K_L", "L_R", "R_R", "K_R"]].copy()
    # model_data["motor_params"] = {
    #     col: {
    #         "min": coeffs[col].min(),
    #         "max": coeffs[col].max(),
    #         "idxmin": str(coeffs[col].argmin()),
    #         "idxmax": str(coeffs[col].argmax()),
    #     }
    #     for col in coeffs.columns.values
    # }
    # ssmru.model_values_to_json(abbr, __MODEL_NAME__, model_data)

    # Export csv
    fdf.meta.models.append(__MODEL_NAME__)
    model_name = "-".join([mn.lower() for mn in fdf.meta.models])

    filename = u.get_filename(abbr)
    basename = u.get_basename(filename)
    model_path = u.get_rundir(basename, subdir="models")
    export_csv_path = model_path / f"{basename}_{model_name}.csv"
    fdf.to_csv(export_csv_path, index=False)
    if verbose:
        print(f"{abbr} : Exported {__MODEL_NAME__} CSV in {export_csv_path}")

    return fdf
