from enum import Enum

import numpy as np

import utils as u
from utils import ugv_utils as ugvu


class SLIP_TRACK_CONSTANTS(float, Enum):
    "Slip Track constants"

    def __str__(self) -> str:
        return str(self.value)

    # Sigma gyroscope / processus
    s_gyro = 0.02
    s_proc = 1e-2

    # Gravity constant
    g = 9.81
    # m (kg) from Baril2020
    m = 590
    # wheel dimensions in meters
    W = 1.2
    L = 0.9
    r = 0.3
    # N° Wheels
    N = 4
    # Half distances
    halfL = L / 2
    halfW = W / 2
    # Weight
    weight = m * g
    # Measurement noise in Watt
    measurement_noise = 50
    # EWMA lambda
    EWMA_lambda = 0.05


ST_CONST = SLIP_TRACK_CONSTANTS


def sliptrack_main(abbr: str):
    """Slip Track from Reina 2016"""
    df = ugvu.read_df_rename_columns(abbr)

    filename = u.get_filename(abbr)
    basename = u.get_basename(filename)
    rundir = u.get_rundir(basename)

    df["P_total"] = df.current * df.voltage
    df["v"] = np.sqrt(df.vx**2 + df.vy**2 + df.vz**2)
    df["a"] = np.sqrt(df.ax_corr**2 + df.ay_corr**2 + df.az_corr**2)

    df["vL"] = df.wL * ST_CONST.r
    df["vR"] = df.wR * ST_CONST.r

    df["deltaV"] = df.vR - df.vL

    df["Bs_naive"] = df.deltaV / df.w_z

    return df
