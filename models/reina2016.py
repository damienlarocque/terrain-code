from __future__ import annotations

import numpy as np
import pandas as pd

import utils as u
from utils import electrical_processing as elecp
from utils import ssmr_utils as ssmru
from utils import ugv_utils as ugvu

__MODEL_NAME__ = "REINA2016"


class REINA_CONSTANTS:
    "Constants for Reina 2016"

    # Gravity constant
    g = 9.81
    # N° Wheels
    N = 4

    # Sigma gyroscope / processus
    s_gyro = 0.02
    s_proc = 1e-2

    def __init__(self, metadata: ugvu.RunMetadata) -> None:
        self.m = metadata.ugv_mass
        self.r = metadata.ugv_wr
        self.B = metadata.ugv_wb
        self.L = metadata.ugv_wl

        self.h = 0

        # Weight
        self.W = self.m * self.g

        # UGV name
        self.name = metadata.ugv_name

        # Motor model
        self.Kt = metadata.motor_Kt
        self.gear_ratio = metadata.gear_ratio

    def __str__(self) -> str:
        return str(self.value)


def reina2016(abbr: str, df: pd.DataFrame | None = None, verbose: bool = True) -> pd.DataFrame:
    """
    Reina 2016 :
    Slip-based terrain estimation with a skid-steer vehicle
    https://doi.org/10.1080/00423114.2016.1203961

    Slip track estimation with a Kalman Filter
    """
    if df is None:
        df = ugvu.read_df_rename_columns(abbr)

    MODCONST = REINA_CONSTANTS(df.meta)

    df["v"] = np.sqrt(df.vx**2 + df.vy**2 + df.vz**2)
    df["a"] = np.sqrt(df.ax_corr**2 + df.ay_corr**2 + df.az_corr**2)

    df["vL"] = df.wL * MODCONST.r
    df["vR"] = df.wR * MODCONST.r

    df["deltaV"] = df.vR - df.vL
    df["Bs_naive"] = df.deltaV / df.wz

    df["cmd_vL"] = df.cmd_v - MODCONST.B / 2 * df.cmd_w
    df["cmd_vR"] = df.cmd_v + MODCONST.B / 2 * df.cmd_w

    elecp.motion_power(df)

    df = ssmru.naive_icr(df)

    time_a = df.head(1).time.item()
    time_b = df.tail(1).time.item()
    df["time_prop"] = (df.time - time_a) / (time_b - time_a)
    df["time_rel"] = df.time - time_a
    rel_time = df.time_rel

    cos_t = np.cos(df.theta)
    cos_p = np.cos(df.psi)
    sin_t = np.sin(df.theta)
    sin_p = np.sin(df.psi)
    FzA = MODCONST.W / 4 * cos_t * cos_p
    FzB = MODCONST.W / 2 * sin_t * MODCONST.h / MODCONST.L
    FzC = MODCONST.W / 2 * cos_t * sin_p * MODCONST.h / MODCONST.B

    df["Fz1"] = FzA + FzB - FzC
    df["Fz2"] = FzA - FzB - FzC
    df["Fz3"] = FzA + FzB + FzC
    df["Fz4"] = FzA - FzB - FzC

    # df["P_L"] = df.I_L * df.V_L
    # df["P_R"] = df.I_R * df.V_R

    # x_sym = np.array([1, 250])
    # x_asym = np.array([1, 250])
    # P_meas = df.P_motion.copy().to_numpy()
    # coefficients = {}

    # def power_estimate_error(x: np.ndarray, H: np.ndarray, P_measured: np.ndarray):
    #     P_est = H @ x
    #     P_err = P_est - P_measured
    #     P_err_squared = P_err**2
    #     return P_err_squared.sum()

    # # Symmetrical
    # optimized = optimize.fmin(
    #     power_estimate_error,
    #     x0=x_sym,
    #     args=(H_sym, P_meas),
    #     full_output=True,
    # )
    # xopt, fopt, *_ = optimized

    # coefficients["sym"] = {
    #     "m": xopt[0],
    #     "G": xopt[1],
    #     "err": fopt,
    # }

    # # Asymmetrical
    # optimized = optimize.fmin(
    #     power_estimate_error,
    #     x0=x_asym,
    #     args=(H_asym, P_meas),
    #     full_output=True,
    # )
    # xopt, fopt, *_ = optimized

    # coefficients["asym"] = {
    #     "m": xopt[0],
    #     "G": xopt[1],
    #     "err": fopt,
    # }

    # df["m_sym"] = coefficients["sym"]["m"]
    # df["G_sym"] = coefficients["sym"]["G"]
    # df["m_asym"] = coefficients["asym"]["m"]
    # df["G_asym"] = coefficients["asym"]["G"]

    # # Predict power and compute power error
    # # df[["JS_sym", "JG"]]
    # PS_sym = coefficients["sym"]["m"] * df.JS_sym
    # PG_sym = coefficients["sym"]["G"] * df.JG
    # df["Ppred_sym"] = PS_sym + PG_sym
    # df["Perr_sym"] = df.P_motion - df.Ppred_sym

    # PS_asym = coefficients["asym"]["m"] * df.JS_asym
    # PG_asym = coefficients["asym"]["G"] * df.JG
    # df["Ppred_asym"] = PS_asym + PG_asym
    # df["Perr_asym"] = df.P_motion - df.Ppred_asym

    # # Energy
    # time_a = df.head(1).time.item()
    # time_b = df.tail(1).time.item()
    # df["time_prop"] = (df.time - time_a) / (time_b - time_a)
    # df["time_rel"] = df.time - time_a
    # rel_time = df.time_rel

    # df["energy_Wh"] = cumulative_trapezoid(df.P_motion, rel_time, initial=0) / 3600
    # df["Epred_sym_Wh"] = cumulative_trapezoid(df.Ppred_sym, rel_time, initial=0) / 3600
    # df["Epred_asym_Wh"] = cumulative_trapezoid(df.Ppred_asym, rel_time, initial=0) / 3600

    # last_energy = df.energy_Wh.dropna().iloc[-1]

    # def pcnt_err(measure: float):
    #     """Compute the percent of error based on energy consumption

    #     Args:
    #         x (float): Measured value

    #     Returns:
    #         float: Error percentage
    #     """
    #     return (measure - last_energy) / last_energy

    # gora_values = {}
    # gora_values["coefficients"] = coefficients
    # gora_values["errors"] = {
    #     "asym": pcnt_err(df.Epred_asym_Wh.tail(1).item()),
    #     "sym": pcnt_err(df.Epred_sym_Wh.tail(1).item()),
    # }

    # ssmru.model_values_to_json(abbr, __MODEL_NAME__, gora_values)

    # # Export csv
    # model_name = __MODEL_NAME__.lower()

    # filename = u.get_filename(abbr)
    # basename = u.get_basename(filename)
    # model_path = u.get_rundir(basename, subdir="models")
    # export_csv_path = model_path / f"{basename}_{model_name}.csv"
    # df.to_csv(export_csv_path, index=False)
    # print(f"{abbr} : Exported {__MODEL_NAME__} CSV in {export_csv_path}")

    df.meta.models.append(__MODEL_NAME__)

    return df
