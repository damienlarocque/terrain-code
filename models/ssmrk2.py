from enum import Enum
from typing import Dict, NamedTuple, Optional

import numpy as np
import pandas as pd
from scipy import optimize

import utils as u
from utils import imu_processing as imup
from utils import ssmr_utils as ssmru
from utils import ugv_utils as ugvu


class SSMRK_CONSTANTS(float, Enum):
    "SSMR constants"

    def __str__(self) -> str:
        return str(self.value)

    # Gravity constant
    g = 9.81
    # m (kg) from Baril2020
    m = 590
    # wheel dimensions in meters
    W = 1.2
    L = 0.9
    r = 0.3
    # N° Wheels
    N = 4
    # Half distances
    halfL = L / 2
    halfW = W / 2
    # Weight
    weight = m * g
    # Measurement noise in Watt
    measurement_noise = 50
    # EWMA lambda
    EWMA_lambda = 0.05


SSMR_CONST = SSMRK_CONSTANTS


def get_logical_power(basename: str, logical_idx: Optional[int] = None):
    """Get b int value"""

    # Get data
    df: pd.DataFrame = ssmru.get_combinedf(basename)

    assert "current" in df.columns.values, "No column in df with name : 'current'"

    abbr = u.abbreviate(basename)
    logical_idx = ssmru.logical_index_from_json(abbr)

    assert logical_idx <= len(df.index), ""
    log_curr = df.current[logical_idx]
    log_volt = df.voltage[logical_idx]

    log_pow = log_curr * log_volt

    ssmru.logical_to_json(
        abbr,
        {
            "current": log_curr,
            "voltage": log_volt,
            "power": log_pow,
            "index": str(logical_idx),
        },
    )

    return log_pow


def ssmrk2_main(abbr: str):
    """SSMR Power consumption from Pentzer & Gora"""

    df = ugvu.read_df_rename_columns(abbr)

    filename = u.get_filename(abbr)
    basename = u.get_basename(filename)
    rundir = u.get_rundir(basename)

    df["P_total"] = df.current * df.voltage
    df["v"] = np.sqrt(df.vx**2 + df.vy**2 + df.vz**2)
    df["a"] = np.sqrt(df.ax_corr**2 + df.ay_corr**2 + df.az_corr**2)

    P_Logical = get_logical_power(basename)
    df["P_logical"] = P_Logical
    df["P_motion"] = df.P_total - P_Logical

    # df["P_L"] = df.I_L * df.V_L
    # df["P_R"] = df.I_R * df.V_R

    df["ICRx"] = -df.vy / df.w_z
    df["ICRy"] = df.vx / df.w_z

    df["vL"] = df.wL * SSMR_CONST.r
    df["vR"] = df.wR * SSMR_CONST.r

    df["ICRyL"] = (df.vx - df.vL) / df.w_z
    df["ICRyR"] = (df.vx - df.vR) / df.w_z

    # df["normal_force"] = SSMR_CONST.weight * np.cos(df.theta) / SSMR_CONST.N
    df["normal_force"] = SSMR_CONST.weight * np.cos(df.theta) / SSMR_CONST.N

    df["Bs"] = (df.ICRyL - df.ICRyR).abs()

    df["R"] = df.Bs / 2 * ((df.vR + df.vL) / (df.vR - df.vL))

    df["dx_front"] = SSMR_CONST.halfL - df.ICRx
    df["dx_rear"] = SSMR_CONST.halfL + df.ICRx
    df["dy_left"] = SSMR_CONST.halfW - df.ICRyL
    df["dy_right"] = SSMR_CONST.halfW + df.ICRyR

    # Distance
    # * front-left
    # * front-right
    # * rear-left
    # * rear-right
    df["d_fl"] = np.sqrt(df.dx_front**2 + df.dy_left**2)
    df["d_fr"] = np.sqrt(df.dx_front**2 + df.dy_right**2)
    df["d_rl"] = np.sqrt(df.dx_rear**2 + df.dy_left**2)
    df["d_rr"] = np.sqrt(df.dx_rear**2 + df.dy_right**2)

    df["dist_asym"] = df.d_fl + df.d_fr + df.d_rl + df.d_rr
    df["dist_sym"] = np.sqrt(SSMR_CONST.L**2 + (SSMR_CONST.W - df.Bs) ** 2)

    df["JS_asym"] = df.w_z.abs() * df.normal_force * df.dist_asym
    df["JS_sym"] = 2 * df.w_z.abs() * df.normal_force * df.dist_sym

    df["JG"] = df.vL.abs() + df.vR.abs()

    # Pentzer 2022 RLS
    H_sym = df[["JS_sym", "JG"]].copy().to_numpy()
    H_asym = df[["JS_asym", "JG"]].copy().to_numpy()

    y_meas = df.P_motion.copy()
    r_k = SSMR_CONST.measurement_noise

    class IterDat(NamedTuple):
        """Save Iteration data"""

        P_sym: Dict[float, np.array]
        P_asym: Dict[float, np.array]
        x_sym: Dict[float, np.array]
        x_asym: Dict[float, np.array]
        H_sym: np.array
        H_asym: np.array

        def results(self) -> pd.DataFrame:
            n = self.H_sym.shape[0]
            x_sym = {idx: v[:, 0] for idx, v in self.x_sym.items()}
            x_asym = {idx: v[:, 0] for idx, v in self.x_asym.items()}
            P_diag_sym = {idx: v.diagonal() for idx, v in self.P_sym.items()}
            P_diag_asym = {idx: v.diagonal() for idx, v in self.P_asym.items()}
            d = {
                "m_asym": pd.Series([x_asym[idx][0] for idx in range(n)]),
                "G_asym": pd.Series([x_asym[idx][1] for idx in range(n)]),
                "m_sym": pd.Series([x_sym[idx][0] for idx in range(n)]),
                "G_sym": pd.Series([x_sym[idx][1] for idx in range(n)]),
                "covm_sym": pd.Series([P_diag_sym[idx][0] for idx in range(n)]),
                "covG_sym": pd.Series([P_diag_sym[idx][1] for idx in range(n)]),
                "covm_asym": pd.Series([P_diag_asym[idx][0] for idx in range(n)]),
                "covG_asym": pd.Series([P_diag_asym[idx][1] for idx in range(n)]),
            }
            return pd.DataFrame(data=d)

    def cov_mat():
        C = 100 * np.eye(2)
        # C[1, 1] = 20
        return C

    pentzer_iters = IterDat(
        P_sym={-1: cov_mat()},
        P_asym={-1: cov_mat()},
        x_sym={-1: np.array([[0.5, 250]]).T},
        x_asym={-1: np.array([[0.5, 250]]).T},
        H_sym=H_sym,
        H_asym=H_asym,
    )

    # x_sym = x_asym = np.array([[0.5, 10, 1]]).T
    # P_sym = P_asym = 100 * np.eye(3)

    def pentzer_rls(i: int, mode: str = "sym"):
        H = getattr(pentzer_iters, f"H_{mode}")
        P = getattr(pentzer_iters, f"P_{mode}")[idx - 1]
        x = getattr(pentzer_iters, f"x_{mode}")[idx - 1]

        Hk = H[i, np.newaxis]
        yk = y_meas.iloc[i]

        # Kk
        Kk = P @ Hk.T * (Hk @ P @ Hk.T + r_k) ** -1

        # Pk
        IKH = np.eye(2) - Kk * Hk
        Pk = IKH @ P @ IKH.T + Kk @ np.array([[r_k]]) @ Kk.T

        # xk
        xk = x + Kk @ (yk - Hk @ x)

        getattr(pentzer_iters, f"P_{mode}")[idx] = Pk
        getattr(pentzer_iters, f"x_{mode}")[idx] = xk

    for idx in df.index:
        pentzer_rls(idx, mode="sym")
        pentzer_rls(idx, mode="asym")

    gora_df = df.join(pentzer_iters.results())

    # Predict power and compute power error
    # df[["JS_sym", "JG"]]
    PS_sym = gora_df.m_sym * gora_df.JS_sym
    PG_sym = gora_df.G_sym * gora_df.JG
    gora_df["P_pred_t"] = PS_sym + PG_sym
    # Then
    gora_df["P_err_t"] = gora_df.P_motion - gora_df.P_pred_t
    gora_df["EWMA_t"] = gora_df.P_err_t.ewm(alpha=SSMR_CONST.EWMA_lambda).mean()
    gora_df["estimator_t"] = gora_df.EWMA_t.cumsum()

    PS_sym = gora_df.m_sym.dropna().iloc[-1] * gora_df.JS_sym
    PG_sym = gora_df.G_sym.dropna().iloc[-1] * gora_df.JG
    gora_df["P_pred_conv"] = PS_sym + PG_sym
    gora_df["P_err_conv"] = gora_df.P_motion - gora_df.P_pred_conv
    gora_df["EWMA_conv"] = gora_df.P_err_conv.ewm(alpha=SSMR_CONST.EWMA_lambda).mean()
    gora_df["estimator_conv"] = gora_df.EWMA_conv.cumsum()

    ssmru.ssmrcoeffs_to_json(abbr, gora_df)

    # Giguère 2022 ¯\_(ツ)_/¯
    H_sym = df[["JS_sym", "JG"]].copy().to_numpy()
    x_sym = np.array([0.5, 250])
    P_meas = gora_df.P_motion.copy().to_numpy()

    def power_estimate_error(x: np.ndarray, H: np.ndarray, P_measured: np.ndarray):
        P_est = H @ x
        P_err = P_est - P_measured
        P_err_squared = P_err**2
        return P_err_squared.sum()

    optimized = optimize.fmin(power_estimate_error, x0=x_sym, args=(H_sym, P_meas), full_output=True)
    xopt, fopt, *_ = optimized

    gig_opt = {"mu": xopt[0], "G": xopt[1], "err": fopt}
    ssmru.fmin_to_json(abbr, gig_opt)

    # Export csv
    merged_path = rundir / "ssmrk"
    gora_csv_path = merged_path / f"{basename}_ssmrk_gora.csv"
    gora_df.to_csv(gora_csv_path, index=False)
    print(f"{abbr} : Exported SSMR kinematics CSV in {gora_csv_path}")

    return gora_df, gig_opt


def naive_ssmrk(abbr: str):
    """(Obsolete) Naive computation of SSMRK parameters, uses Least Squares Correlation, but doesn't converge on some data"""

    df = open_and_prepare_ssmrk_df(abbr)

    filename = u.get_filename(abbr)
    basename = u.get_basename(filename)
    rundir = u.get_rundir(basename)

    df["P_total"] = df.current * df.voltage
    df["v"] = np.sqrt(df.vx**2 + df.vy**2 + df.vz**2)
    df["a"] = np.sqrt(df.ax_corr**2 + df.ay_corr**2 + df.az_corr**2)

    P_Logical = get_logical_power(basename)
    df["P_motion"] = df.P_total - P_Logical

    # df["P_L"] = df.I_L * df.V_L
    # df["P_R"] = df.I_R * df.V_R

    df["ICRx"] = -df.vy / df.w_z
    df["ICRy"] = df.vx / df.w_z

    df["vL"] = df.wL * SSMR_CONST.r
    df["vR"] = df.wR * SSMR_CONST.r

    df["ICRyL"] = (df.vx - df.vL) / df.w_z
    df["ICRyR"] = (df.vx - df.vR) / df.w_z

    # df["normal_force"] = SSMR_CONST.weight * np.cos(df.theta) / SSMR_CONST.N
    df["normal_force"] = SSMR_CONST.weight * np.cos(df.theta) / SSMR_CONST.N

    df["Bs"] = (df.ICRyL - df.ICRyR).abs()

    df["dx_front"] = SSMR_CONST.halfL - df.ICRx
    df["dx_rear"] = SSMR_CONST.halfL + df.ICRx
    df["dy_left"] = SSMR_CONST.halfW - df.ICRyL
    df["dy_right"] = SSMR_CONST.halfW + df.ICRyR

    # Distance
    # * front-left
    # * front-right
    # * rear-left
    # * rear-right
    df["d_fl"] = np.sqrt(df.dx_front**2 + df.dy_left**2)
    df["d_fr"] = np.sqrt(df.dx_front**2 + df.dy_right**2)
    df["d_rl"] = np.sqrt(df.dx_rear**2 + df.dy_left**2)
    df["d_rr"] = np.sqrt(df.dx_rear**2 + df.dy_right**2)

    df["dist_asym"] = df.d_fl + df.d_fr + df.d_rl + df.d_rr
    df["dist_sym"] = np.sqrt(SSMR_CONST.L**2 + (SSMR_CONST.W - df.Bs) ** 2)

    df["JS_asym"] = df.w_z.abs() * df.normal_force * df.dist_asym
    df["JS_sym"] = 2 * df.w_z.abs() * df.normal_force * df.dist_sym

    df["JG"] = df.vL.abs() + df.vR.abs()
    df["JB"] = SSMR_CONST.weight * np.sin(df.theta) * df.vx

    Jasym_arr = df[["JS_asym", "JG", "JB"]].copy().to_numpy()
    Jsym_arr = df[["JS_sym", "JG", "JB"]].copy().to_numpy()

    P_Motion = df.P_motion.copy().to_numpy()

    # Jasym_arr = np.vstack([np.zeros((3,)), Jasym_arr, np.zeros((3,))])
    # Jsym_arr = np.vstack([np.zeros((3,)), Jsym_arr, np.zeros((3,))])
    # P_Motion = np.concatenate([[0], P_Motion, [0]])

    def solve_params(idx: int):
        A_asym = Jasym_arr[: idx + 1, :]
        A_sym = Jsym_arr[: idx + 1, :]
        B = P_Motion[: idx + 1]

        def try_solve(A_mat, B_mat):
            # try:
            #     X = np.linalg.solve(A_mat, B_mat)
            # except np.linalg.LinAlgError:
            #     X = np.linalg.lstsq(A_mat, B_mat, rcond=None)[0]
            # return X
            X, r, rk, s = np.linalg.lstsq(A_mat, B_mat, rcond=None)
            return X

        x_asym = try_solve(A_asym, B)
        x_sym = try_solve(A_sym, B)

        d_asym = dict(
            zip(
                ("mu_asym", "G_asym", "B_asym"),
                x_asym,
            )
        )
        d_sym = dict(
            zip(
                ("mu_sym", "G_sym", "B_sym"),
                x_sym,
            )
        )

        return {**d_asym, **d_sym}

    df["ssmr_coeffs"] = pd.Series({idx: solve_params(idx) for idx in df.index})
    coeffs_df = pd.DataFrame(df.ssmr_coeffs.to_dict()).T

    ssmrk_df = df.join(coeffs_df).drop(columns=["ssmr_coeffs"])
    ssmru.ssmrcoeffs_to_json(abbr, ssmrk_df)

    # Export csv
    merged_path = rundir / "ssmrk"
    ssmrk_csv_path = merged_path / f"{basename}_ssmrk.csv"
    ssmrk_df.to_csv(ssmrk_csv_path, index=False)
    print(f"{abbr} : Exported SSMR kinematics CSV in {ssmrk_csv_path}")

    return ssmrk_df
