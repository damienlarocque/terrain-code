class A:
    def __init__(self) -> None:
        print(self.var)

    def __str__(self) -> str:
        return self.var


class B(A):
    var = "I'm in B"


class C(A):
    var = "I'm in C"


if __name__ == "__main__":
    b = B()
    print(b)
    c = C()
    print(c)
