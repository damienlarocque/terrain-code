from pathlib import Path
from typing import Tuple

import papermill as pm

directory = Path(__file__).parent
input_dir = directory.parent


def abbrange(letter: str, a: int, b: int) -> Tuple[str]:
    return (f"{letter}{i:02}" for i in range(a, b))


abbrs = (
    # "H01",
    # "H02",
    # "H03",
    # "H04",
    # "H05",
    # "H06",
    # "H07",
    # "H08",
    # "H09",
    # "H10",
    # "H11",
    # "H12",
    # "H13",
    # "H14",
    # "S01",
    # "S02",
    # "S03",
    # "S04",
    # "S05",
    # "S06",
    # "S07",
    # "S08",
    # "S09",
    # "S10",
    # "S28",
    # "S29",
    # "S30",
    # "S31",
    # "D01",
    # "D02",
    # "D05",
    # "D11",
    # "D18",
    # "D19",
    # "D20",
    # "D21",
    # "D22",
)
# abbrs = (f"M{i:02}" for i in range(1, 8))
# abbrs = ("M01", "M02", "M03", "M04", "M05", "M06", "M07")
# abbrs += (
#     "S01",
#     "S02",
#     "S03",
#     "S04",
#     "S05",
#     "S06",
#     "S07",
#     "S08",
#     "S09",
#     "S10",
#     "S21",
#     "S22",
#     "S23",
#     "S28",
#     "S29",
#     "S30",
#     "S31",
# )
# # abbrs += ("TB",)
# abbrs += ("R02", "R03", "R09")
# abbrs += (
#     "D01",
#     "D02",
#     "D03",
#     "D04",
#     "D05",
#     "D06",
#     "D09",
#     "D11",
#     "D12",
#     "D13",
#     "D14",
#     "D16",
#     "D18",
#     "D19",
#     "D20",
#     "D21",
#     "D22",
# )
abbrs = tuple(abbrange("H", 1, 15))
# abbrs = tuple(abbr for abbr in abbrs if abbr not in ["H02", "H03", "H04", "H06", "H07", "H09", "H11"])
# abbrs = tuple(abbrange("H", 1, 37))
# abbrs = tuple(abbrange("Y", 1, 21))
h_abbrs = tuple(abbrange("H", 1, 37))
y_abbrs = tuple(abbrange("Y", 1, 21))
abbrs = (*h_abbrs, *y_abbrs)
print(abbrs)
for abbr in abbrs:
    print(abbr)
    try:
        # pm.execute_notebook(
        #     input_dir / "pentzer2022.ipynb",
        #     directory / "pentzer2022_out.ipynb",
        #     parameters={"abbr": abbr},
        # )
        pm.execute_notebook(
            input_dir / "dcmotmod.ipynb",
            directory / "dcmotmod_out.ipynb",
            parameters={"abbr": abbr},
        )
    except KeyError as e:
        print(e)
        continue
    except pm.exceptions.PapermillExecutionError as e:
        print(e)
        continue
    # pm.execute_notebook(
    #     input_dir / "ssmrk.ipynb",
    #     directory / "pm_ssmrk_out.ipynb",
    #     parameters={"abbr": abbr},
    # )
    # pm.execute_notebook(
    #     input_dir / "ssmrk_coeffs.ipynb",
    #     directory / "pm_ssmrkcoeff_out.ipynb",
    #     parameters={"abbr": abbr},
    # )
    # pm.execute_notebook(
    #     input_dir / "ssmrk2_coeffs.ipynb",
    #     directory / "pm_ssmrkcoeff2_out.ipynb",
    #     parameters={"abbr": abbr},
    # )
    # pm.execute_notebook(
    #     input_dir / "ssmrk_covariance.ipynb",
    #     directory / "pm_ssmrkcov_out.ipynb",
    #     parameters={"abbr": abbr},
    # )
    # pm.execute_notebook(
    #     input_dir / "ssmrk_prederror.ipynb",
    #     directory / "pm_ssmrkerr_out.ipynb",
    #     parameters={"abbr": abbr},
    # )
    # pm.execute_notebook(
    #     input_dir / "ssmrk_fiset.ipynb",
    #     directory / "pm_ssmrkfiset_out.ipynb",
    #     parameters={"abbr": abbr},
    # )
    # pm.execute_notebook(
    #     input_dir / "rotationnality.ipynb",
    #     directory / "pm_rotationnality_out.ipynb",
    #     parameters={"abbr": abbr},
    # )
    # pm.execute_notebook(
    #     input_dir / "ssmrk_fiset.ipynb",
    #     directory / "pm_ssmrkfiset_out.ipynb",
    #     parameters={"abbr": abbr},
    # )
