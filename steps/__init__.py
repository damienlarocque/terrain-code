import steps.husky.filtering as husky_filt
import steps.husky.merge as husky_merge
import steps.warthog.filtering as warthog_filt
import steps.warthog.merge as warthog_merge

# import steps.marmotte.filtering as marmotte_filt
# import steps.marmotte.merge as marmotte_merge
import utils as u


def preprocess(abbreviation: str, verbose: bool = True):
    if verbose:
        print(abbreviation)
    filename = u.get_filename(abbreviation)
    basename = u.get_basename(filename)
    ugv_name = u.get_ugv(abbreviation)

    if ugv_name.lower() == "warthog":
        merge = warthog_merge
        filtering = warthog_filt
        merge.merge_motor_data(basename, verbose=verbose)
    elif ugv_name.lower() == "husky":
        merge = husky_merge
        filtering = husky_filt
    else:
        raise NotImplementedError(
            f"Cannot process data for ugv '{ugv_name}'",
        )

    merge.combine_csv(basename, verbose=verbose)
    filtering.sanitize_csv(basename, verbose=verbose)


__all__ = ("preprocess",)
