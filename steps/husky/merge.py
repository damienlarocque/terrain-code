from pathlib import Path
from typing import Union

import numpy as np
import pandas as pd
import utils as u
from pandas.errors import EmptyDataError
from scipy import interpolate as interp
from utils import gps_processing as gpsp
from utils import timeseries_processing as timep

from steps.husky import merge_checks as mc


def get_fname(basename: str, data_type: str, topic: str) -> str:
    return f"{basename}_{data_type}_{topic}.csv"


def combine_csv(basename: Union[Path, str], verbose: bool = True):
    rundir = u.get_rundir(basename)
    merged_path = rundir / "merged"
    merged_path.mkdir(parents=True, exist_ok=True)

    imu_data = rundir / "imu"
    elec_data = rundir / "elec"
    odom_data = rundir / "odom"

    abbr = u.abbreviate(basename)

    dfs_paths = {
        "joint_states": odom_data / f"{basename}_odom_joint_states.csv",
        "elec": elec_data / f"{basename}_elec_status.csv",
        "imu_data": imu_data / f"{basename}_imu_MTI_imu_data.csv",
        "imu_wheel_odom": imu_data / f"{basename}_imu_imu_and_wheel_odom.csv",
        # "nav_vel": odom_data / f"{basename}_odom_nav_vel.csv",
        "cmd_vel": odom_data / f"{basename}_odom_husky_velocity_controller_cmd_vel_unstamped.csv",
    }

    topics_dfs = {}
    for top, path in dfs_paths.items():
        # print(top, str(path))
        try:
            topics_dfs[top] = pd.read_csv(path)
        except EmptyDataError as error:
            print(error)
            print(top, str(path))

    # Get primary topic based on message frequencies and merge mode
    # Mean : use lowest frequency (min)
    frequencies = {topic: timep.freq(df) for topic, df in topics_dfs.items()}
    prim_topic = min(frequencies.items(), key=lambda x: x[1])[0]
    # prim_topic = "status"
    # prim_topic = min(frequencies.items(), key=lambda x: abs(x[1] - 1))[0]
    if verbose:
        print("Primary topic :", prim_topic)
    # print(len(topics_dfs["mcu_status"].index), len(topics_dfs["motor_data"].index))
    # Floor of time for each df
    for df in topics_dfs.values():
        df["time_floor"] = np.floor(df["time"] * 10)

    prim_df = topics_dfs[prim_topic].copy()
    mc.check_mean_duplicates(prim_df, prim_topic)

    repl_dfs = {top: df for top, df in topics_dfs.items() if top != prim_topic}
    grouped_dfs = {top: orig_df.groupby(["time_floor"]).median() for top, orig_df in repl_dfs.items()}
    for top, gp_df in grouped_dfs.items():
        # Change time to topic_time
        gp_df[f"{top}_time"] = grouped_dfs[top]["time"]
        gp_df.drop("time", axis=1, inplace=True)
        gp_df["time_floor"] = grouped_dfs[top].index
        gp_df.index = pd.RangeIndex(len(grouped_dfs[top].index))
        imp_cols = [f"{top}_time", "time_floor"]
        grouped_dfs[top] = gp_df[imp_cols + [c for c in gp_df if c not in imp_cols]]

    # rows_dfs = [df for top, df in grouped_dfs.items() if "gps" not in top] + [prim_df]
    # mc.check_mean_rows(rows_dfs)

    for repl_df in grouped_dfs.values():
        prim_df = pd.merge(prim_df, repl_df, on="time_floor", how="left")

    merged_csv_path = merged_path / f"{basename}_m.csv"
    prim_df.to_csv(merged_csv_path, index=False)
    runo = u.abbreviate(basename)
    if verbose:
        print(f"{runo} : Exported merged CSV in {merged_csv_path}")


def combine_csv_interpolation(basename: Union[Path, str]):
    """(Obsolete) Old method to merge timeseries CSVs"""

    rundir = u.get_rundir(basename)
    merged_path = rundir / "merged"
    merged_path.mkdir(parents=True, exist_ok=True)

    imu_data = rundir / "imu"
    gps_data = rundir / "gps"
    elec_data = rundir / "elec"

    dfs_paths = {
        "warthog_velocity_controller_odom": imu_data / f"{basename}_imu_warthog_velocity_controller_odom.csv",
        "imu_data": imu_data / f"{basename}_imu_MTI_imu_data.csv",
        "imu_and_wheel_odom": imu_data / f"{basename}_imu_imu_and_wheel_odom.csv",
        "mcu_status": elec_data / f"{basename}_elec_mcu_status.csv",
        "gps_coords_front": gps_data / f"{basename}_gps_gps_coords_front.csv",
        "gps_coords_back": gps_data / f"{basename}_gps_gps_coords_back.csv",
    }
    topics_dfs = {top: pd.read_csv(path, index_col=[0]) for top, path in dfs_paths.items()}

    # Differentiate geodesic columns
    for sd in ("front", "back"):
        df = topics_dfs[f"gps_coords_{sd}"]
        renamed_columns = {geodata: f"{sd}_{geodata}" for geodata in ("latitude", "longitude", "altitude")}
        topics_dfs[f"gps_coords_{sd}"] = df.rename(columns=renamed_columns, errors="raise")

    # Get primary topic based on message frequencies and merge mode
    # Interpolation : use highest frequency (max)
    frequencies = {topic: timep.freq(df) for topic, df in topics_dfs.items()}
    prim_topic = max(frequencies.items(), key=lambda x: x[1])[0]

    prim_df = topics_dfs[prim_topic].copy()
    repl_dfs = {top: df.copy() for top, df in topics_dfs.items() if top != prim_topic}

    for top, repl_df in repl_dfs.items():
        # Save times in specifically topic named columns
        renamed_columns = {"time": f"{top}_time"}
        repl_df.rename(columns=renamed_columns, errors="raise", inplace=True)

    interp_dfs = {}
    for top, repl_df in repl_dfs.items():
        time_topic = f"{top}_time"
        cols = [c for c in list(repl_df)]

        # Time column
        t = repl_df[time_topic]
        # Interpolation function
        fs = {c: interp.interp1d(t, repl_df[c], fill_value="extrapolate") for c in cols}

        # Interpolated df
        int_df = prim_df[["time"]].copy()

        # For each function
        for c, func in fs.items():
            int_df[c] = func(int_df.time)

        interp_dfs[top] = int_df.copy()

    mc.check_interp_time(prim_df, interp_dfs)
    mc.check_interp_rows(prim_df, interp_dfs)

    for repl_df in interp_dfs.values():
        prim_df = pd.merge(prim_df, repl_df, on="time", how="left")

    merged_csv_path = merged_path / f"{basename}_m.csv"
    prim_df.to_csv(merged_csv_path, index=False)
    runo = u.abbreviate(basename)
    print(f"{runo} : Exported merged CSV in {merged_csv_path}")
