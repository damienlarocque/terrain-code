from typing import Union

import numpy as np
import pandas as pd
from utils import timeseries_processing as timep


#
# Mean mode checks
#
def check_mean_rows(dfs: Union[list, pd.DataFrame]):
    """Check that the number of rows is coherent"""
    if isinstance(dfs, pd.DataFrame):
        dfs = [dfs]
    rows = [timep.n_rows(df) for df in dfs]
    if len(set(rows)) != 1:
        import warnings

        warnings.warn("Number of rows in DataFrames is not coherent. Merged df will contain nan values")


def check_mean_duplicates(prim_df: pd.DataFrame, prim_topic: str):
    dupl_prim_time = prim_df["time_floor"].duplicated().any()
    if dupl_prim_time:
        dupl_time = prim_df["time_floor"][prim_df["time_floor"].duplicated(keep=False)]
        dupl_idx = dupl_time.index.tolist()
        if sorted(dupl_idx) == [0, 1]:
            # Remove first row
            prim_df = prim_df.iloc[1:, 1]
            prim_df.index = pd.RangeIndex(len(prim_df.index))
        else:
            print(dupl_time)
            raise ValueError(
                f"Pandas dataframe for {prim_topic} has duplicate time values in indices {dupl_idx} and times {dupl_time.tolist()}"
            )


#
# Interpolation mode checks
#
def check_interp_time(prim_df: pd.DataFrame, second_dfs: dict):
    coherent_time = [np.all(df[f"{top}_time"] == prim_df.time) for top, df in second_dfs.items()]
    if not all(coherent_time):
        import warnings

        warnings.warn("Time topics are not coherent. Merging dfs can raise Exceptions")


def check_interp_rows(prim_df: pd.DataFrame, second_dfs: dict):
    coher_rows = [timep.n_rows(df) for df in second_dfs.values()] + [timep.n_rows(prim_df)]
    if len(set(coher_rows)) != 1:
        import warnings

        warnings.warn("Time topics are not coherent. Merging dfs can raise Exceptions")
