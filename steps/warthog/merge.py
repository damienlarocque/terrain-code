from pathlib import Path
from typing import Union

import numpy as np
import pandas as pd
import utils as u
from pandas.errors import EmptyDataError
from scipy import interpolate as interp
from utils import gps_processing as gpsp
from utils import timeseries_processing as timep

from steps.warthog import merge_checks as mc


def merge_motor_data(basename: Union[Path, str], verbose: bool = True):
    rundir = u.get_rundir(basename)
    elec_data = rundir / "elec"

    dfs_paths = {
        "left_current": elec_data / f"{basename}_elec_left_drive_status_battery_current.csv",
        "left_voltage": elec_data / f"{basename}_elec_left_drive_status_battery_voltage.csv",
        "right_current": elec_data / f"{basename}_elec_right_drive_status_battery_current.csv",
        "right_voltage": elec_data / f"{basename}_elec_right_drive_status_battery_voltage.csv",
    }
    motor_dfs = {top: pd.read_csv(path) for top, path in dfs_paths.items()}

    VL = motor_dfs["left_voltage"]
    VR = motor_dfs["right_voltage"]

    IL = motor_dfs["left_current"]
    IR = motor_dfs["right_current"]

    elec_tops = {
        "IL": "/left_drive/status/battery_current",
        "VL": "/left_drive/status/battery_voltage",
        "IR": "/right_drive/status/battery_current",
        "VR": "/right_drive/status/battery_voltage",
        "IL_signed": "/left_drive/status/battery_current_signed",
        "IR_signed": "/right_drive/status/battery_current_signed",
    }

    u.sign_warthog_current(IL, elec_tops["IL"])
    u.sign_warthog_current(IR, elec_tops["IR"])

    def best_join_time_idx(idx: int, prim_df: pd.DataFrame, seco_df: pd.DataFrame):
        """Get to secondary time from first time"""
        prim_time = prim_df.time
        seco_time = seco_df.time

        best_idx = (seco_time - prim_time[idx]).abs().argmin()
        return best_idx

    L_mot = IL.copy()
    R_mot = IR.copy()

    # Which time is best on VL and VR to merge with IL and IR
    L_IV_idx = pd.Series({idx: best_join_time_idx(idx, L_mot, VL) for idx in L_mot.index})
    R_IV_idx = pd.Series({idx: best_join_time_idx(idx, R_mot, VR) for idx in R_mot.index})

    # Add VL and VR to IL and IR
    L_mot[elec_tops["VL"]] = pd.Series({idx: VL[elec_tops["VL"]].iloc[L_IV_idx.iloc[idx]] for idx in L_mot.index})
    R_mot[elec_tops["VR"]] = pd.Series({idx: VR[elec_tops["VR"]].iloc[R_IV_idx.iloc[idx]] for idx in R_mot.index})

    full_mot = L_mot.copy()
    # Time to merge R in L. L + R = full_mot
    LR_idx = pd.Series({idx: best_join_time_idx(idx, full_mot, R_mot) for idx in full_mot.index})

    # Copy right topics to full motor
    full_mot[elec_tops["IR"]] = pd.Series(
        {idx: R_mot[elec_tops["IR"]].iloc[LR_idx.iloc[idx]] for idx in full_mot.index}
    )
    full_mot[elec_tops["IR_signed"]] = pd.Series(
        {idx: R_mot[elec_tops["IR_signed"]].iloc[LR_idx.iloc[idx]] for idx in full_mot.index}
    )
    full_mot[elec_tops["VR"]] = pd.Series(
        {idx: R_mot[elec_tops["VR"]].iloc[LR_idx.iloc[idx]] for idx in full_mot.index}
    )

    motor_csv_path = elec_data / f"{basename}_elec_motor_data.csv"
    full_mot.to_csv(motor_csv_path, index=False)
    runo = u.abbreviate(basename)
    if verbose:
        print(f"{runo} : Exported merged CSV in {motor_csv_path}")


def combine_csv(basename: Union[Path, str], verbose: bool = True):

    rundir = u.get_rundir(basename)
    merged_path = rundir / "merged"
    merged_path.mkdir(parents=True, exist_ok=True)

    imu_data = rundir / "imu"
    gps_data = rundir / "gps"
    elec_data = rundir / "elec"
    odom_data = rundir / "odom"
    temp_data = rundir / "temp"

    front_coords_path = gps_data / f"{basename}_gps_gps_coords_front.csv"
    if not front_coords_path.exists():
        back_coords_path = gpsp.fix_to_coords(basename, side="back")
        front_coords_path = gpsp.fix_to_coords(basename, side="front")

    abbr = u.abbreviate(basename)

    dfs_paths = {
        "warthog_velocity_controller_odom": odom_data / f"{basename}_odom_warthog_velocity_controller_odom.csv",
        "imu_data": imu_data / f"{basename}_imu_MTI_imu_data.csv",
        "imu_and_wheel_odom": imu_data / f"{basename}_imu_imu_and_wheel_odom.csv",
        "mcu_status": elec_data / f"{basename}_elec_mcu_status.csv",
        "vel_left": odom_data / f"{basename}_odom_left_drive_status_speed.csv",
        "vel_right": odom_data / f"{basename}_odom_right_drive_status_speed.csv",
        "motor_data": elec_data / f"{basename}_elec_motor_data.csv",
        "cmd_vel": odom_data / f"{basename}_odom_cmd_vel.csv",
        "left_temp": temp_data / f"{basename}_temp_left_drive_status_motor_temperature.csv",
        "right_temp": temp_data / f"{basename}_temp_right_drive_status_motor_temperature.csv",
    }

    if front_coords_path is not None:
        dfs_paths["gps_coords_front"] = gps_data / f"{basename}_gps_gps_coords_front.csv"
        dfs_paths["gps_coords_back"] = gps_data / f"{basename}_gps_gps_coords_back.csv"

    if abbr in ("TA", "TB", "TC"):
        dfs_paths["imu_and_wheel_odom"] = odom_data / f"{basename}_odom_warthog_velocity_controller_odom.csv"
        dfs_paths["cmd_vel"] = odom_data / f"{basename}_odom_warthog_velocity_controller_cmd_vel.csv"
        # if abbr in ("TA",):
        #     dfs_paths["cmd_vel"] = odom_data / f"{basename}_odom_hri_cmd_vel.csv"

    D_abbrs = ("D15", "D16", "D17", "D18", "D19", "D20", "D21", "D22", "D23")
    if abbr in D_abbrs or abbr.startswith("S") or abbr.startswith("M"):
        dfs_paths["cmd_vel"] = odom_data / f"{basename}_odom_warthog_velocity_controller_cmd_vel.csv"

    topics_dfs = {}
    for top, path in dfs_paths.items():
        # print(top, str(path))
        try:
            topics_dfs[top] = pd.read_csv(path)
        except EmptyDataError as error:
            print(error)
            print(top, str(path))
    # topics_dfs = {top: pd.read_csv(path) for  path in top, path in dfs_paths.items()}

    # if "warthog" in str(dfs_paths["cmd_vel"]):
    #     warthog_rename = lambda s: s.replace("/warthog_velocity_controller/cmd_vel", "/cmd_vel")
    #     topics_dfs["cmd_vel"] = topics_dfs["cmd_vel"].rename(columns=warthog_rename)
    if "hri_cmd" in str(dfs_paths["cmd_vel"]):
        hri_rename = lambda s: s.replace("/hri_cmd_vel", "/cmd_vel")
        topics_dfs["cmd_vel"] = topics_dfs["cmd_vel"].rename(columns=hri_rename, errors="raise")
    if "warthog" in str(dfs_paths["imu_and_wheel_odom"]):
        warthog_rename = lambda s: s.replace("/warthog_velocity_controller/odom", "/imu_and_wheel_odom")
        topics_dfs["imu_and_wheel_odom"] = topics_dfs["imu_and_wheel_odom"].rename(
            columns=warthog_rename, errors="raise"
        )

    if front_coords_path is not None:
        # Differentiate geodesic columns
        for sd in ("front", "back"):
            df = topics_dfs[f"gps_coords_{sd}"]
            renamed_columns = {geodata: f"{sd}_{geodata}" for geodata in ("latitude", "longitude", "altitude")}
            topics_dfs[f"gps_coords_{sd}"] = df.rename(columns=renamed_columns, errors="raise")

    # Get primary topic based on message frequencies and merge mode
    # Mean : use lowest frequency (min)
    frequencies = {topic: timep.freq(df) for topic, df in topics_dfs.items()}
    # prim_topic = min(frequencies.items(), key=lambda x: x[1])[0]
    prim_topic = "mcu_status"
    # prim_topic = min(frequencies.items(), key=lambda x: abs(x[1] - 1))[0]
    if verbose:
        print("Primary topic :", prim_topic)
    # print(len(topics_dfs["mcu_status"].index), len(topics_dfs["motor_data"].index))
    # Floor of time for each df
    for df in topics_dfs.values():
        df["time_floor"] = np.floor(df["time"])

    prim_df = topics_dfs[prim_topic].copy()
    mc.check_mean_duplicates(prim_df, prim_topic)

    repl_dfs = {top: df for top, df in topics_dfs.items() if top != prim_topic}
    grouped_dfs = {top: orig_df.groupby(["time_floor"]).mean() for top, orig_df in repl_dfs.items()}
    for top, gp_df in grouped_dfs.items():
        # Change time to topic_time
        gp_df[f"{top}_time"] = grouped_dfs[top]["time"]
        gp_df.drop("time", axis=1, inplace=True)
        gp_df["time_floor"] = grouped_dfs[top].index
        gp_df.index = pd.RangeIndex(len(grouped_dfs[top].index))
        imp_cols = [f"{top}_time", "time_floor"]
        grouped_dfs[top] = gp_df[imp_cols + [c for c in gp_df if c not in imp_cols]]

    # rows_dfs = [df for top, df in grouped_dfs.items() if "gps" not in top] + [prim_df]
    # mc.check_mean_rows(rows_dfs)

    for repl_df in grouped_dfs.values():
        prim_df = pd.merge(prim_df, repl_df, on="time_floor", how="left")

    merged_csv_path = merged_path / f"{basename}_m.csv"
    prim_df.to_csv(merged_csv_path, index=False)
    runo = u.abbreviate(basename)
    if verbose:
        print(f"{runo} : Exported merged CSV in {merged_csv_path}")


def combine_csv_interpolation(basename: Union[Path, str]):
    """(Obsolete) Old method to merge timeseries CSVs"""

    rundir = u.get_rundir(basename)
    merged_path = rundir / "merged"
    merged_path.mkdir(parents=True, exist_ok=True)

    imu_data = rundir / "imu"
    gps_data = rundir / "gps"
    elec_data = rundir / "elec"

    dfs_paths = {
        "warthog_velocity_controller_odom": imu_data / f"{basename}_imu_warthog_velocity_controller_odom.csv",
        "imu_data": imu_data / f"{basename}_imu_MTI_imu_data.csv",
        "imu_and_wheel_odom": imu_data / f"{basename}_imu_imu_and_wheel_odom.csv",
        "mcu_status": elec_data / f"{basename}_elec_mcu_status.csv",
        "gps_coords_front": gps_data / f"{basename}_gps_gps_coords_front.csv",
        "gps_coords_back": gps_data / f"{basename}_gps_gps_coords_back.csv",
    }
    topics_dfs = {top: pd.read_csv(path, index_col=[0]) for top, path in dfs_paths.items()}

    # Differentiate geodesic columns
    for sd in ("front", "back"):
        df = topics_dfs[f"gps_coords_{sd}"]
        renamed_columns = {geodata: f"{sd}_{geodata}" for geodata in ("latitude", "longitude", "altitude")}
        topics_dfs[f"gps_coords_{sd}"] = df.rename(columns=renamed_columns, errors="raise")

    # Get primary topic based on message frequencies and merge mode
    # Interpolation : use highest frequency (max)
    frequencies = {topic: timep.freq(df) for topic, df in topics_dfs.items()}
    prim_topic = max(frequencies.items(), key=lambda x: x[1])[0]

    prim_df = topics_dfs[prim_topic].copy()
    repl_dfs = {top: df.copy() for top, df in topics_dfs.items() if top != prim_topic}

    for top, repl_df in repl_dfs.items():
        # Save times in specifically topic named columns
        renamed_columns = {"time": f"{top}_time"}
        repl_df.rename(columns=renamed_columns, errors="raise", inplace=True)

    interp_dfs = {}
    for top, repl_df in repl_dfs.items():
        time_topic = f"{top}_time"
        cols = [c for c in list(repl_df)]

        # Time column
        t = repl_df[time_topic]
        # Interpolation function
        fs = {c: interp.interp1d(t, repl_df[c], fill_value="extrapolate") for c in cols}

        # Interpolated df
        int_df = prim_df[["time"]].copy()

        # For each function
        for c, func in fs.items():
            int_df[c] = func(int_df.time)

        interp_dfs[top] = int_df.copy()

    mc.check_interp_time(prim_df, interp_dfs)
    mc.check_interp_rows(prim_df, interp_dfs)

    for repl_df in interp_dfs.values():
        prim_df = pd.merge(prim_df, repl_df, on="time", how="left")

    merged_csv_path = merged_path / f"{basename}_m.csv"
    prim_df.to_csv(merged_csv_path, index=False)
    runo = u.abbreviate(basename)
    print(f"{runo} : Exported merged CSV in {merged_csv_path}")
