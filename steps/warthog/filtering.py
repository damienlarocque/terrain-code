from pathlib import Path
from typing import Union

import pandas as pd
import utils as u
from utils import electrical_processing as elecp


def sanitize_csv(basename: Union[Path, str], verbose: bool = True):
    rundir = u.get_rundir(basename)
    merged_path = rundir / "merged"

    df_path = merged_path / f"{basename}_m.csv"
    combined_df: pd.DataFrame = pd.read_csv(df_path)

    elecp.warthog_pwr(combined_df)
    elecp.compute_energy(combined_df)

    spans = u.get_json_spans()
    span = spans[u.abbreviate(basename)]["span"]

    cutted_df: pd.DataFrame = combined_df.iloc[span["start"] : span["end"]].copy()

    cutted_df.index = pd.RangeIndex(len(cutted_df.index))
    cutted_df["time_rel"] = cutted_df["time"] - cutted_df["time"][0]

    gps_back_columns = [c for c in cutted_df.columns.values if c.startswith("back_")]
    gps_front_columns = [c for c in cutted_df.columns.values if c.startswith("front_")]
    gps_columns = [*gps_back_columns, *gps_front_columns]
    if len(gps_columns) > 0:
        cutted_df[gps_columns] = cutted_df[gps_columns].fillna(method="bfill")
        cutted_df[gps_columns] = cutted_df[gps_columns].fillna(method="ffill")

    cutted_csv_path = merged_path / f"{basename}_mc.csv"
    cutted_df.to_csv(cutted_csv_path, index=False)
    runo = u.abbreviate(basename)
    if verbose:
        print(f"{runo} : Exported cut CSV in {cutted_csv_path}")

    const_cols = cutted_df.columns[cutted_df.nunique() <= 1].values
    if verbose:
        print(f"Constant columns : {len(const_cols)}")
        print(*const_cols.tolist(), sep="\n")
    const_cols = [c for c in const_cols if not "lin_twi" in c]
    if "/warthog_velocity_controller/cmd_vel/angular/z" in const_cols:
        const_cols.remove("/warthog_velocity_controller/cmd_vel/angular/z")
    if "/warthog_velocity_controller/cmd_vel/linear/x" in const_cols:
        const_cols.remove("/warthog_velocity_controller/cmd_vel/linear/x")

    filt_df = cutted_df.drop(columns=const_cols)
    filt_csv_path = merged_path / f"{basename}_mcf.csv"
    filt_df.to_csv(filt_csv_path, index=False)
    runo = u.abbreviate(basename)
    if verbose:
        print(f"{runo} : Exported filt CSV in {filt_csv_path}")
