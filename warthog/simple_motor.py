import pandas as pd
from scipy import interpolate as interp

import utils as u
from models.pentzer2022 import SSMRK_CONSTANTS as UGV_CONST

filename = u.get_filename("P11")
basename = u.get_basename(filename)
abbr = u.abbreviate(filename)

rundir = u.get_rundir(basename)
merged_path = rundir / "simple_motor"
merged_path.mkdir(parents=True, exist_ok=True)

elec_data = rundir / "elec"
odom_data = rundir / "odom"

radius = UGV_CONST.r

df_paths = dict(
    wL=odom_data / f"{basename}_odom_left_drive_velocity.csv",
    wR=odom_data / f"{basename}_odom_right_drive_velocity.csv",
    I_L=elec_data / f"{basename}_elec_left_drive_status_battery_current.csv",
    I_R=elec_data / f"{basename}_elec_right_drive_status_battery_current.csv",
    V_L=elec_data / f"{basename}_elec_left_drive_status_battery_voltage.csv",
    V_R=elec_data / f"{basename}_elec_right_drive_status_battery_voltage.csv",
)

dfs = {top: pd.read_csv(path) for top, path in df_paths.items()}

elec_topics = {
    "I_L": "/left_drive/status/battery_current",
    "I_R": "/right_drive/status/battery_current",
    "V_L": "/left_drive/status/battery_voltage",
    "V_R": "/right_drive/status/battery_voltage",
}

# Rename each df
for acr, df in dfs.items():
    if not acr in elec_topics:
        continue
    top = elec_topics[acr]
    dfs[acr] = df.rename(columns={top: acr}, errors="raise")

V_L = dfs["V_L"]
V_R = dfs["V_R"]
I_L = dfs["I_L"]
I_R = dfs["I_R"]


def remove_outliers(data: pd.DataFrame, topic: str, base_column: str = "time", threshold: int = 3000):
    corrected_df = data.copy()
    mask = corrected_df[topic] < threshold

    y_mask = corrected_df[topic][mask]
    x_mask = corrected_df[base_column][mask]

    x = corrected_df[base_column]

    f = interp.interp1d(x_mask, y_mask, fill_value="extrapolate")
    corrected_df[topic] = y = f(x)

    return corrected_df


I_L = remove_outliers(I_L, "I_L")
I_R = remove_outliers(I_R, "I_R")


def best_join_time_idx(idx: int, prim_df: pd.DataFrame, seco_df: pd.DataFrame):
    """Get to secondary time from first time"""
    prim_time = prim_df.time
    seco_time = seco_df.time

    best_idx = (seco_time - prim_time[idx]).abs().argmin()
    return best_idx


VI_L_idx = pd.Series({idx: best_join_time_idx(idx, V_L, I_L) for idx in V_L.index})
VI_R_idx = pd.Series({idx: best_join_time_idx(idx, V_R, I_R) for idx in V_R.index})

V_L["I_L"] = I_L.I_L[VI_L_idx]
V_R["I_R"] = I_R.I_R[VI_R_idx]

V_L["P_L"] = V_L.V_L * V_L.I_L
V_R["P_R"] = V_R.V_R * V_R.I_R

dfs["P_L"] = P_L = V_L[["time", "P_L", "I_L", "V_L"]].copy()
dfs["P_R"] = P_R = V_R[["time", "P_R", "I_R", "V_R"]].copy()

# ---
# We have power data in P_L / P_R
# ---

wL = dfs["wL"]
wR = dfs["wR"]
wL = wL.rename(columns={"/left_drive/velocity": "wL"}, errors="raise")
wR = wR.rename(columns={"/right_drive/velocity": "wR"}, errors="raise")

# Merge through near index, like Baril2020
Pw_L_idx = pd.Series({idx: best_join_time_idx(idx, P_L, wL) for idx in P_L.index})
Pw_R_idx = pd.Series({idx: best_join_time_idx(idx, P_R, wR) for idx in P_R.index})

# print(len(wL.wL), Pw_L_idx.max(), Pw_L_idx.min(), Pw_L_idx.shape)
# print(len(wR.wR), Pw_R_idx.max(), Pw_R_idx.min(), Pw_R_idx.shape)

P_L["wL"] = pd.Series({idx: wL.wL.iloc[Pw_L_idx.iloc[idx]] for idx in P_L.index})
P_R["wR"] = pd.Series({idx: wR.wR.iloc[Pw_R_idx.iloc[idx]] for idx in P_R.index})

# P_L["wL"] = wL.wL.iloc[Pw_L_idx.to_numpy()]
# P_R["wR"] = wR.wR.iloc[Pw_R_idx.to_numpy()]

# print(len(wL.wL), len(P_L.wL), len(P_L.P_L), len(Pw_L_idx))
# print(wL.wL.count(), P_L.wL.count(), P_L.P_L.count(), Pw_L_idx.count())

print("-------------")
print(wL.wL.head())
print(Pw_L_idx.head())
print(P_L.wL.head())


# View changes
import matplotlib.pyplot as plt

fig, ax1 = plt.subplots(figsize=(48, 12))
ax1.plot(wL.time, wL.wL, "ro", label="original velocity", alpha=0.2)
ax1.plot(P_L.time, P_L.wL, "ko", label="undersampled velocity")
ax2 = ax1.twinx()
ax2.plot(P_L.time, P_L.P_L, "go", label="power", alpha=0.4)
ax1.legend()
ax2.legend()
plt.show()

# Interpolate
fPL = interp.interp1d(P_L.time, P_L.P_L, fill_value="extrapolate")
fPR = interp.interp1d(P_R.time, P_R.P_R, fill_value="extrapolate")
fVL = interp.interp1d(P_L.time, P_L.V_L, fill_value="extrapolate")
fVR = interp.interp1d(P_R.time, P_R.V_R, fill_value="extrapolate")
fIL = interp.interp1d(P_L.time, P_L.I_L, fill_value="extrapolate")
fIR = interp.interp1d(P_R.time, P_R.I_R, fill_value="extrapolate")

motorL = dfs["wL"]
motorR = dfs["wR"]

motorL = motorL.rename(columns={"/left_drive/velocity": "wL"}, errors="raise")
motorR = motorR.rename(columns={"/right_drive/velocity": "wR"}, errors="raise")

motorL["P_L"] = fPL(motorL.time)
motorR["P_R"] = fPR(motorR.time)
motorL["V_L"] = fVL(motorL.time)
motorR["V_R"] = fVR(motorR.time)
motorL["I_L"] = fIL(motorL.time)
motorR["I_R"] = fIR(motorR.time)

motor_path_L = merged_path / f"{basename}_simpleL.csv"
motor_path_R = merged_path / f"{basename}_simpleR.csv"
motorL.to_csv(motor_path_L, index=False)
motorR.to_csv(motor_path_R, index=False)
print(f"{abbr} : Exported simple motor CSVs in {motor_path_L} and {motor_path_R}")

power_path_L = merged_path / f"{basename}_powerL.csv"
power_path_R = merged_path / f"{basename}_powerR.csv"

P_L.to_csv(power_path_L, index=False)
P_R.to_csv(power_path_R, index=False)
print(f"{abbr} : Exported simple power CSVs in {motor_path_L} and {motor_path_R}")

# dI/dt vs speed
# DC motor model : V = L di/dt + R i + K w
# Motor model, find datasheet, params
# TODO: Get torque with T = P / w
