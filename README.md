# Code for all runs

## Hypothesis

* Mean is right
* Privilegiate `/imu_and_wheel_odom`
* Try bigger windows for distance estimations
* `/MTI_imu/data` is coming from Madgwick filter
  * Madwick filters the gyroscope and accelerometer data by using the magnetometer and gravitational acceleration as references
  * Madgwick uses a gradient descent and needs the frequency of the IMU in order to filter out the noise
* After RLS :
  * There are higher rolling resistance values in rotations
  * Lower rolling resistance values in front of boreal (could it be related to weather ?)
* Low speed, hence no aerodynamic drag
  * Not true with high speed winds, like in a polar area
  * Effect of air temperature on aerodynamic drag
* We used
  * v = ||{vx, vy, vz}||
  * a = ||{ax, ay, az}||
  * F_Fr = ||F_Fr|| cinétique // Régime permanent
* Sensor power is constant during an experiment

## Method names

* `Naive` : When I skip parts of articles just to see if we can get results without implementing algorithms
* `Quann` : Longitudinal Vehicle Model / parts of the model as the GP was never implemented
* `Pentzer` : SSMR Power Model with 3 coefficients (mu, G, beta)
* `Gora` : SSMR Power model with 2 coefficients (mu, G)
* `Reina` : Using only the kinematics model for terrain estimation, inspiration for doing a Kalman Filter for Slip Track Estimation, he used main motion primitives
* `Fiset (Concordia)` : Looks like Pentzer, except they rely on $v_c$, some kind of relation between power and turn radius

## Quaternions

![Multiplication](https://personal.utdallas.edu/~sxb027100/dock/quaternion_files/QuatProduct.gif)

## Weather

![Weather](figs/docs/weather.png)

## Rosbag comparison

![Doughnut topics](figs/docs/missing_topics_doughnut.jpg)
![Forêt topics](figs/docs/missing_topics_foret.jpg)

## Extras

* Dogru, Marques uses temperatures
* Sardpour uses driving style

## Terminology

* `terrain characterization`
* `Anisotropism` : the robot doesn't consume the same power on the same point every time. The power consumption depends on the direction of the robot on the point
* `in situ exploration`
* `regenerative breaking` : process through which a robot regenerates the battery through times where wheels are driven by external forces (gravity, inertia)

## SSMR

* Pentzer RLS is better than naive Ax=b
  * Naive Ax=b : We have a linear model with three coefficients. Take the first N consecutives measurements and compute the coefficients like a Ax=B problem, except that we have N equations for 3 variables, this will be solved through np.lingalg.lstsq
  * Pentzer RLS : Pentzer 2022 article, takes in account a covariance matrix and a covariance of the measurement noise of about r_k = 1W, uses an initial state of mu=0.5, G = 10 N, B = 1
* Symmetric model is better than assymetric

## Pipeline

```mermaid
graph TD
    bag([data/base.bag]) --> filter_script[Filter bagfile]
    click filter_script "./data/filter_bag.sh"

    filter_script --> imu_bag([data/runs/run/base_imu.bag])
    filter_script --> elec_bag([data/runs/run/base_elec.bag])
    filter_script --> gps_bag([data/runs/run/base_gps.bag])

    imu_bag --> rosbag_csv[Create CSVs]
    elec_bag --> rosbag_csv
    gps_bag --> rosbag_csv
    click rosbag_csv "./data/create_csv.py"

    rosbag_csv --> imu_csv([data/runs/run/imu/*.csv])
    rosbag_csv --> elec_csv([data/runs/run/elec/*.csv])

    gps_txt([data/rawgps/*.txt]) --> gps_prep[GPS data formatting]
    click gps_prep "./steps/gps.py"

    gps_prep --> gps_csv([data/runs/run/gps/*.csv])

    imu_csv --> data_exploration[Exploration of topics data]
    elec_csv --> data_exploration

    imu_csv --> csv_merging[Merge all CSVs]
    elec_csv --> csv_merging
    gps_csv --> csv_merging
    click csv_merging "steps/merge.py"

    csv_merging --> merged_csv([data/runs/run/merged/_m.csv<br/><strong>Merged</strong>])

    merged_csv --> csv_sanitization[Sanitize merged CSV]
    click csv_sanitization "./steps/filtering.py"

    csv_sanitization --> cutted_csv([data/runs/run/merged/_mc.csv<br/><strong>Merged + Cutted</strong>])

    csv_sanitization --> filtered_csv([data/runs/run/merged/_mcf.csv<br/><strong>Merged + Filtered</strong>])

    filtered_csv --> merged_exploration{{Exploration of merged CSV}}

    merged_exploration --> expl_imupow[Relation between<br/>IMU and elec]
    click expl_imupow "./run-3-code/-/blob/master/exploration_imupower.ipynb"

    merged_exploration --> expl_gpspow[Relation between<br/>GPS and elec]
    click expl_gpspow "./run-3-code/-/blob/master/exploration_gpspower.ipynb"

    merged_exploration --> expl_imugps[Relation between<br/>IMU and GPS]
    click expl_imugps "./run-3-code/-/blob/master/exploration_imugps.ipynb"

    gps_csv --> gps_exploration[Exploration of GPS data]
    click gps_exploration "./run-3-code/-/blob/master/exploration_gps.ipynb"

```

## ROS bag topics

| Variable    | Warthog                               | Husky                                                  |
| ----------- | ------------------------------------- | ------------------------------------------------------ |
| vx          | /imu_and_wheel_odom/lin_twi/x         | /imu_and_wheel_odom/lin_twi/x                          |
| vy          | /imu_and_wheel_odom/lin_twi/y         | /imu_and_wheel_odom/lin_twi/y                          |
| vz          | /imu_and_wheel_odom/lin_twi/z         | /imu_and_wheel_odom/lin_twi/z                          |
| px          | /imu_and_wheel_odom/position/x        | /imu_and_wheel_odom/position/x                         |
| py          | /imu_and_wheel_odom/position/y        | /imu_and_wheel_odom/position/y                         |
| pz          | /imu_and_wheel_odom/position/z        | /imu_and_wheel_odom/position/z                         |
| wz          | /MTI_imu/data/ang_vel/z               | /MTI_imu/data/ang_vel/z                                |
| ax          | /MTI_imu/data/lin_acc/x               | /MTI_imu/data/lin_acc/x                                |
| ay          | /MTI_imu/data/lin_acc/y               | /MTI_imu/data/lin_acc/y                                |
| az          | /MTI_imu/data/lin_acc/z               | /MTI_imu/data/lin_acc/z                                |
| qw          | /MTI_imu/data/orientation/w           | /MTI_imu/data/orientation/w                            |
| qx          | /MTI_imu/data/orientation/x           | /MTI_imu/data/orientation/x                            |
| qy          | /MTI_imu/data/orientation/y           | /MTI_imu/data/orientation/y                            |
| qz          | /MTI_imu/data/orientation/z           | /MTI_imu/data/orientation/z                            |
| psi         | /imu_and_wheel_odom/orientation/roll  | /imu_and_wheel_odom/orientation/roll                   |
| theta       | /imu_and_wheel_odom/orientation/pitch | /imu_and_wheel_odom/orientation/pitch                  |
| phi         | /imu_and_wheel_odom/orientation/yaw   | /imu_and_wheel_odom/orientation/yaw                    |
| cmd_v       | /cmd_vel/linear/x                     | /husky_velocity_controller/cmd_vel_unstamped/linear/x  |
| cmd_w       | /cmd_vel/angular/z                    | /husky_velocity_controller/cmd_vel_unstamped/angular/z |
| current     | /mcu_status/current_battery           | -                                                      |
| current_cpu | -                                     | /status/current/cpu                                    |
| voltage     | /mcu_status/measured_battery          | /status/voltage/battery                                |
| wL          | /left_drive/status/speed              | /joint_states/velocity/left                            |
| wR          | /right_drive/status/speed             | /joint_states/velocity/right                           |
| IL          | /left_drive/status/battery_current    | /status/current/drive_left                             |
| IR          | /right_drive/status/battery_current   | /status/current/drive_right                            |
| VL          | /left_drive/status/battery_voltage    | /status/voltage/drive_left                             |
| VR          | /right_drive/status/battery_voltage   | /status/voltage/drive_right                            |
