import unittest
from typing import Tuple

import numpy as np

import utils as u
from utils import Quaternion


class QuaternionTest(unittest.TestCase):
    """Quaternion test"""

    def setUp(self) -> None:
        self.q1 = Quaternion(w=0, x=0, y=0, z=1)
        self.q2 = Quaternion(w=2, x=3, y=4, z=5)
        self.q3 = Quaternion(w=2, x=4, y=6, z=8)
        self.q4 = Quaternion(w=-5, x=10, y=-20, z=40)
        self.q5 = Quaternion(w=np.sqrt(2) / 2, x=0, y=0, z=np.sqrt(2) / 2)
        self.q6 = Quaternion(w=np.sqrt(2) / 2, x=0, y=np.sqrt(2) / 2, z=0)

    def test_length(self):
        self.assertAlmostEqual(self.q1.norm(), 1)
        self.assertNotEqual(self.q2.norm(), 1)

    def test_division(self):
        self.assertEqual(self.q3 / 2, Quaternion(w=1, x=2, y=3, z=4))
        self.assertEqual(self.q4 / 5, Quaternion(w=-1, x=2, y=-4, z=8))

    def test_is_unit(self):
        self.assertEqual(self.q1.is_unit(), True)
        self.assertEqual(self.q2.is_unit(), False)

    def test_to_dict(self):
        self.assertEqual(
            self.q2.to_dict(),
            {
                "w": 2,
                "x": 3,
                "y": 4,
                "z": 5,
            },
        )

    def test_from_list(self):
        q1_l = Quaternion.from_list([0, 0, 1])
        q2_l = Quaternion.from_list([2, 3, 4, 5])
        self.assertEqual(self.q1, q1_l)
        self.assertEqual(self.q2, q2_l)

    def test_conjugate(self):
        self.assertEqual(self.q2.conjugate(), Quaternion(w=2, x=-3, y=-4, z=-5))

    def assertTupleAlmostEqual(self, tuple_a: Tuple[float], tuple_b: Tuple[float]):
        """`self.assertAlmostEqual` on tuples of floats"""
        for a, b in zip(tuple_a, tuple_b):
            self.assertAlmostEqual(a, b)

    def test_rpy(self):
        # self.assertTupleAlmostEqual(self.q5.to_rpy(), (0, 0, 1.5707963))
        self.assertTupleAlmostEqual(self.q5.to_rpy(mode="deg"), (0, 0, 90))
        # self.assertTupleAlmostEqual(self.q5.to_euler(), (0, 0, 1.5707963268))
        self.assertTupleAlmostEqual(self.q5.to_euler(mode="deg"), (0, 0, 90))

        # self.assertTupleAlmostEqual(self.q6.to_rpy(), (0, 1.5707963, 0))
        self.assertTupleAlmostEqual(self.q6.to_rpy(mode="deg"), (0, 90, 0))
        # self.assertTupleAlmostEqual(self.q6.to_euler(), (0, 1.5707963, 0))
        self.assertTupleAlmostEqual(self.q6.to_euler(mode="deg"), (0, 90, 0))


class FilenameTest(unittest.TestCase):
    """Filename test"""

    def setUp(self) -> None:
        self.abbrv = [
            "R01",
            "R02",
            "R03",
            "R04",
            "R05",
            "R06",
            "R07",
            "R08",
            "R09",
            "R10",
            "R11",
            "R12",
            "R13",
            "R14",
            "TA",
            "TB",
            "TC",
        ]

    def test_abbreviate_n_filename(self):
        for abbr in self.abbrv:
            fname = u.get_filename(abbr)
            self.assertEqual(abbr, u.abbreviate(fname))


if __name__ == "__main__":
    unittest.main()
