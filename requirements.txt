# Accelerate install and build
wheel

# Science
numpy
pandas
ipython
jupyterlab
matplotlib>=3.7.0
scipy
papermill
scikit-learn
tikzplotlib

# BLDC motors
# sudo apt install libgirepository1.0-dev libcairo2-dev
nemo_bldc


# Map
folium
utm
mplleaflet
geopandas
gpxpy
haversine
cartopy

# Dev
black
pylint
ipykernel

# ros
PyYaml
--extra-index-url https://rospypi.github.io/simple/ rospkg


