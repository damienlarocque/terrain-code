from sklearn.ensemble import RandomForestClassifier
from sklearn.utils.estimator_checks import check_estimator

from models.ML import terrain_MLP as tmlp
from models.ML import terrain_RF as trf

if __name__ == "__main__":
    terrains = sorted(["Sandy loam", "Asphalt", "Snow", "Flooring", "Ice"])
    ter_rf = trf.TerrainRFRegressor(terrains=terrains)
    ter_mlp = tmlp.TerrainMLPRegressor(terrains=terrains)
    rf_clf = trf.TerrainRFClassifier()
    rf = RandomForestClassifier()

    check_estimator(rf)
    check_estimator(rf_clf)
    check_estimator(ter_mlp)
    check_estimator(ter_rf)
