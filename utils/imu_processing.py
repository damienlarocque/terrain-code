from __future__ import annotations  # procompatibilité

from typing import Dict, List, Tuple

import numpy as np
import pandas as pd

import models


def shrink_topics(df: pd.DataFrame):
    rename_cols = {
        "/MTI_imu/data/orientation/pitch": "imu_data_pitch",
        "/imu_and_wheel_odom/orientation/pitch": "imu_wheel_pitch",
        "/MTI_imu/data/orientation/yaw": "imu_data_yaw",
        "/imu_and_wheel_odom/orientation/yaw": "imu_wheel_yaw",
        "/MTI_imu/data/orientation/roll": "imu_data_roll",
        "/imu_and_wheel_odom/orientation/roll": "imu_wheel_roll",
    }
    df.rename(columns=rename_cols, inplace=True, errors="raise")


def compute_pitch_angles(df: pd.DataFrame):
    cols = [c for c in df.columns.values.tolist() if "imu_" in c]
    for c in cols:
        df[f"{c}_deg"] = np.rad2deg(df[c])


def wrap_to_pi(angle: float) -> float:
    """Wrap angle to pi

    Args:
        angle (float): Wrap angle to pi

    Returns:
        float: Wrap to pi
    """
    return np.arctan2(np.sin(angle), np.cos(angle))


class Quaternion:
    """Quaternion class

    Math StackExchange : https://math.stackexchange.com/a/1721393/448516

    w is the real part
    q = w + xi + yj + zk
    """

    def __init__(self, w=0, x=0, y=0, z=0) -> None:
        """Initialize a quaternion"""
        self.w = w
        self.x = x
        self.y = y
        self.z = z

    @classmethod
    def from_list(cls, l):
        attr = "wxyz"
        if len(l) == 4:
            # w, x, y, z
            vals = {k: e for k, e in zip(attr, l)}
            return cls(**vals)
        if len(l) == 3:
            # x, y, z
            vals = {k: e for k, e in zip(attr[1:], l)}
            return cls(**vals)
        else:
            raise NotImplementedError(f"Can't instantiate with list of length {len(l)}")

    def __mul__(self, other: Quaternion) -> Quaternion:
        """Multiply self * other"""
        mult_w = self.w * other.w - self.x * other.x - self.y * other.y - self.z * other.z
        mult_x = self.x * other.w + self.w * other.x - self.z * other.y + self.y * other.z
        mult_y = self.y * other.w + self.z * other.x + self.w * other.y - self.x * other.z
        mult_z = self.z * other.w - self.y * other.x + self.x * other.y + self.w * other.z
        return Quaternion(w=mult_w, x=mult_x, y=mult_y, z=mult_z)

    def __truediv__(self, other) -> Quaternion:
        if isinstance(other, (int, float)):
            return Quaternion(
                w=self.w / other,
                x=self.x / other,
                y=self.y / other,
                z=self.z / other,
            )
        else:
            raise NotImplementedError()

    def __sub__(self, other: Quaternion) -> Quaternion:
        return Quaternion(
            w=self.w - other.w,
            x=self.x - other.x,
            y=self.y - other.y,
            z=self.z - other.z,
        )

    def __eq__(self, other: Quaternion) -> bool:
        """Self equal"""
        selfdict = self.to_dict()
        otherdict = other.to_dict()

        keys = selfdict.keys()
        comparisons = [selfdict[k] == otherdict[k] for k in keys]
        return all(comparisons)

    def to_list(self) -> List[float]:
        """Return list of components
        [w, x, y, z]
        """
        return [self.w, self.x, self.y, self.z]

    def to_dict(self) -> Dict[str, float]:
        """Return dict of components
        {'w':w, 'x':x, 'y':y, 'z':z}
        """
        return {
            "w": self.w,
            "x": self.x,
            "y": self.y,
            "z": self.z,
        }

    def dot_prod(self) -> float:
        return np.square(self.to_list()).sum()

    def conjugate(self) -> Quaternion:
        """Get the conjugate of the quaternion"""
        return Quaternion(w=self.w, x=-self.x, y=-self.y, z=-self.z)

    def inverse(self) -> Quaternion:
        """Get the inverse of the quaternion"""
        return self.conjugate() / self.dot_prod()

    def norm(self) -> float:
        """Compute the length of the quaternion
        Docs : https://math.stackexchange.com/a/1703467/448516
        """
        return np.sqrt(self.dot_prod())

    def unit(self) -> Quaternion:
        """Get unit quaternion"""
        return self / self.norm()

    def is_unit(self) -> bool:
        """Determine if quaternion is a unit quaternion"""
        return np.isclose(self.norm(), 1)

    def get_pitch(self) -> float:
        sinp = 2 * (self.w * self.y - self.z * self.x)
        pitch = np.where(np.abs(sinp) >= 1, np.sign(sinp) * np.pi / 2, np.arcsin(sinp))
        return float(pitch)

    def to_rpy(self, mode: str = "rad") -> Tuple[float]:
        """Get rpy values from quaternion
        Args:
        - mode (str) : either 'deg' or 'rad'

        Returns:
        - angles (tuple): euler angles -  rpy (roll, pitch, yaw)

        Documentation: https://automaticaddison.com/how-to-convert-a-quaternion-into-euler-angles-in-python/
        """

        class RPYAngles(object):
            roll: float
            pitch: float
            yaw: float

        angles = RPYAngles()
        if mode not in ("rad", "deg"):
            raise NotImplementedError(f"Mode {mode} is not implement for quaternion to Euler conversion")

        sinr_cosp = 2 * (self.w * self.x + self.y * self.z)
        cosr_cosp = 1 - 2 * (self.x**2 + self.y**2)
        angles.roll = np.arctan2(sinr_cosp, cosr_cosp)

        sinp = 2 * (self.w * self.y - self.z * self.x)
        angles.pitch = np.where(
            np.abs(sinp) >= 1,
            np.sign(sinp) * np.pi / 2,
            np.arcsin(sinp),
        )

        siny_cosp = 2 * (self.w * self.z + self.x * self.y)
        cosy_cosp = 1 - 2 * (self.y**2 + self.z**2)
        angles.yaw = np.arctan2(siny_cosp, cosy_cosp)

        angles = (angles.roll, angles.pitch, angles.yaw)
        if mode == "deg":
            return (np.rad2deg(a) for a in angles)
        else:
            return angles

    def to_euler(self, mode: str = "rad") -> Tuple[float]:
        """Get rpy values from quaternion
        Alias for to_rpy
        mode (str) : either 'deg' or 'rad'
        """
        return self.to_rpy(mode=mode)


def remove_gravity(acceleration: List[float], attitude: Quaternion) -> Quaternion:
    """Remove gravity measure from IMU data
    Docs : https://stackoverflow.com/a/60563702/7767136
    """
    q = attitude
    v = Quaternion.from_list(acceleration)
    v_earth = q * v * q.conjugate()
    v_earth -= Quaternion(z=models.K1D_CONSTANTS.g)
    q_inv = q.inverse()
    v_imu = q_inv * v_earth * q_inv.conjugate()

    return v_imu
