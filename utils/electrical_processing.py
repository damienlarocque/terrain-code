import numpy as np
import pandas as pd
from scipy import integrate, signal

from utils import ssmr_utils as ssmru
from utils import ugv_utils as ugvu


def motion_power(thedf: pd.DataFrame):
    """Compute motion power from the DataFrame

    Args:
        thedf (pd.DataFrame): Original DataFrame

    Raises:
        ValueError: Returned when argument DataFrame doesn't contain vehicle metadata.
    """
    if not hasattr(thedf, "meta"):
        # No metadata
        raise AttributeError("DataFrame doesn't have vehicle metadata")

    meta: ugvu.RunMetadata = thedf.meta

    ugv_name = thedf.meta.ugv_name
    if ugv_name == "warthog":
        # Electrical power
        thedf["Vmot_L"] = thedf.V_L * np.sign(thedf.wL)
        thedf["Vmot_R"] = thedf.V_R * np.sign(thedf.wR)

        thedf["wmot_L"] = thedf.wL * meta.gear_ratio
        thedf["wmot_R"] = thedf.wR * meta.gear_ratio

        # thedf["Emot_L"] = thedf.wmot_L * meta.motor_Ke
        # thedf["Emot_R"] = thedf.wmot_R * meta.motor_Ke

        thedf["Imot_L"] = thedf.I_L * np.sign(thedf.wL)
        thedf["Imot_R"] = thedf.I_R * np.sign(thedf.wR)

        thedf["PE_L"] = thedf.I_L * thedf.V_L
        thedf["PE_R"] = thedf.I_R * thedf.V_R

        # thedf["Tmot_L"] = meta.motor_Kt * (thedf.I_L - meta.motor_I_nl)
        # thedf["Tmot_R"] = meta.motor_Kt * (thedf.I_R - meta.motor_I_nl)

        thedf["Tmot_L"] = meta.motor_Kt * thedf.I_L * np.sign(thedf.wL)
        thedf["Tmot_R"] = meta.motor_Kt * thedf.I_R * np.sign(thedf.wR)

        thedf["TL"] = meta.gear_eta * meta.gear_ratio * thedf.Tmot_L
        thedf["TR"] = meta.gear_eta * meta.gear_ratio * thedf.Tmot_R

        thedf["PM_L"] = thedf.TL * thedf.wL
        thedf["PM_R"] = thedf.TR * thedf.wR

        thedf["P_logical"] = thedf.current_cpu * thedf.voltage
        thedf["P_motion"] = thedf.PM_L + thedf.PM_R
        thedf["PE_motion"] = thedf.PE_L + thedf.PE_R
        thedf["P_total"] = thedf.PE_motion + thedf.P_logical
        thedf["PE_batt"] = thedf.current * thedf.voltage

    elif ugv_name == "husky":
        # Electrical power
        thedf["Vmot_L"] = thedf.V_L * np.sign(thedf.wL)
        thedf["Vmot_R"] = thedf.V_R * np.sign(thedf.wR)

        thedf["wmot_L"] = thedf.wL * meta.gear_ratio
        thedf["wmot_R"] = thedf.wR * meta.gear_ratio

        thedf["Emot_L"] = thedf.wmot_L * meta.motor_Ke
        thedf["Emot_R"] = thedf.wmot_R * meta.motor_Ke

        thedf["Imot_L"] = thedf.I_L * np.sign(thedf.wL)
        thedf["Imot_R"] = thedf.I_R * np.sign(thedf.wR)

        # thedf["Tmot_L"] = meta.motor_Kt * (thedf.Imot_L.abs() - meta.motor_I_nl) * np.sign(thedf.Imot_L)
        # thedf["Tmot_R"] = meta.motor_Kt * (thedf.Imot_R.abs() - meta.motor_I_nl) * np.sign(thedf.Imot_R)

        # thedf["PE_L"] = thedf.Imot_L * thedf.Vmot_L
        # thedf["PE_R"] = thedf.Imot_R * thedf.Vmot_R

        thedf["PE_L"] = thedf.I_L * thedf.V_L
        thedf["PE_R"] = thedf.I_R * thedf.V_R

        thedf["Tmot_L"] = meta.motor_Kt * thedf.I_L * np.sign(thedf.wL)
        thedf["Tmot_R"] = meta.motor_Kt * thedf.I_R * np.sign(thedf.wR)

        # thedf["Tmot_L"] = meta.motor_Kt * (thedf.I_L - meta.motor_I_nl)
        # thedf["Tmot_R"] = meta.motor_Kt * (thedf.I_R - meta.motor_I_nl)

        thedf["TL"] = meta.gear_eta * meta.gear_ratio * thedf.Tmot_L
        thedf["TR"] = meta.gear_eta * meta.gear_ratio * thedf.Tmot_R

        thedf["PM_L"] = thedf.TL * thedf.wL
        thedf["PM_R"] = thedf.TR * thedf.wR

        thedf["P_logical"] = thedf.current_cpu * thedf.voltage
        thedf["P_motion"] = thedf.PM_L + thedf.PM_R
        thedf["PE_motion"] = thedf.PE_L + thedf.PE_R
        thedf["P_total"] = thedf.PE_motion + thedf.P_logical


def warthog_pwr(the_df: pd.DataFrame):
    """Compute power using /mcu/status values"""
    the_df["current"] = the_df["/mcu/status/current_battery"]
    the_df["voltage"] = the_df["/mcu/status/measured_battery"]
    the_df["power"] = the_df["current"] * the_df["voltage"]


def husky_pwr(thedf: pd.DataFrame):
    """Compute power using /status values"""
    thedf["current"] = thedf["/status/current/drive_left"]
    thedf["voltage"] = thedf["/status/voltage/drive_left"]
    thedf["power"] = thedf["voltage"] * thedf["current"]


def compute_energy(the_df: pd.DataFrame):
    """Compute energy no matter the df"""

    # Create relative time
    the_df.index = pd.RangeIndex(len(the_df.index))
    the_df["time_rel"] = the_df["time"] - the_df["time"][0]

    power = the_df["power"]
    rel_time = the_df["time_rel"]
    the_df["energy_ws"] = integrate.cumulative_trapezoid(power, rel_time, initial=0)
    the_df["energy_wh"] = the_df["energy_ws"] / 3600


def butterworth_filter(the_df: pd.DataFrame, topic: str, order: int = 3, cutoff_freq: float = 10):
    """Apply Butterworth filter on electrical time series, like Pentzer2014
    Pentzer 2014 : The measured and modeled power signals are noisy and were filtered with a 3rd order Butterworth filter with a cutoff frequency of 10 Hz.
    - fs : sampling frequency of 1 Hz because of electrical CAN bus sampling

    Docs:
    * https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.butter.html
    * https://dsp.stackexchange.com/questions/49460/apply-low-pass-butterworth-filter-in-python
    """
    b, a = signal.butter(N=order, Wn=cutoff_freq, fs=1)
    output = signal.filtfilt(b, a, the_df[topic])
    # the_df[f"{topic}_butter"] = output
    return output
