from __future__ import annotations

import re
from pathlib import Path
from typing import TYPE_CHECKING

from .CONSTANTS import TERRAINS
from .json_utils import export_descriptions_json
from .yaml_utils import export_descriptions_yaml

if TYPE_CHECKING:
    from typing import Generator

data_path: Path = Path("data")
runs_path = data_path / "runs"
figs_path: Path = Path("figs")
runs_figs = figs_path / "runs"


def abbreviate(name: str) -> str:
    """
    Abbreviate basename / filename
    - D01_2020-10-08-15-32-10-TF-boreal01 -> D01
    - Run14_2021-04-02-06-53-15 -> R14
    - TeachA -> TA
    """
    if name[0].lower() == "D".lower():
        return name.split("_", 1)[0]
    elif name[0].lower() == "S".lower():
        return name.split("_", 1)[0]
    elif name[0].lower() == "P".lower():
        return name.split("_", 1)[0]
    elif name[0].lower() == "M".lower():
        return name.split("_", 1)[0]
    elif name[0].lower() == "H".lower():
        return name.split("_", 1)[0]
    elif name[0].lower() == "Y".lower():
        return name.split("_", 1)[0]

    # Foret FR
    tag = name.split("_")[0]
    letter = tag[0]
    if letter.lower() == "R".lower():
        num = re.findall(r"\d+", tag)[-1]
        return f"R{num:0>2}"
    elif letter.lower() == "T".lower():
        ref = re.findall("[A-Z][^A-Z]*", tag)[-1]
        return f"T{ref}"
    else:
        raise NotImplementedError(f"Unknown name {name}")


def get_rundir(basename: str, subdir: str | None = None) -> Path:
    """
    * Create run data directory from basename
        - D01_2020-10-08-15-32-10-TF-boreal01 -> data/runs/D01
        - Run3_2021-03-31-14-03-54 -> data/runs/R03
        - summary -> data/runs
    * Make directory if not existing
    * Make subdirectories
    * Return directory path
    """
    if basename == "summary":
        return runs_path
    abbr: str = abbreviate(basename)
    name_path: Path = runs_path / abbr
    name_path.mkdir(parents=True, exist_ok=True)

    if subdir is not None:
        subdir_path = name_path / subdir
        subdir_path.mkdir(parents=True, exist_ok=True)
        return subdir_path

    # for subdir in ("elec", "gps", "imu", "merged", "k1d", "ssmrk", "simple_motor", "elecmotmod"):
    #     subdir_path = name_path / subdir
    #     subdir_path.mkdir(parents=True, exist_ok=True)

    return name_path


def get_figdir(basename: str, subdir: str | None = None) -> Path:
    """
    * Create fig directory from basename:
        - D01_2020-10-08-15-32-10-TF-boreal01 -> figs/runs/D01
        - Run3_2021-03-31-14-03-54 -> figs/runs/R03
        - summary -> figs/summary
    * Make directory if not existing
    * Make subdirectories
    * Return directory path
    """
    # Sepcial case for summary
    if basename in ["summary", "thesis"]:
        name_path: Path = figs_path / basename
        name_path.mkdir(parents=True, exist_ok=True)

        if subdir is not None:
            subdir_path = name_path / subdir
            subdir_path.mkdir(parents=True, exist_ok=True)
            return subdir_path

        # for subdir in ("lvm", "sensor_energy", "ssmrk", "gora"):
        #     subdir_path = name_path / subdir
        #     subdir_path.mkdir(parents=True, exist_ok=True)

        return name_path

    abbr: str = abbreviate(basename)
    name_path: Path = runs_figs / abbr
    name_path.mkdir(parents=True, exist_ok=True)

    # for subdir in (
    #     "elecslope",
    #     "imugps",
    #     "elecimu",
    #     "elevation",
    #     "heading",
    #     # "roll_res",
    #     "k1d_sigs",
    #     "k1d",
    #     "3dpath",
    #     "naive",
    #     "ssmrk",
    #     "simple_motor",
    #     "elecmotmod",
    # ):
    #     subdir_path = name_path / subdir
    #     subdir_path.mkdir(parents=True, exist_ok=True)

    if subdir is not None:
        subdir_path = name_path / subdir
        subdir_path.mkdir(parents=True, exist_ok=True)
        return subdir_path

    return name_path


def get_basename(filename: Path | str) -> str:
    """
    Abbreviate filename to basename
    * D01_2020-10-08-15-32-10-TF-boreal01_elec.bag -> D01_2020-10-08-15-32-10-TF-boreal01
    * Run3_2021-03-31-14-03-54_elec.bag -> Run3_2021-03-31-14-03-54"""
    return Path(filename).stem.rsplit("_", 1)[0]


def get_filename(abbr: str) -> str:
    """
    Get filename from abbreviation
    * D01 -> D01_2020-10-08-15-32-10-TF-boreal01
    * R14 -> Run14_2021-04-02-06-53-15
    * TA -> TeachA
    """
    if abbr.startswith("D"):
        filenames = {
            "D01": "D01_2020-10-08-15-32-10-TF-boreal01_elec",
            "D02": "D02_2020-10-09-09-03-28-TF-boreal02_elec",
            "D03": "D03_2020-11-11-15-04-17-VC-deposit01_elec",
            "D04": "D04_2020-11-11-15-12-46-VC-deposit02_elec",
            "D05": "D05_2021-01-18-10-36-09-SG-grandaxe_elec",
            "D06": "D06_2021-02-22-15-43-34-SF-boreal01_elec",
            "D07": "D07_2021-02-22-15-50-23-SF-boreal02_elec",
            "D08": "D08_2021-02-22-16-01-10-SF-boreal03_elec",
            "D09": "D09_2022-02-23-16-34-49-SG-theodolite_elec",
            "D10": "D10_2020-02-07-14-55-43-SG-winterstorm_elec",
            "D11": "D11_2020-AP-crv2020train_elec",
            "D12": "D12_2020-AP-crv2020valid_elec",
            "D13": "D13_2020-SF-crv2020train_elec",
            "D14": "D14_2020-SF-crv2020valid_elec",
            "D15": "D15_2020-11-05-12-55-22-AP-ParkingLotOdometry_elec",
            "D16": "D16_2020-11-05-13-15-14-VG-GrandAxeOdometry_elec",
            "D17": "D17_2020-11-05-13-27-54-VG-GrandAxe_SP_elec",
            "D18": "D18_2022-08-10-13-38-28-VG-GrandAxeGrassTrail01_elec",
            "D19": "D19_2022-08-10-13-45-44-VG-GrandAxeGrassTrail02_elec",
            "D20": "D20_2022-08-10-13-53-23-VG-GrandAxe100Associes_elec",
            "D21": "D21_2022-08-10-14-00-53-VG-GrandAxePentzerLike_elec",
            "D22": "D22_2022-08-10-14-22-06-AD-DesjardinsParking01_elec",
            "D23": "D23_2022-08-10-14-32-18-AD-DesjardinsParking02_elec",
        }
    elif abbr.startswith("S"):
        filenames = {
            "S01": "S01_2022-08-12-16-15-17-VG-LinGrandAxe_elec",
            "S02": "S02_2022-08-12-16-15-17-VG-LinGrandAxe_elec",
            "S03": "S03_2022-08-12-16-15-17-VG-LinGrandAxe_elec",
            "S04": "S04_2022-08-12-16-15-17-VG-LinGrandAxe_elec",
            "S05": "S05_2022-08-12-16-15-17-VG-LinGrandAxe_elec",
            "S06": "S06_2022-08-12-16-35-17-VG-RotGrandAxe_elec",
            "S07": "S07_2022-08-12-16-35-17-VG-RotGrandAxe_elec",
            "S08": "S08_2022-08-12-16-38-47-VG-RotGrandAxe_elec",
            "S09": "S09_2022-08-12-16-38-47-VG-RotGrandAxe_elec",
            "S10": "S10_2022-08-12-16-38-47-VG-RotGrandAxe_elec",
            "S11": "S11_2022-08-12-16-44-40-AG-LinTrottoirGrandAxe_elec",
            "S12": "S12_2022-08-12-16-44-40-AG-LinTrottoirGrandAxe_elec",
            "S13": "S13_2022-08-12-16-44-40-AG-LinTrottoirGrandAxe_elec",
            "S14": "S14_2022-08-12-16-44-40-AG-LinTrottoirGrandAxe_elec",
            "S15": "S15_2022-08-12-16-48-29-AG-RotTrottoirGrandAxe_elec",
            "S16": "S16_2022-08-12-16-48-29-AG-RotTrottoirGrandAxe_elec",
            "S17": "S17_2022-08-12-16-48-29-AG-RotTrottoirGrandAxe_elec",
            "S18": "S18_2022-08-12-16-51-31-AG-RotTrottoirGrandAxe_elec",
            "S19": "S19_2022-08-12-16-51-31-AG-RotTrottoirGrandAxe_elec",
            "S20": "S20_2022-08-12-16-51-31-AG-RotTrottoirGrandAxe_elec",
            "S21": "S21_2022-08-12-17-02-49-VG-ClimbHill_elec",
            "S22": "S22_2022-08-12-17-02-49-VG-RotHill_elec",
            "S23": "S23_2022-08-12-17-04-22-VG-DownHill_elec",
            "S24": "S24_2022-08-12-17-05-49-AC-LinGroundhogHillBikeTrail_elec",
            "S25": "S25_2022-08-12-17-05-49-AC-LinGroundhogHillBikeTrail_elec",
            "S26": "S26_2022-08-12-17-05-49-AC-LinGroundhogHillBikeTrail_elec",
            "S27": "S27_2022-08-12-17-05-49-AC-LinGroundhogHillBikeTrail_elec",
            "S28": "S28_2022-08-12-17-09-56-VG-LinGrassGrandAxe_elec",
            "S29": "S29_2022-08-12-17-16-02-VG-LinGrassGrandAxe_elec",
            "S30": "S30_2022-08-12-17-16-02-AG-LinTrottoirGrandAxe_elec",
            "S31": "S31_2022-08-12-17-16-02-AG-LinTrottoirGrandAxe_elec",
        }
    elif abbr.startswith("R") or abbr.startswith("T"):
        filenames = {
            "R01": "Run1_2021-03-31-10-42-40-SF-pathC_elec",
            "R02": "Run2_2021-03-31-14-03-54-SF-pathB_elec",
            "R03": "Run3_2021-03-31-15-02-10-SF-pathAp_elec",
            "R04": "Run4_2021-03-31-20-42-00-SF-pathA_elec",
            "R05": "Run5_2021-03-31-21-12-53-SF-pathBp_elec",
            "R06": "Run6_2021-03-31-22-00-21-SF-pathB_elec",
            "R07": "Run7_2021-03-31-22-47-30-SF-failure-pathAp_elec",
            "R08": "Run8_2021-04-01-09-21-40-SF-pathA_elec",
            "R09": "Run9_2021-04-01-10-19-18-SF-pathBp_elec",
            "R10": "Run10_2021-04-01-11-01-21-SF-pathC_elec",
            "R11": "Run11_2021-04-01-18-13-23-SF-pathB_elec",
            "R12": "Run12_2021-04-01-19-09-15-SF-pathAp_elec",
            "R13": "Run13_2021-04-02-06-53-15-SF-pathA_elec",
            "R14": "Run14_2021-04-02-07-25-47-SF-pathBp_elec",
            "TA": "TeachA_2021-03-30-11-04-06-SF-pathA_elec",
            "TB": "TeachB_2021-03-29-15-45-53-SF-pathBp_elec",
            "TC": "TeachC_2021-03-30-07-28-32-SF-pathC_elec",
        }
    elif abbr.startswith("P"):
        filenames = {
            "P01": "P01_2022-09-01-16-44-14-VP-SlopeReceptionMarchandises_elec",
            "P02": "P02_2022-09-01-16-45-57-VP-SlopeReceptionMarchandises_elec",
            "P03": "P03_2022-09-01-16-50-37-VC-UpGroundHogHill_elec",
            "P04": "P04_2022-09-01-16-54-19-VC-DownGroundHogHill_elec",
            "P05": "P05_2022-09-01-16-56-29-VC-GroundHogHill_elec",
            "P06": "P06_2022-09-01-16-58-14-VC-GroundHogHill_elec",
            "P07": "P07_2022-09-01-16-59-55-AC-RotTrailGroundHogHill_elec",
            "P08": "P08_2022-09-01-17-00-27-VC-GroundHogHill_elec",
            "P09": "P09_2022-09-01-17-01-15-VC-RotGroundHogHill_elec",
            "P10": "P10_2022-09-01-17-01-15-VC-GroundHogHill_elec",
            "P11": "P11_2022-09-01-17-03-20-AP-LongRunBack_elec",
        }
    elif abbr.startswith("M"):
        filenames = {
            "M01": "M01_2022-09-14-19-01-10-AP-LinParking01_elec",
            "M02": "M02_2022-09-14-19-05-20-AP-LinParking02_elec",
            "M03": "M03_2022-09-14-19-09-59-AP-RotParkingCCW01_elec",
            "M04": "M04_2022-09-14-19-13-56-AP-RotParkingCW02_elec",
            "M05": "M05_2022-09-12-15-14-00-GD-RotDumpCCW01_elec",
            "M06": "M06_2022-09-12-15-16-51-GD-RotDumpCCW02_elec",
            "M07": "M07_2022-09-12-15-08-14-GD-LinDump_elec",
        }
    elif abbr.startswith("H"):
        filenames = {
            "H01": "H01_2023_04_14-10_44_17-TF-boreal01_elec",
            "H02": "H02_2023_04_14-10_46_43-TF-boreal02_elec",
            "H03": "H03_2023_04_14-10_47_58-TF-boreal03_elec",
            "H04": "H04_2023_04_14-10_49_08-TF-boreal04_elec",
            "H05": "H05_2023_04_14-10_50_23-TF-boreal05_elec",
            "H06": "H06_2023_04_14-10_52_18-TF-boreal06_elec",
            "H07": "H07_2023_04_14-10_53_17-TF-boreal07_elec",
            "H08": "H08_2023_04_14-10_55_13-TF-boreal08_elec",
            "H09": "H09_2023_04_14-10_55_13-TF-boreal09_elec",
            "H10": "H10_2023_04_14-10_55_13-TF-boreal10_elec",
            "H11": "H11_2023_04_14-10_55_13-TF-boreal11_elec",
            "H12": "H12_2023_04_14-10_55_13-TF-boreal12_elec",
            "H13": "H13_2023_04_14-10_55_13-TF-boreal13_elec",
            "H14": "H14_2023_04_14-10_55_13-TF-boreal14_elec",
            "H15": "H15_2023_04_19-18_39_17-SG-grandaxe01_elec",
            "H16": "H16_2023_04_19-18_39_17-SG-grandaxe02_elec",
            "H17": "H17_2023_04_19-18_39_17-SG-grandaxe03_elec",
            "H18": "H18_2023_04_19-18_39_17-SG-grandaxe04_elec",
            "H19": "H19_2023_04_19-18_39_17-SG-grandaxe05_elec",
            "H20": "H20_2023_04_19-18_39_17-SG-grandaxe06_elec",
            "H21": "H21_2023_04_19-18_39_17-SG-grandaxe07_elec",
            "H22": "H22_2023_04_19-18_39_17-SG-grandaxe08_elec",
            "H23": "H23_2023_04_19-18_39_17-SG-grandaxe09_elec",
            "H24": "H24_2023_04_19-18_39_17-AG-grandaxe10_elec",
            "H25": "H25_2023_04_19-18_54_37-AG-grandaxe11_elec",
            "H26": "H26_2023_04_19-19_03_28-AG-grandaxe12_elec",
            "H27": "H27_2023_04_20-17_26_17-AG-grandaxe13_elec",
            "H28": "H28_2023_04_20-17_26_17-SG-grandaxe14_elec",
            "H29": "H29_2023_04_20-17_26_17-SG-grandaxe15_elec",
            "H30": "H30_2023_04_20-17_26_17-SG-grandaxe16transition_elec",
            "H31": "H31_2023_04_20-17_26_17-AG-grandaxe17_elec",
            "H32": "H32_2023_04_20-17_26_17-SG-grandaxe18_elec",
            "H33": "H33_2023_04_20-17_26_17-SG-grandaxe19_elec",
            "H34": "H34_2023_04_20-17_26_17-SG-grandaxe20_elec",
            "H35": "H35_2023_04_20-17_26_17-AG-grandaxe21_elec",
            "H36": "H36_2023_04_20-17_26_17-AG-grandaxe22_elec",
            "H37": "H37_2023_06_23-16_59_18-FB-Pouliot01Static_elec",
            "H38": "H38_2023_06_23-16_59_18-FB-Pouliot02Static_elec",
            "H39": "H39_2023_06_23-16_59_18-FB-Pouliot03Hallway_elec",
            "H40": "H40_2023_06_23-17_13_09-FB-Pouliot04Hallway_elec",
            "H41": "H41_2023_06_23-17_13_09-FB-Pouliot05Hallway_elec",
            "H42": "H42_2023_06_23-17_16_27-FB-Pouliot06Hallway_elec",
            "H43": "H43_2023_06_23-17_16_27-FB-Pouliot07Hallway_elec",
            "H44": "H44_2023_06_23-17_47_48-FB-Pouliot08Hallway_elec",
            "H45": "H45_2023_06_23-17_47_48-FB-Pouliot09Hallway_elec",
            "H46": "H46_2023_06_23-17_47_48-FB-Pouliot10Hallway_elec",
            "H47": "H47_2023_06_23-17_47_48-FB-Pouliot11Hallway_elec",
            "H48": "H48_2023_06_23-17_47_48-FB-Pouliot12Hallway_elec",
            "H49": "H49_2023_08_08-15_07_49-IR-PEPS01Rink_elec",
            "H50": "H50_2023_08_08-15_07_49-IR-PEPS02RinkDoughnut_elec",
            "H51": "H51_2023_08_08-15_56_34-IR-PEPS03Rink_elec",
            "H52": "H52_2023_08_08-15_56_34-IR-PEPS04Rink_elec",
            "H53": "H53_2023_08_08-15_56_34-IR-PEPS05Rink_elec",
            "H54": "H54_2023_08_08-15_56_34-IR-PEPS06Rink_elec",
            "H55": "H55_2023_08_08-15_56_34-IR-PEPS07Rink_elec",
            "H56": "H56_2023_08_08-15_56_34-IR-PEPS08Rink_elec",
            "H57": "H57_2023_08_08-15_56_34-IR-PEPS09Rink_elec",
            "H58": "H58_2023_08_08-15_56_34-IR-PEPS10Rink_elec",
            "H59": "H59_2023_08_08-15_56_34-IR-PEPS11Rink_elec",
            "H60": "H60_2023_08_08-15_56_34-IR-PEPS12Rink_elec",
            "H61": "H61_2023_08_08-15_56_34-IR-PEPS13Rink_elec",
            "H62": "H62_2023_11_24-10_52_48-SF-boreal01_elec",
            "H63": "H63_2023_11_24-10_52_48-SF-boreal02_elec",
            "H64": "H64_2023_11_24-10_52_48-SF-boreal03_elec",
            "H65": "H65_2023_11_24-11_29_24-SF-boreal04_elec",
            "H66": "H66_2023_11_24-11_29_24-SF-boreal05_elec",
            "H67": "H67_2023_11_24-11_29_24-SF-boreal06_elec",
            "H68": "H68_2023_11_24-11_29_24-SF-boreal07_elec",
            "H69": "H69_2023_11_24-11_29_24-SF-boreal08_elec",
            "H70": "H70_2023_11_24-11_29_24-SF-boreal09_elec",
            "H71": "H71_2023_11_24-11_29_24-SF-boreal10_elec",
            "H72": "H72_2023_11_24-11_29_24-SF-boreal11_elec",
            "H73": "H73_2023_11_24-11_29_24-SF-boreal12_elec",
            "H74": "H74_2023_12_18-14_08_09-MG-GrandAxeMultiTerrain_elec",
        }
    elif abbr.startswith("Y"):
        filenames = {
            "Y01": "Y01_2023_06_13-13_48_35-YY-HuskyNoLoad005_elec",
            "Y02": "Y02_2023_06_13-13_51_47-YY-HuskyNoLoad010_elec",
            "Y03": "Y03_2023_06_13-13_54_01-YY-HuskyNoLoad105_elec",
            "Y04": "Y04_2023_06_13-13_56_10-YY-HuskyNoLoad015_elec",
            "Y05": "Y05_2023_06_13-13_59_16-YY-HuskyNoLoad020_elec",
            "Y06": "Y06_2023_06_13-14_02_14-YY-HuskyNoLoad025_elec",
            "Y07": "Y07_2023_06_13-14_05_51-YY-HuskyNoLoad030_elec",
            "Y08": "Y08_2023_06_13-14_09_23-YY-HuskyNoLoad035_elec",
            "Y09": "Y09_2023_06_13-14_12_53-YY-HuskyNoLoad040_elec",
            "Y10": "Y10_2023_06_13-14_16_47-YY-HuskyNoLoad045_elec",
            "Y11": "Y11_2023_06_13-14_19_49-YY-HuskyNoLoad050_elec",
            "Y12": "Y12_2023_06_13-14_23_03-YY-HuskyNoLoad150_elec",
            "Y13": "Y13_2023_06_13-14_29_27-YY-HuskyNoLoad145_elec",
            "Y14": "Y14_2023_06_13-14_32_10-YY-HuskyNoLoad140_elec",
            "Y15": "Y15_2023_06_13-14_35_18-YY-HuskyNoLoad135_elec",
            "Y16": "Y16_2023_06_13-14_39_37-YY-HuskyNoLoad130_elec",
            "Y17": "Y17_2023_06_13-14_42_37-YY-HuskyNoLoad125_elec",
            "Y18": "Y18_2023_06_13-14_45_45-YY-HuskyNoLoad120_elec",
            "Y19": "Y19_2023_06_13-14_48_53-YY-HuskyNoLoad115_elec",
            "Y20": "Y20_2023_06_13-14_51_51-YY-HuskyNoLoad110_elec",
        }
    else:
        raise NotImplementedError(f"Unknown run {abbr}")
    return filenames[abbr]


def get_terrain(name: str) -> str:
    "Get terrain value from abbr or basename"
    abbr = abbreviate(name) if len(name) < 4 else name
    fname = get_filename(abbr)

    fname_parts = fname.split("-")
    duo_parts = [p for p in fname_parts if len(p) == 2]
    terrain_id = duo_parts[-1]
    terrain = TERRAINS[terrain_id[0]]
    export_descriptions_json(abbr, fname, terrain)
    export_descriptions_yaml(abbr, fname, terrain)
    return terrain


def abbrange(letter: str, a: int, b: int) -> Generator[str, None, None]:
    return (f"{letter}{i:02}" for i in range(a, b))


def slugify(s: str, sep: str = "_") -> str:
    """Replace white spaces with underscores

    Args:
        s (str): Original string
        sep (str): Separator. Defaults to '_

    Returns:
        str: Slugified string
    """
    return sep.join(s.split())


def unslugify(s: str, sep: str = "_") -> str:
    """Replace underscores or sep with spaces

    Args:
        s (str): Original string
        sep (str): Separator. Defaults to '_

    Returns:
        str: Slugified string
    """
    return " ".join(s.split(sep))
