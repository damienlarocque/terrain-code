import json
from typing import Dict, Optional

import numpy as np
import pandas as pd
import utm


def geodom_from_json(run_abbr: str, odom_type: str = "imu") -> Optional[Dict[str, float]]:
    with open("metadata_in.json", "r", encoding="utf-8") as f:
        data = json.load(f)
    run_data = data[run_abbr]
    geodom_data = run_data.get("geodom", None)
    return geodom_data[odom_type] if geodom_data is not None else None


def transf_mat2d(tx, ty, theta):
    theta_rad = np.deg2rad(theta)
    T = np.eye(3)
    T[:2, 2] = [tx, ty]
    T[0, :2] = [np.cos(theta_rad), -np.sin(theta_rad)]
    T[1, :2] = [np.sin(theta_rad), np.cos(theta_rad)]
    R = T[:2, :2]
    return T, R


def transform_odom(
    x_topic: pd.Series,
    y_topic: pd.Series,
    parameters: Optional[Dict[str, float]] = None,
    odom_type: Optional[str] = "imu",
    abbr: Optional[str] = None,
) -> Optional[pd.DataFrame]:
    """Transform odometry data to comply with GPS coordinates
    Fake GPS coordinates using IMU/ICP odom

    Args:
        odom_x (pd.Series): _pandas Series corresponding to latitude_
        odom_y (pd.Series): _pandas Series corresponding to longitude_
        parameters (dict): _Dictionary of 3D transformation parameters_ (`tx`, `ty`, `theta`)
    """
    if parameters is None:
        parameters = geodom_from_json(abbr, odom_type)
    if parameters is None:
        return None

    for k in ("tx", "ty", "theta"):
        assert k in parameters.keys(), f"We need a {k} inside the parameters"

    # Covnert parameters to UTM
    geo_params = {"theta": parameters["theta"]}
    tlat, tlon, zone_num, zone_let = utm.from_latlon(parameters["tx"], parameters["ty"])
    geo_params["tx"] = tlat
    geo_params["ty"] = tlon

    # Move coordinates to numpy array
    odom_df = pd.DataFrame({"x": x_topic, "y": y_topic})
    odom_utm = odom_df.to_numpy().T
    n = len(odom_df)
    odom_utm = np.vstack((odom_utm, np.ones((1, n))))

    # Apply transform on utm and covnert to latlon
    T, _ = transf_mat2d(**geo_params)
    geo_utm = T @ odom_utm
    lat, lon = utm.to_latlon(geo_utm[0, :], geo_utm[1, :], zone_num, zone_let)

    # Return dataframe with GPS data
    return pd.DataFrame({"lat": lat, "lon": lon})

    # geo_df = pd.DataFrame(geo_coords, columns=["latitude", "longitude"])

    # print(geo_coords.shape)
