from itertools import product
from typing import Dict

import folium
import pandas as pd


def map_gps_pts(gps_dfs: Dict[str, pd.DataFrame]):
    """Map points from GPS with dfs dict

    gps_dfs = {"front": front_df, "back": back_df}
    """
    # Get center coordinates
    mean = lambda span: (span.max() + span.min()) / 2

    sides = ("front", "back")
    all_gps_pts = pd.concat([gps_dfs[sd] for sd in sides], ignore_index=True)

    position = (mean(all_gps_pts.latitude), mean(all_gps_pts.longitude))
    m = folium.Map(location=position, zoom_start=15)

    colors = ("red", "blue")
    for sd, col in zip(sides, colors):
        gps_pts = gps_dfs[sd]
        path_coords = [(glat, glong) for glat, glong in zip(gps_pts.latitude, gps_pts.longitude)]

        folium.PolyLine(path_coords, color=col, weight=2.5, opacity=0.5, popup=f"{sd.title()} sensor").add_to(m)

    return m


def map_combined_df(comb_df: pd.DataFrame):
    """Map points from combined dataframe"""
    # Get center coordinates
    mean = lambda span: (span.max() + span.min()) / 2

    sides = ("front", "back")

    gps_subset = [f"{sd}_{tp}" for sd, tp in product(sides, ("latitude", "longitude"))]
    comb_df.dropna(subset=gps_subset, inplace=True)

    comb_df.head()

    all_lat = pd.concat([comb_df[f"{sd}_latitude"] for sd in sides])
    all_long = pd.concat([comb_df[f"{sd}_longitude"] for sd in sides])

    all_gps_pts = pd.DataFrame(
        {
            "latitude": all_lat,
            "longitude": all_long,
        }
    )

    position = (mean(all_gps_pts.latitude), mean(all_gps_pts.longitude))
    m = folium.Map(location=position, zoom_start=15)

    colors = ("red", "blue")
    for sd, col in zip(sides, colors):
        lat = comb_df[f"{sd}_latitude"]
        long = comb_df[f"{sd}_longitude"]
        path_coords = [(glat, glong) for glat, glong in zip(lat, long)]

        folium.PolyLine(path_coords, color=col, weight=2.5, opacity=0.5, popup=f"{sd.title()} sensor").add_to(m)

    return m
