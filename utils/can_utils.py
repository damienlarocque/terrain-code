from __future__ import annotations

import struct
from pathlib import Path

import pandas as pd

# Scaling factor (Refer to index 5100h of Master Object Dictionary)
scaling = 1 / 16


def find_canlog(folder: Path, idx: int | None = 0) -> Path:
    """Find canlog path

    Args:
        folder (Path): Folder that contains can logs

    Raises:
        FileNotFoundError: The provided folder is not a directory

    Returns:
        Path: Path to canlog
    """
    if folder.exists() and folder.is_dir():
        canlog_paths = tuple(f for f in folder.glob("candump*.log"))
        if canlog_paths is None:
            return canlog_paths
        return canlog_paths[idx]
    else:
        raise FileNotFoundError(f"{folder} is not a directory")


def read_canlog(in_fname: Path) -> pd.DataFrame:
    colnames = ["time", "busID", "can_packet"]
    df = pd.read_csv(in_fname, sep=" ", header=None, names=colnames)
    # Remove parenthesis around time values "(time)" -> "time"
    df["time"] = df.time.replace(r"\(|\)", "", regex=True).astype(float)
    df["frameID"] = df.can_packet.str[:3]
    # df["packet"] = df.can_packet.str[3:]

    df.to_csv(in_fname.parent / f"{in_fname.stem}.csv", index=False)

    return df


def decompose_packet(df: pd.DataFrame, col: str, n_bytes=7) -> None:
    """Decompose bytes from packet column

    Args:
        df (pd.DataFrame): DataFrame
        col (str): Column to decompose
    """
    assert col in df, f"Column {col} not in dataframe"
    assert (df[col].str.len() == 2 * n_bytes).all(), f"Column {col} should contain strings of length {2*n_bytes}"
    df["bytes_str"] = df[col].apply(bytes.fromhex)
    df["bytes"] = df.bytes_str.apply(list)
    bytes_columns = [f"b{i}" for i in range(n_bytes)]
    df[bytes_columns] = df.bytes.to_list()

    elec_frameIDs = ("501", "502")
    frameIDs = df.frameID.unique()
    if len(frameIDs == 1) and frameIDs.item() in elec_frameIDs:
        can_cols = ["CAN_V", "CAN_I", "CAN_foo", "CAN_htemp"]
        df[can_cols] = df.bytes_str.apply(lambda x: struct.unpack("<3hB", x)).to_list()
        can_cols = ["uCAN_V", "uCAN_I", "uCAN_foo", "uCAN_htemp"]
        df[can_cols] = df.bytes_str.apply(lambda x: struct.unpack("<3HB", x)).to_list()

        df["CAN_I"] *= scaling
        df["CAN_V"] *= scaling


def sign_warthog_current(df: pd.DataFrame, col: str) -> None:
    """Correct Warthog current inside a DataFrame

    Retrieve CAN bytes by packing (I / scaling) = 16 * I as a unsigned 16-bit short
    Then unpack the value as an unsigned 16-bit short and apply 1/16 scaling

    Args:
        df (pd.DataFrame): DataFrame
        col (str): Column name that contains current
    """
    col_uint = (df[col] / scaling).astype(int)
    col_bytes = col_uint.apply(lambda x: struct.pack("<H", x))
    col_int = col_bytes.apply(lambda x: struct.unpack("<h", x)[0])
    df[f"{col}_signed"] = col_int * scaling
