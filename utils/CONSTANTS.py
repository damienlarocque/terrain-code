# RUNS = ("R03", "R04", "R05", "R06", "R07", "R08", "R09", "R10", "R11", "R12", "R13", "R14", "R15")
RUNS = ("D05",)

EXTS = ("png", "jpg", "pdf")
# EXTS = ("jpg",)

TERRAINS = {
    "V": "vegetal",
    "S": "snow",
    "T": "sandy loam",
    "A": "asphalt",
    "G": "gravel",
    "Y": "no-load",
    "F": "flooring",
    "I": "ice",
    "M": "mixed",
}

COLORS = {
    "vegetal": "xkcd:grass green",
    "snow": "xkcd:cerulean",
    "sandy loam": "xkcd:dark orange",
    "asphalt": "xkcd:dark grey",
    "gravel": "#4248ad",
    "no-load": "xkcd:baby purple",
    "flooring": "xkcd:light red",
    "ice": "#067d77",
    "mixed": "xkcd:baby purple",
}

TEST_RUNS = ("H74",)
