import pandas as pd


def freq(df: pd.DataFrame):
    sig_period = df.time.diff().mean()
    return 1 / sig_period


def period(df: pd.DataFrame):
    return df.time.diff().mean()


def start_time(df: pd.DataFrame):
    df.index = pd.RangeIndex(len(df.index))
    return df.time.iloc[0]


def end_time(df: pd.DataFrame):
    df.index = pd.RangeIndex(len(df.index))
    return df.time.iloc[-1]


def duration(df: pd.DataFrame):
    return end_time(df) - start_time(df)


def show_size(topic: str, df: pd.DataFrame):
    rows, cols = df.shape
    print(f"{topic} : {rows} rows, {cols} cols, size {rows*cols}")


def n_rows(df: pd.DataFrame):
    rows, _ = df.shape
    return rows


def last(s: pd.Series):
    return s.tail(1).item()


def first(s: pd.Series):
    return s.head(1).item()
