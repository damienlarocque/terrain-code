import json
from pathlib import Path
from typing import Union

import pandas as pd

import utils as u


def elecparams_to_json(run_abbr: str, df):
    """Save electrical motor parameters in metadata_out.json"""
    coeffs = df[["L_L", "R_L", "K_L", "L_R", "R_R", "K_R"]].copy()
    motor_params = {
        col: {
            "min": coeffs[col].min(),
            "max": coeffs[col].max(),
            "idxmin": str(coeffs[col].argmin()),
            "idxmax": str(coeffs[col].argmax()),
        }
        for col in coeffs.columns.values
    }

    with open("metadata_out.json", "r", encoding="utf-8") as f:
        data = json.load(f)

    data[run_abbr]["motor_params"] = motor_params

    with open("metadata_out.json", "w", encoding="utf-8") as f:
        json.dump(data, f, indent=2, sort_keys=True)
