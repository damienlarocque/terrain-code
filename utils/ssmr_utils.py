import json
from pathlib import Path
from typing import Dict, Optional, Sequence, Union

import numpy as np
import pandas as pd

import utils as u
from utils import electrical_processing as elecp
from utils import json_utils as jsonu


def get_combinedf(basename: Union[Path, str]) -> pd.DataFrame:
    """Get combine df for getting spans in a notebook (see debug.ipynb)"""
    rundir = u.get_rundir(basename)
    merged_path = rundir / "merged"
    df_path = merged_path / f"{basename}_m.csv"
    combined_df = pd.read_csv(df_path)

    ugv_name = u.get_ugv(u.abbreviate(basename)).lower()
    if ugv_name == "warthog":
        elecp.warthog_pwr(combined_df)
    elif ugv_name == "husky":
        elecp.husky_pwr(combined_df)
    elecp.compute_energy(combined_df)

    return combined_df


def logical_from_json(run_abbr: str):
    with open("metadata_out.json", "r", encoding="utf-8") as f:
        data = json.load(f)
    if run_abbr not in data.keys():
        return None

    run_data = data[run_abbr]
    logical_power = run_data.get("logical_power", None)
    return logical_power


def logical_index_from_json(run_abbr: str):
    with open("metadata_in.json", "r", encoding="utf-8") as f:
        data = json.load(f)

    run_data = data[run_abbr]
    logical_index = run_data["logical_index"]
    return logical_index


def logical_to_json(run_abbr: str, logical_power: dict):
    """Save logical value to json"""
    with jsonu.JSONExporter("metadata_out.json") as data:
        data[run_abbr]["logical_power"] = logical_power


def get_logical_power(abbr: str, logical_idx: Optional[int] = None):
    """Get b int value"""

    # Get data
    filename = u.get_filename(abbr)
    basename = u.get_basename(filename)
    df: pd.DataFrame = get_combinedf(basename)

    assert "current" in df.columns.values, "No column in df with name : 'current'"

    logical_idx = logical_index_from_json(abbr)

    assert logical_idx <= len(df.index), ""
    log_curr = df.current[logical_idx]
    log_volt = df.voltage[logical_idx]

    log_pow = log_curr * log_volt

    logical_to_json(
        abbr,
        {
            "current": log_curr,
            "voltage": log_volt,
            "power": log_pow,
            "index": str(logical_idx),
        },
    )

    return log_pow


def fmin_to_json(run_abbr: str, optimal_data: Dict[str, float]):
    """Save optimal fmin data in metadata_out.json"""
    with jsonu.JSONExporter("metadata_out.json") as data:
        data[run_abbr]["optimal"] = optimal_data


def model_values_to_json(run_abbr: str, model_name: str, model_values: dict):
    """Save model values to json file

    Args:
        run_abbr: Experiment abbreviation
        model_name: Model name
        model_values: Dictionary with values to export
    """
    with jsonu.JSONExporter("metadata_out.json") as data:
        data[run_abbr][model_name] = model_values


def model_values_from_json(run_abbr: str, model_name: str) -> dict:
    """Get model values from json file

    Args:
        run_abbr: Experiment abbreviation
        model_name: Model name
    """
    with jsonu.JSONImporter("metadata_out.json") as data:
        return data[run_abbr][model_name]


def model_subset_from_json(run_abbreviations: Sequence[str], model_name: str) -> dict:
    """Get model values from json file

    Args:
        run_abbreviations: Sequences of experiment abbreviations
        model_name: Model name
    """
    with jsonu.JSONImporter("metadata_out.json") as data:
        return {abbr: data[abbr][model_name] for abbr in run_abbreviations}


def ssmrcoeffs_to_json(run_abbr: str, df):
    """Save ssmr coefficients in metadata_out.json"""
    n = 3 if "B_sym" in df.columns.values else 2
    if n == 3:
        coeffs = df[["m_asym", "G_asym", "B_asym", "m_sym", "G_sym", "B_sym"]].copy()
    else:
        coeffs = df[["m_asym", "G_asym", "m_sym", "G_sym"]].copy()

    ssmr_data = {
        col: {
            "min": coeffs[col].min(),
            "max": coeffs[col].max(),
            "idxmin": str(coeffs[col].argmin()),
            "idxmax": str(coeffs[col].argmax()),
            "last": coeffs[col].iloc[-1],
        }
        for col in coeffs.columns.values
    }

    with jsonu.JSONExporter("metadata_out.json") as data:
        data[run_abbr][f"ssmr{n}_coeffs"] = ssmr_data


def naive_icr(pre_df: pd.DataFrame):
    """Add Naive ICR values to a dataframe

    Args:
        df (pd.DataFrame): DataFrame without ICR values

    Returns:
        pd.DataFrame: DataFrame with ICR values
    """

    # if len(pre_df.meta.models) > 0:
    #     return pre_df

    df = pre_df.copy()
    df.meta = pre_df.meta

    df["ICRx"] = -df.vy / df.wz
    df["ICRy"] = df.vx / df.wz

    df["ICRyL"] = (df.vx - df.vL) / df.wz
    df["ICRyR"] = (df.vx - df.vR) / df.wz

    if df.meta.ugv_Bs < 0:
        # Not set
        df["ICRyL"] = (df.vx - df.vL) / df.wz
        df["ICRyR"] = (df.vx - df.vR) / df.wz
        df["Bs"] = (df.ICRyL - df.ICRyR).abs()
    else:
        df["Bs"] = df.meta.ugv_Bs
        df["ICRyL"] = df.Bs / 2
        df["ICRyR"] = -df.Bs / 2

    df["R"] = df.Bs / 2 * ((df.vR + df.vL) / (df.vR - df.vL))

    return df


def pcnt_err(measure: float, theoretical: float):
    """Compute the percent of error based on energy consumption

    Args:
        x (float): Measured value
        x (float): Theoretical value

    Returns:
        float: Error percentage
    """
    return (measure - theoretical) / theoretical


def energy_err(measured: pd.Series, estimated: pd.Series) -> float:
    """Get final energy consumption percentage of error

    Args:
        measured: Measured energy
        estimated: Estimated energy

    Returns:
        float: percentage of error
    """
    e_meas = measured.dropna()
    e_esti = estimated.dropna()

    last_meas_idx = e_meas.index[-1]
    last_esti_idx = e_esti.index[-1]

    last_idx = min(last_meas_idx, last_esti_idx)

    last_measured = measured.iloc[last_idx]
    last_estimated = estimated.iloc[last_idx]

    return pcnt_err(last_estimated, last_measured)
