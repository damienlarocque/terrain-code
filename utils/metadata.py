import json
from typing import Dict, Optional


def get_json_spans() -> Optional[dict]:
    data = None
    with open("metadata_in.json", "r", encoding="utf-8") as f:
        data = json.load(f)
    return data


def get_run_metadata(abbr):
    """Get run metadata"""
    with open("metadata_out.json", "r", encoding="utf-8") as f:
        data = json.load(f)

    return data[abbr]


def get_run_info(abbr: str, key: str = "path"):
    """Get specific data key"""
    metadata = get_run_metadata(abbr)
    return metadata[key]


def final_from_json(run_abbr: str):
    with open("metadata_out.json", "r", encoding="utf-8") as f:
        data = json.load(f)
    run_data = data[run_abbr]
    final_values = run_data.get("final_values", None)
    return final_values


def final_to_json(run_abbr: str, final_data: Dict[str, str]):
    with open("metadata_out.json", "r", encoding="utf-8") as f:
        data = json.load(f)

    data[run_abbr]["final_values"] = final_data

    with open("metadata_out.json", "w", encoding="utf-8") as f:
        json.dump(data, f, indent=2, sort_keys=True)
