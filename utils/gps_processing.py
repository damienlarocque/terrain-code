from pathlib import Path
from typing import Optional, Union

import cartopy.crs as ccrs
import numpy as np
import pandas as pd
import utm

import utils as u

data_path = Path("data")
gps_data = data_path / "gps_raw"

# SIDES = ("back", "front")
SIDES = ("back",)

ZONE_NUMBER = 19
ZONE_LETTER = "T"

from geopy import Point
from geopy.distance import distance


def compute_traveled_distance(lat: pd.Series, lon: pd.Series) -> pd.Series:
    geodf = pd.DataFrame({"lat": lat, "lon": lon})
    geodf["pts"] = geodf.apply(lambda row: Point(latitude=row.lat, longitude=row.lon), axis=1)
    geodf["pts_next"] = geodf.pts.shift(1)
    geodf.pts_next.loc[geodf.pts_next.isna()] = None
    geodf["distance"] = geodf.apply(lambda r: distance(r.pts, r.pts_next).m if r.pts_next is not None else 0, axis=1)
    return geodf.distance


def reprojector(target_crs: ccrs.Projection, source_src: ccrs.Projection):
    def reproject_pts(pts: np.ndarray) -> np.ndarray:
        """Reproject points

        Args:
            pts (np.ndarray): Longitude in first column, latitude in second

        Returns:
            np.ndarray: Longitude in first columns, latitude in second
        """
        new_coords = target_crs.transform_points(
            source_src,
            pts[:, 1],
            pts[:, 0],
        )
        return new_coords[:, :2]

    return reproject_pts


class GPSOrigin:
    """GPS absolute point"""

    ZONE_NUMBER = 19
    ZONE_LETTER = "T"

    def __init__(self, lat: float = 0, long: float = 0, east: float = 0, nort: float = 0) -> None:
        self.lat = lat
        self.long = long
        self.east = east
        self.nort = nort

    @classmethod
    def from_latlon(cls, lat: float = 0, long: float = 0):
        east, nort = utm.from_latlon(lat, long)[:2]
        return cls(lat=lat, long=long, east=east, nort=nort)

    @classmethod
    def from_utm(cls, east: float = 0, nort: float = 0):
        lat, long = utm.to_latlon(east, nort, cls.ZONE_NUMBER, cls.ZONE_LETTER)
        return cls(lat=lat, long=long, east=east, nort=nort)


def format_coord(number):
    """Format GPS coordinate for matplotlib axis

    Use it with `ax.xaxis.set_major_formatter(format_coord)`

    """
    return f"{number*1e3:.1f}m"


# def compute_distances(gps_df: pd.DataFrame, side: str, n: int = 1):
#     assert side in ("back", "front"), f"Front or back, nothing else, not {side}"
#     latitude = gps_df[f"{side}_latitude"]
#     longitude = gps_df[f"{side}_longitude"]

#     # Convert to utm
#     utm_data = utm.from_latlon(latitude.to_numpy(), longitude.to_numpy())
#     easting, northing = utm_data[:2]

#     # Use easting and northing to get distances
#     e_diff = easting[n:] - easting[:-n]
#     n_diff = northing[n:] - northing[:-n]
#     # e_diff, n_diff = np.diff(easting), np.diff(northing)
#     dist = np.sqrt(np.square(e_diff) + np.square(n_diff))

#     # Transform to pd.Series
#     for _ in range(n):
#         dist = np.insert(dist, 0, np.nan)
#     gps_df[f"{side}_dist"] = pd.Series(dist)


# def save_gps_data(basename: Union[Path, str]):
#     """Save GPS data in CSVs with normalized data columns:
#     (time, lat, long, alt)"""

#     # GPS name
#     gpsname = get_gps_filename(Path(basename).name)

#     # Open Front data
#     front_path = gps_data / f"{gpsname}_gps_odom_front.txt"
#     front_colnames = ["time", "latitude", "longitude", "altitude"]
#     front_df: pd.DataFrame = pd.read_csv(front_path, sep=" ", header=None, names=front_colnames)

#     # Open Back data
#     back_path = gps_data / f"{gpsname}_gps_odom_back.txt"
#     back_colnames = ["time", "utm_northing", "utm_easting", "altitude"]  # Bad column disposition
#     back_df: pd.DataFrame = pd.read_csv(back_path, sep=" ", header=None, names=back_colnames)

#     back_lat, back_long = utm.to_latlon(back_df.utm_easting, back_df.utm_northing, ZONE_NUMBER, ZONE_LETTER)
#     back_df["latitude"] = back_lat
#     back_df["longitude"] = back_long

#     back_df: pd.DataFrame = back_df[front_colnames]

#     export_path = u.get_rundir(basename)
#     export_gps = export_path / "gps"

#     front_df.to_csv(export_gps / f"{basename}_gps_gps_coords_front.csv")
#     back_df.to_csv(export_gps / f"{basename}_gps_gps_coords_back.csv")


def fix_to_coords(basename: Union[Path, str], side: str) -> Optional[Union[Path, str]]:
    assert side in ("back", "front"), f"Front or back, nothing else, not {side}"

    rundir = u.get_rundir(basename)
    gps_csv_path = rundir / "gps"

    fix_path = gps_csv_path / f"{basename}_gps_gps_fix_{side}.csv"
    coords_path = gps_csv_path / f"{basename}_gps_gps_coords_{side}.csv"

    try:
        fix_df = pd.read_csv(fix_path)
    except (pd.errors.EmptyDataError, FileNotFoundError):
        return None

    fix_df.drop(columns=["time"], inplace=True)
    simple_cols = {
        f"/gps/fix_{side}/altitude": "altitude",
        f"/gps/fix_{side}/latitude": "latitude",
        f"/gps/fix_{side}/longitude": "longitude",
        f"/gps/fix_{side}/time": "time",
    }
    fix_df = fix_df.rename(columns=simple_cols, errors="raise")

    coords_df = fix_df[["time", "latitude", "longitude", "altitude"]]
    coords_df.to_csv(coords_path, index=False)

    return coords_path


def get_gps_filename(name: str) -> str:
    """Get GPS Filename
    Run3_2021-03-31-14-03-54 -> run3
    """
    return (name.split("_")[0]).lower()


def read_pos_file(filename):
    return pd.read_table(filename, sep="\s+", parse_dates={"Timestamp": [0, 1]}, skiprows=9)
