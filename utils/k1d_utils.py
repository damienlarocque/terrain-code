import json
from pathlib import Path
from typing import Union

import pandas as pd

import utils as u
from utils import electrical_processing as elecp


def b_int_from_json(run_abbr: str):
    with open("metadata_out.json", "r", encoding="utf-8") as f:
        data = json.load(f)
    run_data = data[run_abbr]
    sens_ener = run_data.get("sensor_energy", None)
    return sens_ener


def b_int_to_json(run_abbr: str, sensor_energy: dict):
    with open("metadata_out.json", "r", encoding="utf-8") as f:
        data = json.load(f)

    data[run_abbr]["sensor_energy"] = sensor_energy

    with open("metadata_out.json", "w", encoding="utf-8") as f:
        json.dump(data, f, indent=2, sort_keys=True)


def rr_to_json(run_abbr: str, df):
    """Save rr summary in metadata_out.json"""
    rr = df.rr_rls.copy()
    rr_data = {
        "min": rr.min(),
        "max": rr.max(),
        "idxmin": str(rr.argmin()),
        "idxmax": str(rr.argmax()),
    }

    with open("metadata_out.json", "r", encoding="utf-8") as f:
        data = json.load(f)

    data[run_abbr]["roll_res"] = rr_data

    with open("metadata_out.json", "w", encoding="utf-8") as f:
        json.dump(data, f, indent=2, sort_keys=True)
