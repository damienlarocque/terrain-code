import json
from pathlib import Path


class JSONImporter:
    def __init__(self, path):
        self.path = Path(path)
        self.data = None

    def __enter__(self):
        with open(self.path, mode="r", encoding="utf-8") as f:
            self.data = json.load(f)
        return self.data

    def __exit__(self, exc_type, exc_value, exc_tb):
        pass


class JSONExporter:
    def __init__(self, path):
        self.path = Path(path)
        self.data = None

    def __enter__(self):
        with open(self.path, mode="r", encoding="utf-8") as f:
            self.data = json.load(f)
        return self.data

    def __exit__(self, exc_type, exc_value, exc_tb):
        if self.data is not None:
            with open(self.path, mode="w", encoding="utf-8") as f:
                json.dump(self.data, f, indent=2, sort_keys=True)


def export_descriptions_json(abbr: str, fname: str, terrain: str) -> None:
    """Export descriptions in file

    Args:
        abbr (str): abbreviation to export
        fname (str): filename to export
        terrain (str): terrain type to export
    """
    with JSONExporter("descriptions.json") as descriptions:
        meta = fname.split("_")[1]
        date = "-".join(meta.split("-")[:3])
        desc = meta.split("-")[-1]
        descriptions[abbr] = f"{terrain.title()} - {date} - {desc}"
