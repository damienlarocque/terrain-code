from __future__ import annotations

from pathlib import Path
from typing import Dict, List, Sequence

import yaml


class YAMLImporter:
    def __init__(self, path: Path | str):
        self.path = Path(path)
        self.data = None

    def __enter__(self):
        with open(self.path, mode="r", encoding="utf-8") as f:
            self.data = yaml.safe_load(f)
        return self.data

    def __exit__(self, exc_type, exc_value, exc_tb):
        pass


class YAMLExporter:
    def __init__(self, path: Path | str):
        self.path = Path(path)
        self.data = None

    def __enter__(self):
        with open(self.path, mode="r", encoding="utf-8") as f:
            self.data = yaml.safe_load(f)
        return self.data

    def __exit__(self, exc_type, exc_value, exc_tb):
        if self.data is not None:
            with open(self.path, mode="w", encoding="utf-8") as f:
                yaml.safe_dump(self.data, f, indent=2, sort_keys=True)


def export_descriptions_yaml(abbr: str, fname: str, terrain: str) -> None:
    """Export descriptions in file

    Args:
        abbr (str): abbreviation to export
        fname (str): filename to export
        terrain (str): terrain type to export
    """
    with YAMLExporter("descriptions.yaml") as descriptions:
        meta = fname.split("_")[1]
        date = "-".join(meta.split("-")[:3])
        desc = meta.split("-")[-1]
        descriptions[abbr] = f"{date} - {terrain.title()} - {desc}"


def import_terrain_abbrs_yaml(terrain_request: Sequence[str] | None = None) -> Dict[str, List[str]]:
    """Get all abbreviations for each terrain

    Args:
        terrain_request (Sequence[str], optional): selections of terrains to return

    Returns:
        Dict[str, str]: Abbreviations for each type of terrain
    """
    with YAMLImporter("descriptions.yaml") as descriptions:
        descs = {abbr: desc.lower() for abbr, desc in descriptions.items()}

    terrains = {abbr: desc.split(" - ")[1] for abbr, desc in descs.items()}

    terrain_abbrs = {}
    for abbr, terr in terrains.items():
        terrain_abbrs.setdefault(terr, []).append(abbr)

    if terrain_request is None:
        # Return all abbreviations
        return terrain_abbrs

    # Return requested terrains
    request = (terr.lower() for terr in terrain_request)
    return {terr: terrain_abbrs[terr] for terr in request}
