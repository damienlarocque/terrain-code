from typing import Sequence

import numpy as np
import pandas as pd

import utils as u
from utils import imu_processing as imup
from utils import timeseries_processing as timep


class RunMetadata:
    """Object to store dataframe metadata"""

    models: list = []

    abbr: str = ""
    terrain: str = ""

    ugv_name: str = ""
    ugv_mass: float = -1

    # Radius, Baseline, Length
    ugv_wr: float = -1
    ugv_wb: float = -1
    ugv_wl: float = -1

    # Slip Track
    ugv_Bs: float = -1

    # Motor parameters
    motor_Kt: float = -1  # in Nm/A
    motor_Ke: float = -1
    motor_I_nl: float = -1  # in A
    motor_R: float = -1
    motor_L: float = -1
    gear_ratio: float = -1
    gear_eta: float = -1
    motor_I_bias: float = -1  # in A


def get_ugv(abbr: str) -> str:
    """Get ugv name from the experiment abbreviation

    Args:
        abbr: Experiment abbreviation

    Returns:
        str: UGV name
    """
    ugv_runs = {
        "warthog": [
            "D01",
            "D02",
            "D03",
            "D04",
            "D05",
            "D06",
            "D07",
            "D08",
            "D09",
            "D10",
            "D11",
            "D12",
            "D13",
            "D14",
            "D15",
            "D16",
            "D17",
            "D18",
            "D19",
            "D20",
            "D21",
            "D22",
            "D23",
            "M01",
            "M02",
            "M03",
            "M04",
            "M05",
            "M06",
            "M07",
            "P01",
            "P02",
            "P03",
            "P04",
            "P05",
            "P06",
            "P07",
            "P08",
            "P09",
            "P10",
            "P11",
            "R01",
            "R02",
            "R03",
            "R04",
            "R05",
            "R06",
            "R07",
            "R08",
            "R09",
            "R10",
            "R11",
            "R12",
            "R13",
            "R14",
            "S01",
            "S02",
            "S03",
            "S04",
            "S05",
            "S06",
            "S07",
            "S08",
            "S09",
            "S10",
            "S11",
            "S12",
            "S13",
            "S14",
            "S15",
            "S16",
            "S17",
            "S18",
            "S19",
            "S20",
            "S21",
            "S22",
            "S23",
            "S24",
            "S25",
            "S26",
            "S27",
            "S28",
            "S29",
            "S30",
            "S31",
            "TA",
            "TB",
            "TC",
            "TCb",
        ],
        "marmotte": [
            "D98",
            "D99",
        ],
        "husky": [
            "Y01",
            "Y02",
            "Y03",
            "Y04",
            "Y05",
            "Y06",
            "Y07",
            "Y08",
            "Y09",
            "Y10",
            "Y11",
            "Y12",
            "Y13",
            "Y14",
            "Y15",
            "Y16",
            "Y17",
            "Y18",
            "Y19",
            "Y20",
            "H01",
            "H02",
            "H03",
            "H04",
            "H05",
            "H06",
            "H07",
            "H08",
            "H09",
            "H10",
            "H11",
            "H12",
            "H13",
            "H14",
            "H15",
            "H16",
            "H17",
            "H18",
            "H19",
            "H20",
            "H21",
            "H22",
            "H23",
            "H24",
            "H25",
            "H26",
            "H27",
            "H28",
            "H29",
            "H30",
            "H31",
            "H32",
            "H33",
            "H34",
            "H35",
            "H36",
            "H37",
            "H38",
            "H39",
            "H40",
            "H41",
            "H42",
            "H43",
            "H44",
            "H45",
            "H46",
            "H47",
            "H48",
            "H49",
            "H50",
            "H51",
            "H52",
            "H53",
            "H54",
            "H55",
            "H56",
            "H57",
            "H58",
            "H59",
            "H60",
            "H61",
            "H62",
            "H63",
            "H64",
            "H65",
            "H66",
            "H67",
            "H68",
            "H69",
            "H70",
            "H71",
            "H72",
            "H73",
            "H74",
        ],
    }
    ugv_candidates = tuple(ugv for ugv, runs in ugv_runs.items() if abbr in runs)
    assert len(ugv_candidates) > 0, f"There are no UGV that corresponds to {abbr}"
    assert len(ugv_candidates) == 1, f"There are more than one UGV that corresponds to {abbr}"
    return ugv_candidates[0]


def prepare_warthog(abbr: str, pre_df: pd.DataFrame) -> pd.DataFrame:
    """Filter out and rename columns, preprocess data
    Warthog data

    Args:
        abbr: Experiment abbreviation
        df: DataFrame to prepare

    Returns:
        pd.DataFrame: renamed dataframe
    """
    # TODO: Check how we can fix topic names for consistency
    # if "/imu_and_wheel_odom/lin_twi/x" not in pre_df.columns.values:
    #     rename_cols = {
    #         "/warthog_velocity_controller/odom/lin_twi/x": "/imu_and_wheel_odom/lin_twi/x",
    #         "/warthog_velocity_controller/odom/ang_twi/z": "/imu_and_wheel_odom/ang_twi/z",
    #     }
    #     pre_df.rename(columns=rename_cols, inplace=True, errors="raise")
    #     # removed_columns = (
    #     #     "/imu_and_wheel_odom/lin_twi/y",
    #     #     "/imu_and_wheel_odom/lin_twi/z",
    #     #     "/imu_and_wheel_odom/ang_twi/x",
    #     #     "/imu_and_wheel_odom/ang_twi/y",
    #     # )
    #     # for col in removed_columns:
    #     #     pre_df[col] = 0

    if "/cmd_vel/linear/x" not in pre_df.columns.values:
        rename_cols = {
            "/warthog_velocity_controller/cmd_vel/linear/x": "/cmd_vel/linear/x",
            "/warthog_velocity_controller/cmd_vel/angular/z": "/cmd_vel/angular/z",
        }
        pre_df.rename(columns=rename_cols, inplace=True, errors="raise")

    # Retrieve topics of interest
    coi = [
        "time",
        "time_rel",
        "/imu_and_wheel_odom/lin_twi/x",
        "/imu_and_wheel_odom/lin_twi/y",
        "/imu_and_wheel_odom/lin_twi/z",
        "/MTI_imu/data/ang_vel/z",
        "/mcu/status/current_battery",
        "/mcu/status/measured_battery",
        "/mcu/status/current_computer",
        "/MTI_imu/data/lin_acc/x",
        "/MTI_imu/data/lin_acc/y",
        "/MTI_imu/data/lin_acc/z",
        "/imu_and_wheel_odom/orientation/roll",
        "/imu_and_wheel_odom/orientation/pitch",
        "/imu_and_wheel_odom/orientation/yaw",
        "/MTI_imu/data/orientation/w",
        "/MTI_imu/data/orientation/x",
        "/MTI_imu/data/orientation/y",
        "/MTI_imu/data/orientation/z",
        "/cmd_vel/linear/x",
        "/cmd_vel/angular/z",
        "/left_drive/status/speed",
        "/right_drive/status/speed",
        "/imu_and_wheel_odom/position/x",
        "/imu_and_wheel_odom/position/y",
        "/imu_and_wheel_odom/position/z",
        "/left_drive/status/battery_current_signed",
        "/right_drive/status/battery_current_signed",
        "/left_drive/status/battery_voltage",
        "/right_drive/status/battery_voltage",
    ]

    if "back_altitude" in pre_df.columns.values:
        coi.extend(
            [
                "back_latitude",
                "back_longitude",
                "back_altitude",
            ]
        )
    df = pre_df[coi].copy()

    rename_cols = {
        "/imu_and_wheel_odom/lin_twi/x": "vx",
        "/imu_and_wheel_odom/lin_twi/y": "vy",
        "/imu_and_wheel_odom/lin_twi/z": "vz",
        "/imu_and_wheel_odom/position/x": "px",
        "/imu_and_wheel_odom/position/y": "py",
        "/imu_and_wheel_odom/position/z": "pz",
        "/MTI_imu/data/ang_vel/z": "wz",
        "/mcu/status/current_battery": "current",
        "/mcu/status/measured_battery": "voltage",
        "/mcu/status/current_computer": "current_cpu",
        "/MTI_imu/data/lin_acc/x": "ax",
        "/MTI_imu/data/lin_acc/y": "ay",
        "/MTI_imu/data/lin_acc/z": "az",
        "/imu_and_wheel_odom/orientation/roll": "psi",
        "/imu_and_wheel_odom/orientation/pitch": "theta",
        "/imu_and_wheel_odom/orientation/yaw": "phi",
        "/MTI_imu/data/orientation/w": "qw",
        "/MTI_imu/data/orientation/x": "qx",
        "/MTI_imu/data/orientation/y": "qy",
        "/MTI_imu/data/orientation/z": "qz",
        "/cmd_vel/linear/x": "cmd_v",
        "/cmd_vel/angular/z": "cmd_w",
        "/left_drive/status/speed": "wL",
        "/right_drive/status/speed": "wR",
        "/left_drive/status/battery_current_signed": "I_L",
        "/right_drive/status/battery_current_signed": "I_R",
        "/left_drive/status/battery_voltage": "V_L",
        "/right_drive/status/battery_voltage": "V_R",
    }
    if "back_altitude" in pre_df.columns.values:
        rename_cols = {
            **rename_cols,
            **{
                "back_latitude": "lat",
                "back_longitude": "long",
                "back_altitude": "alt",
            },
        }
    df.rename(columns=rename_cols, inplace=True, errors="raise")

    imu_cols = df[["ax", "ay", "az", "qw", "qx", "qy", "qz"]].to_numpy()
    imu_data = [([ax, ay, az], [qw, qx, qy, qz]) for ax, ay, az, qw, qx, qy, qz in imu_cols]
    corr_quat = [imup.remove_gravity(acc, imup.Quaternion.from_list(att)) for acc, att in imu_data]
    accelerations = np.array([cq.to_list()[1:] for cq in corr_quat])

    # Assert that all w components are close to zero
    zero_w = np.allclose([q.w for q in corr_quat], 0)
    assert zero_w, "All quaternions should have a w value close to 0"

    # Add corrected accelerations to dataframe
    corr_lab = ["ax_corr", "ay_corr", "az_corr"]
    for idx, elem in enumerate(corr_lab):
        df[elem] = accelerations[:, idx]

    for ecol in ("I_L", "I_R", "V_L", "V_R"):
        df[ecol] = df[ecol].ffill()
        if np.isnan(df[ecol].iloc[0]):
            df[ecol] = df[ecol].bfill()

    if np.isnan(df.wL.iloc[0]):
        df.wL = df.wL.bfill()
    if np.isnan(df.wR.iloc[0]):
        df.wR = df.wR.bfill()

    # Invert for GPS theta standard
    # + go up
    # - go down
    df["theta"] = -df.theta
    # if abbr == "R11":
    #     df["theta"] = df.theta * np.sign(df.vx)

    df.meta = pre_df.meta

    return df


def prepare_marmotte(abbr: str, pre_df: pd.DataFrame) -> pd.DataFrame:
    """Filter out and rename columns, preprocess data
    Marmotte data

    Args:
        abbr: Experiment abbreviation
        pre_df: DataFrame to prepare

    Returns:
        pd.DataFrame: renamed dataframe
    """
    print(abbr)
    return pre_df


def prepare_husky(abbr: str, pre_df: pd.DataFrame) -> pd.DataFrame:
    """Filter out and rename columns, preprocess data
    Husky data

    Args:
        abbr: Experiment abbreviation
        pre_df: DataFrame to prepare

    Returns:
        pd.DataFrame: renamed dataframe
    """
    # Retrieve topics of interest
    coi = [
        "time",
        "time_rel",
        "/imu_and_wheel_odom/lin_twi/x",
        "/imu_and_wheel_odom/lin_twi/y",
        "/imu_and_wheel_odom/lin_twi/z",
        "/imu_and_wheel_odom/position/x",
        "/imu_and_wheel_odom/position/y",
        "/imu_and_wheel_odom/position/z",
        "/imu_and_wheel_odom/orientation/roll",
        "/imu_and_wheel_odom/orientation/pitch",
        "/imu_and_wheel_odom/orientation/yaw",
        "/MTI_imu/data/ang_vel/z",
        "/MTI_imu/data/lin_acc/x",
        "/MTI_imu/data/lin_acc/y",
        "/MTI_imu/data/lin_acc/z",
        "/MTI_imu/data/orientation/w",
        "/MTI_imu/data/orientation/x",
        "/MTI_imu/data/orientation/y",
        "/MTI_imu/data/orientation/z",
        "/husky_velocity_controller/cmd_vel_unstamped/linear/x",
        "/husky_velocity_controller/cmd_vel_unstamped/angular/z",
        "/joint_states/velocity/left",
        "/joint_states/velocity/right",
        "/status/voltage/battery",
        "/status/voltage/drive_left",
        "/status/voltage/drive_right",
        "/status/current/cpu",
        "/status/current/drive_left",
        "/status/current/drive_right",
    ]
    df = pre_df[coi].copy()

    rename_cols = {
        "/imu_and_wheel_odom/lin_twi/x": "vx",
        "/imu_and_wheel_odom/lin_twi/y": "vy",
        "/imu_and_wheel_odom/lin_twi/z": "vz",
        "/imu_and_wheel_odom/position/x": "px",
        "/imu_and_wheel_odom/position/y": "py",
        "/imu_and_wheel_odom/position/z": "pz",
        "/imu_and_wheel_odom/orientation/roll": "psi",
        "/imu_and_wheel_odom/orientation/pitch": "theta",
        "/imu_and_wheel_odom/orientation/yaw": "phi",
        "/MTI_imu/data/ang_vel/z": "wz",
        "/MTI_imu/data/lin_acc/x": "ax",
        "/MTI_imu/data/lin_acc/y": "ay",
        "/MTI_imu/data/lin_acc/z": "az",
        "/MTI_imu/data/orientation/w": "qw",
        "/MTI_imu/data/orientation/x": "qx",
        "/MTI_imu/data/orientation/y": "qy",
        "/MTI_imu/data/orientation/z": "qz",
        "/husky_velocity_controller/cmd_vel_unstamped/linear/x": "cmd_v",
        "/husky_velocity_controller/cmd_vel_unstamped/angular/z": "cmd_w",
        "/joint_states/velocity/left": "wL",
        "/joint_states/velocity/right": "wR",
        "/status/current/cpu": "current_cpu",
        "/status/voltage/battery": "voltage",
        "/status/current/drive_left": "I_L",
        "/status/current/drive_right": "I_R",
        "/status/voltage/drive_left": "V_L",
        "/status/voltage/drive_right": "V_R",
    }
    df.rename(columns=rename_cols, inplace=True, errors="raise")

    #
    # Correct imu accelerations
    #
    imu_cols = df[["ax", "ay", "az", "qw", "qx", "qy", "qz"]].to_numpy()
    imu_data = [([ax, ay, az], [qw, qx, qy, qz]) for ax, ay, az, qw, qx, qy, qz in imu_cols]
    corr_quat = [imup.remove_gravity(acc, imup.Quaternion.from_list(att)) for acc, att in imu_data]
    accelerations = np.array([cq.to_list()[1:] for cq in corr_quat])

    # Assert that all w components are close to zero
    zero_w = np.allclose([q.w for q in corr_quat], 0)
    assert zero_w, "All quaternions should have a w value close to 0"

    # Add corrected accelerations to dataframe
    corr_lab = ["ax_corr", "ay_corr", "az_corr"]
    for idx, elem in enumerate(corr_lab):
        df[elem] = accelerations[:, idx]

    # Invert for GPS theta standard
    # + go up
    # - go down
    df["theta"] = -df.theta

    df.meta = pre_df.meta

    return df


def add_ugv_metadata(df: pd.DataFrame, ugv_name: str) -> pd.DataFrame:
    """Add UGV metadata to a DataFrame

    Args:
        ugv_name (str): _UGV name_
        df (pd.DataFrame): _Dataframe_

    Returns:
        pd.DataFrame: _DataFrame with added metadata_
    """
    if ugv_name == "warthog":
        # m (kg) from Baril2020
        df.meta.ugv_mass = 590
        df.meta.ugv_wr = 0.3
        df.meta.ugv_wb = 1.2
        df.meta.ugv_wl = 0.9
        df.meta.ugv_Bs = 1.93
        df.meta.motor_Kt = 0.1270
        df.meta.motor_Ke = 0.378 / np.pi
        df.meta.motor_I_nl = 0.05
        df.meta.motor_R = 0.0048
        df.meta.motor_L = 0.0820e-3
        df.meta.gear_ratio = 24
        df.meta.gear_eta = 1
        df.meta.motor_I_bias = 0
    elif ugv_name == "marmotte":
        pass
    elif ugv_name == "husky":
        # Data from Reina2016
        df.meta.ugv_mass = 70
        df.meta.ugv_wr = 0.13
        df.meta.ugv_wb = 0.6
        df.meta.ugv_wl = 0.55
        df.meta.ugv_Bs = 1.1
        df.meta.motor_Kt = 0.044
        df.meta.motor_Ke = 0.141 / np.pi
        df.meta.motor_I_nl = 1.35
        df.meta.motor_R = 0.46
        df.meta.motor_L = 0.22e-3
        df.meta.gear_ratio = 78.71
        df.meta.gear_eta = 0.7
        df.meta.motor_I_bias = 0.59

    return df


def read_df_rename_columns(abbr: str) -> pd.DataFrame:
    """Open and prepare df columns depending on the type of robot

    Args:
        abbr: Experiment abbreviation
        verbose: Is verbose ?

    Raises:
        NotImplementedError: abbr points to an unknown UGV

    Returns:
        pd.DataFrame: formatted and prepared dataframe
    """
    # Get data
    filename = u.get_filename(abbr)
    basename = u.get_basename(filename)
    rundir = u.get_rundir(basename)
    merged_path = rundir / "merged"
    df_path = merged_path / f"{basename}_mcf.csv"
    merged_df = pd.read_csv(df_path)

    ugv_name = get_ugv(abbr)
    merged_df.meta = RunMetadata()
    merged_df.meta.ugv_name = ugv_name.lower()
    merged_df.meta.abbr = abbr
    merged_df.meta.terrain = u.get_terrain(abbr).lower()

    if ugv_name == "warthog":
        df = prepare_warthog(abbr, merged_df)
    elif ugv_name == "marmotte":
        df = prepare_marmotte(abbr, merged_df)
    elif ugv_name == "husky":
        df = prepare_husky(abbr, merged_df)
    else:
        raise NotImplementedError(f"UGV '{ugv_name}' is not implemented")

    df = add_ugv_metadata(df, ugv_name)

    df.meta.models = []

    return df


def append_dfs(orig_dfs: Sequence[pd.DataFrame]) -> pd.DataFrame:
    has_meta = tuple(hasattr(odf, "meta") for odf in orig_dfs)
    assert all(has_meta), "All DataFrames must have a metadata attribute named 'meta'"

    vehicles = tuple(odf.meta.ugv_name for odf in orig_dfs)
    assert all(v == vehicles[0] for v in vehicles), "All DataFrames must be recorded with the same vehicle"

    dfs = [odf.copy() for odf in orig_dfs]
    for idx, odf in enumerate(orig_dfs):
        dfs[idx].meta = odf.meta
        dfs[idx]["terrain"] = odf.meta.terrain
        dfs[idx]["abbr"] = odf.meta.abbr
        dfs[idx]["time_rel"] = odf.time - timep.first(odf.time)
        dfs[idx]["run_idx"] = idx
        if idx == 0:
            continue
        dfs[idx] = dfs[idx].iloc[1:]
        dfs[idx].meta = odf.meta
        dfs[idx]["time"] = timep.last(dfs[idx - 1].time) + dfs[idx].time_rel
        dfs[idx]["time_rel"] = dfs[idx].time - timep.first(dfs[0].time)
        dfs[idx]["px"] = timep.last(dfs[idx - 1].px) + dfs[idx].px - timep.first(dfs[idx].px)
        dfs[idx]["py"] = timep.last(dfs[idx - 1].py) + dfs[idx].py - timep.first(dfs[idx].py)
        dfs[idx]["pz"] = timep.last(dfs[idx - 1].pz) + dfs[idx].pz - timep.first(dfs[idx].pz)

    app_df = pd.concat(list(dfs), ignore_index=True)
    app_df.meta = dfs[0].meta
    all_abbrs = tuple(df.meta.abbr for df in dfs)
    app_df.meta.abbr = "".join(all_abbrs)

    return app_df
