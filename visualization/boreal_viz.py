from typing import Dict, Optional

import matplotlib.pyplot as plt
import pandas as pd
import utils as u
import utm

from . import ColorbarLimits

boreal_orig = u.GPSOrigin.from_utm(337560, 5242970)


def plot_boreal_rr_mosaic(dfs: Dict[str, pd.DataFrame], rr_name: str = "rr_rls"):

    boreal_lim = ColorbarLimits(cbar_min=0.22, cbar_max=0.36)
    fig, subfigs = plt.subplots(nrows=4, ncols=3, figsize=(14, 10))

    cmap = plt.cm.get_cmap("viridis")
    cmap.set_over("xkcd:orange yellow")
    cmap.set_under("red")
    norm = plt.Normalize(vmin=boreal_lim.min, vmax=boreal_lim.max)

    for (run, df), ax in zip(dfs.items(), subfigs.flat):
        components = FigureComponents(ax, cmap, norm)
        plot_boreal_rr_inset(df, name=run, rr_name=rr_name, is_subfig=True, components=components)

    fig.tight_layout()
    fig.subplots_adjust(top=0.9, right=0.85)
    cbar_ax = fig.add_axes([0.9, 0.05, 0.05, 0.8])
    clb = fig.colorbar(plt.cm.ScalarMappable(norm=norm, cmap=cmap), extend="both", cax=cbar_ax)
    clb.ax.set_ylabel("Rolling resistance")
    # clb = fig.colorbar(plt.cm.ScalarMappable(norm=norm, cmap=cmap), extend="both", ax=ax)

    fig.suptitle("Rolling resistance near the Boreal")

    # plt.subplots_adjust(wspace=0.5)

    return fig


class FigureComponents:
    """Figure Components to pass through a function"""

    def __init__(self, ax, cmap, norm) -> None:
        self.ax = ax
        self.cmap = cmap
        self.norm = norm


def plot_boreal_rr_inset(
    df: pd.DataFrame,
    name: str,
    rr_name: str = "rr_rls",
    is_subfig: bool = False,
    components: Optional[FigureComponents] = None,
):
    boreal_lim = ColorbarLimits(cbar_min=0.22, cbar_max=0.36)

    orig_east = boreal_orig.east
    orig_nort = boreal_orig.nort

    long = df.long
    lat = df.lat
    rr = df[rr_name].to_list()

    # Convert to UTM for better axis
    ZONE_NUMBER = 19
    ZONE_LETTER = "T"
    east, nort = utm.from_latlon(lat, long, force_zone_number=ZONE_NUMBER, force_zone_letter=ZONE_LETTER)[:2]
    east = east - orig_east
    nort = nort - orig_nort

    if not is_subfig and components is None:
        fig, ax = plt.subplots()

        cmap = plt.cm.get_cmap("viridis")
        cmap.set_over("xkcd:orange yellow")
        cmap.set_under("red")
        norm = plt.Normalize(vmin=boreal_lim.min, vmax=boreal_lim.max)
    else:
        ax = components.ax
        cmap = components.cmap
        norm = components.norm
    sc = ax.scatter(east, nort, c=rr, norm=norm, cmap=cmap, s=2)
    ax.set_xlabel("Relative easting (m)")
    ax.set_ylabel("Relative northing (m)")
    ax.set_aspect("equal")

    # Easting, northing
    min_lim = u.GPSOrigin.from_utm(337560, 5242970)
    max_lim = u.GPSOrigin.from_utm(337760, 5243120)

    ax.set_xlim((min_lim.east - boreal_orig.east, max_lim.east - boreal_orig.east))
    ax.set_ylim((min_lim.nort - boreal_orig.nort, max_lim.nort - boreal_orig.nort))

    path = u.get_run_info(name, "path")
    temperature = u.get_run_info(name, "temperature")

    if is_subfig:
        ax.set_title(f"{name} - {path} (T° : {temperature}°C)")
        return sc

    clb = fig.colorbar(plt.cm.ScalarMappable(norm=norm, cmap=cmap), extend="both", ax=ax)
    clb.ax.set_ylabel("Rolling resistance")
    fig.suptitle(f"{name} - {path} - Rolling resistance near the boreal")

    return fig
