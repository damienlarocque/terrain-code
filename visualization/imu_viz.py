import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.stats import linregress


def compare_power_imu(df: pd.DataFrame, datatype: str, name: str, rotax="pitch"):
    """Electrical power vs IMU pitch"""

    plot_df = df[[f"imu_{datatype}_{rotax}", "power"]].copy()
    # slope_s = plot_df[f"imu_{datatype}_{rotax}"]
    # plot_df = plot_df[(slope_s > -0.5) & (slope_s < 0.5)]
    # plot_df = plot_df[np.abs(slope_s - slope_s.mean()) <= (3 * slope_s.std())]

    imuax = plot_df[f"imu_{datatype}_{rotax}"]
    power = plot_df.power

    colors = {"data": "b", "wheel": "r"}

    fig, ax = plt.subplots()
    ax.plot(imuax, power, f"o{colors[datatype]}", markersize=2, label=f"IMU {datatype.title()} {rotax}")

    # Trendline
    z = np.polyfit(imuax, power, 1)
    p = np.poly1d(z)
    params = {"m": z[0], "b": z[1]}
    ax.plot(imuax, p(imuax), "k--", label="y={m:.3f}x+{b:.3f}".format(**params))

    ax.set_xlabel(f"IMU {rotax} (rad)")
    ax.set_ylabel("Power [W]")

    # Get values for R²
    m, b, r_val, p_val, std_err = linregress(imuax, power)
    ax.set_title(f"y = {m:.3f}x + {b:.3f}, R²={r_val**2:.3f}")

    ax.legend()

    fig.suptitle(f"{name} - Power vs IMU {datatype.title()} {rotax}")

    return fig
