from .simple_viz import plot_sig_xy, plot_signal
from .timeseries_viz import nearby


class ColorbarLimits:
    """Colorbar limits"""

    def __init__(self, cbar_min, cbar_max) -> None:
        self.min = cbar_min
        self.max = cbar_max


glob_lim = ColorbarLimits(cbar_min=0.22, cbar_max=0.36)


def papermode(plt, size=8):
    plt.rc("font", family="serif", serif="Times")
    plt.rc("text", usetex=True)
    plt.rc("figure", titlesize=size)
    plt.rc("xtick", labelsize=size)
    plt.rc("ytick", labelsize=size)
    plt.rc("axes", labelsize=size, titlesize=size)
    plt.rc("legend", fontsize=size, title_fontsize=size)


def multiline(s: str) -> str:
    return "\n".join(s.split())


__all__ = (
    "plot_signal",
    "plot_sig_xy",
    "ColorbarLimits",
    "glob_lim",
    "nearby",
    "papermode",
    "multiline",
)
