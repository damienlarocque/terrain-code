from pathlib import Path
from typing import Union

import matplotlib.pyplot as plt
import pandas as pd
import utils as u
import utm

from visualization.simple_viz import plot_sig_xy

from . import glob_lim, plot_sig_xy, plot_signal


def k1d_plot_rollres_estimate(df: pd.DataFrame, rrname: str = "rr_simple", tag: str = ""):
    rel_time = df.time_rel
    rr = df[rrname]

    fig, ax = plt.subplots(figsize=(11, 8))

    ax.plot(rel_time, rr, "xkcd:orange", markersize=2)
    ax.set_xlabel("Duration (seconds)")
    ax.set_ylabel("Rolling resistance (no units) [1]")

    ax.set_xlim(left=0)
    # if name == "R11":
    #     ax.set_ylim(-0.1, 1)

    fig.suptitle(f"{tag} - Rolling resistance estimate vs time")

    return fig


def k1d_plot_compare_signal_rr_xy(df: pd.DataFrame, comp_sig: str, tag: str):
    signals = {"speed": "v", "acceleration": "a", "power": "P", "pitch": "theta"}

    assert comp_sig in signals, f"Unknown variable : {comp_sig}"

    xsig = df[signals[comp_sig]]
    rr = df.rr_rls

    fig, ax = plt.subplots()

    ax.plot(xsig, rr, "o", color="xkcd:orange", markersize=2)
    # clb = fig.colorbar(plt.cm.ScalarMappable(norm=norm, cmap=cmap), ax=ax)
    # clb.ax.set_ylabel("Data index")

    ax.set_xlabel(comp_sig.title())
    ax.set_ylabel("Rolling resistance")
    if comp_sig.lower() == "power":
        ax.set_ylim((0.2, 0.4))

    fig.suptitle(f"{tag} - Rolling resistance vs. {comp_sig.title()}")

    return fig


def k1d_plot_map_sigcolor(df: pd.DataFrame, tag: str):

    long = df.long
    lat = df.lat
    rr = df.rr_rls

    # if name == "R11":
    #     long = long[df.vx < 0]
    #     lat = lat[df.vx < 0]
    #     rr = rr[df.vx < 0]

    rr = rr.tolist()

    fig, ax = plt.subplots()

    cmap = plt.cm.get_cmap("viridis")
    cmap.set_over("xkcd:orange yellow")
    cmap.set_under("red")
    norm = plt.Normalize(vmin=glob_lim.min, vmax=glob_lim.max)
    ax.scatter(long, lat, c=rr, norm=norm, cmap=cmap, s=2)
    clb = fig.colorbar(plt.cm.ScalarMappable(norm=norm, cmap=cmap), extend="both", ax=ax)
    clb.ax.set_ylabel("Rolling resistance")

    ax.set_xlabel("Longitude (°)")
    ax.set_ylabel("Latitude (°)")

    fig.suptitle(f"{tag} - Rolling resistance along the run")

    return fig


def k1d_plot_compare_signal_rr_time(df: pd.DataFrame, name: str, topic: str, label: str):
    rel_time = df.time_rel
    signal = df[topic]
    rr = df.rr_rls.tolist()

    fig, ax = plt.subplots()

    cmap = plt.cm.get_cmap("viridis")
    cmap.set_over("xkcd:orange yellow")
    cmap.set_under("red")
    norm = plt.Normalize(vmin=glob_lim.min, vmax=glob_lim.max)
    ax.scatter(rel_time, signal, c=rr, norm=norm, cmap=cmap, s=2)
    clb = fig.colorbar(plt.cm.ScalarMappable(norm=norm, cmap=cmap), extend="both", ax=ax)
    clb.ax.set_ylabel("Rolling resistance")

    ax.set_xlabel("Duration (seconds)")
    ax.set_ylabel(label.title())

    ax.set_xlim(left=0)

    fig.suptitle(f"{name} - {label.title()} signal with rolling resistance")

    return fig


def k1d_plot_3d_path(df: pd.DataFrame, name: str):

    long: pd.Series = df.long
    lat: pd.Series = df.lat
    alt: pd.Series = df.alt
    rr: pd.Series = df.rr_rls

    if name.startswith("R11"):
        # A : Garage -> Gazebo
        # A : Gazebo -> Garage
        conds = {"a": df.vx > 0, "b": df.vx < 0}
        cond = conds[name[-1].lower()]

        long = long[cond]
        lat = lat[cond]
        alt = alt[cond]
        rr = rr[cond]

        long.index = pd.RangeIndex(len(long.index))
        lat.index = pd.RangeIndex(len(lat.index))
        alt.index = pd.RangeIndex(len(alt.index))
        rr.index = pd.RangeIndex(len(rr.index))

    rr = rr.tolist()

    # Convert to UTM for equal axis
    ZONE_NUMBER = 19
    ZONE_LETTER = "T"
    east, nort = utm.from_latlon(lat, long, force_zone_number=ZONE_NUMBER, force_zone_letter=ZONE_LETTER)[:2]
    rel_east = east - east[0]
    rel_nort = nort - nort[0]

    fig, ax = plt.subplots(subplot_kw={"projection": "3d"})

    cmap = plt.cm.get_cmap("viridis")
    cmap.set_over("xkcd:orange yellow")
    cmap.set_under("red")
    norm = plt.Normalize(vmin=0.22, vmax=0.36)
    ax.scatter(rel_east, rel_nort, alt, c=rr, norm=norm, cmap=cmap, s=2)
    clb = fig.colorbar(plt.cm.ScalarMappable(norm=norm, cmap=cmap), extend="both", ax=ax)
    clb.ax.set_ylabel("Rolling resistance")

    ax.set_xlabel("Easting (m)")
    ax.set_ylabel("Northing (m)")
    ax.set_zlabel("Elevation (m)")

    fig.suptitle(f"{name} - Rolling resistance along the path")

    return fig


def plot_k1d(basename: Union[Path, str]):
    # Get data
    rundir = u.get_rundir(basename)
    merged_path = rundir / "roll_res"
    df_path = merged_path / f"{basename}_rr.csv"
    rr_df = pd.read_csv(df_path)
    abbr = u.abbreviate(basename)

    figdir = u.get_figdir(basename)
    rrfigs = figdir / "roll_res"
    qusigfigs = figdir / "quann_sigs"

    fig = k1d_plot_rollres_estimate(rr_df, abbr)
    for ext in u.EXTS:
        fig.savefig(rrfigs / f"{basename}_rollres-simple.{ext}")

    fig = k1d_plot_rollres_estimate(rr_df, rrname="rr_rls", tag=abbr)
    for ext in u.EXTS:
        fig.savefig(rrfigs / f"{basename}_rollres-rls.{ext}")

    fig = k1d_plot_compare_signal_rr_xy(rr_df, comp_sig="pitch", tag=abbr)
    for ext in u.EXTS:
        fig.savefig(rrfigs / f"{basename}_rrd0_vs_pitch.{ext}")
    fig = k1d_plot_compare_signal_rr_xy(rr_df, comp_sig="power", tag=abbr)
    for ext in u.EXTS:
        fig.savefig(rrfigs / f"{basename}_rrd0_vs_power.{ext}")

    signals = {
        "speed": "v",
        "acceleration": "a",
        "power": "P",
        "pitch": "theta",
        "acceleration_x": "ax",
        "acceleration_y": "ay",
        "acceleration_z": "az",
        "corracc_x": "ax_c",
        "corracc_y": "ay_c",
        "corracc_z": "az_c",
        "speed_x": "vx",
        # "power_diff": "P-b",
    }

    for label, topic in signals.items():
        fig = plot_signal(rr_df, abbr, topic, label)
        for ext in u.EXTS:
            fig.savefig(qusigfigs / f"{basename}_{label}sig.{ext}")

    # Plot xy
    xy = {"x": "power", "y": "pitch"}
    fig = plot_sig_xy(rr_df, abbr, xy)
    for ext in u.EXTS:
        fig.savefig(qusigfigs / f"{basename}_sig_{xy['x']}{xy['y']}.{ext}")

    # Plot quann map
    fig = k1d_plot_map_sigcolor(rr_df, abbr)
    for ext in u.EXTS:
        fig.savefig(rrfigs / f"{basename}_map_rr.{ext}")

    # # Plot boreal inset
    # if abbr not in ("R11", "R08"):
    #     # RR Diff 0
    #     fig = k1d_plot_boreal_rr_inset(rr_df, abbr)
    #     for ext in u.EXTS:
    #         fig.savefig(rrfigs / f"{basename}_boreal_rr.{ext}")
    #     # Simple without rollong average
    #     fig = k1d_plot_boreal_rr_inset(rr_df, abbr, rr_name="rr_simple")
    #     for ext in u.EXTS:
    #         fig.savefig(rrfigs / f"{basename}_boreal_rr_simple.{ext}")

    # plt.close("all")

    # Plot quann signal rr
    fig = k1d_plot_compare_signal_rr_time(rr_df, abbr, "alt", "Elevation")
    for ext in u.EXTS:
        fig.savefig(rrfigs / f"{basename}_elevation_rr.{ext}")
    fig = k1d_plot_compare_signal_rr_time(rr_df, abbr, "rot_vel", "Angular velocity")
    for ext in u.EXTS:
        fig.savefig(rrfigs / f"{basename}_rotation_velocity.{ext}")
    fig = k1d_plot_compare_signal_rr_time(rr_df, abbr, "rotvel_mag", "Angular velocity magnitude")
    for ext in u.EXTS:
        fig.savefig(rrfigs / f"{basename}_rotation_magnitude.{ext}")
    fig = k1d_plot_compare_signal_rr_time(rr_df, abbr, "P", "Power")
    for ext in u.EXTS:
        fig.savefig(rrfigs / f"{basename}_power_rr.{ext}")

    # xy = {"x": "A", "y": "power_diff"}
    # fig = k1d_plot_quann_sig_xy(df, abbr, xy)
    # for ext in u.EXTS:
    #     fig.savefig(qusigfigs / f"{basename}_sig_{xy['x']}{xy['y']}.{ext}")
    # xy = {"x": "speed", "y": "power_diff"}
    # fig = k1d_plot_quann_sig_xy(df, abbr, xy)
    # for ext in u.EXTS:
    #     fig.savefig(qusigfigs / f"{basename}_sig_{xy['x']}{xy['y']}.{ext}")
    # xy = {"x": "speed", "y": "power"}
    # fig = k1d_plot_quann_sig_xy(df, abbr, xy)
    # for ext in u.EXTS:
    #     fig.savefig(qusigfigs / f"{basename}_sig_{xy['x']}{xy['y']}.{ext}")

    # anim = k1d.animate_pitch_power(df, abbr)
    # anim.save(qusigfigs / "pitchpower.gif", writer="pillow", fps=30)

    print("Max RR :", rr_df.rr_rls.max(), "at", rr_df.rr_rls.argmax())

    plt.close("all")
    print(f"{abbr} : Exported figs for rolling resistance and quann signals")


def plot_estimators(abbr: Union[Path, str]):
    # Get data
    basename = u.get_basename(u.get_filename(abbr))
    rundir = u.get_rundir(basename)
    merged_path = rundir / "k1d"
    df_path = merged_path / f"{basename}_k1d.csv"
    k1d_df = pd.read_csv(df_path)

    figdir = u.get_figdir(basename)
    rrfigs = figdir / "k1d"
    # qusigfigs = figdir / "k1d_sigs"

    fig = k1d_plot_rollres_estimate(k1d_df, tag=abbr)
    for ext in u.EXTS:
        fig.savefig(rrfigs / f"{abbr}_rollres-simple.{ext}")

    fig = k1d_plot_rollres_estimate(k1d_df, rrname="rr_rls", tag=abbr)
    for ext in u.EXTS:
        fig.savefig(rrfigs / f"{abbr}_rollres-rls.{ext}")

    print("Max RR :", k1d_df.rr_rls.max(), "at", k1d_df.rr_rls.argmax())

    plt.close("all")
    print(f"{abbr} : Exported figs for rolling resistance and quann signals")


if __name__ == "__main__":
    plot_k1d(basename=u.get_basename(u.get_filename("D05")))
