import json

import matplotlib.pyplot as plt
import pandas as pd
import utils as u


def get_rr_df(run_abbr: str) -> pd.DataFrame:
    fname = u.get_filename(run_abbr)
    base = u.get_basename(fname)
    # Data directory
    rundir = u.get_rundir(base)
    merged_path = rundir / "roll_res"
    df_path = merged_path / f"{base}_rr.csv"
    rr_df = pd.read_csv(df_path)
    return rr_df


def plot_rr_summary() -> plt.Figure:
    rr_data = {}
    rr_paths = {}
    for run in u.RUNS:
        fname = u.get_filename(run)
        base = u.get_basename(fname)
        # Data directory
        rundir = u.get_rundir(base)
        merged_path = rundir / "roll_res"
        df_path = merged_path / f"{base}_rr.csv"
        rr_df = pd.read_csv(df_path)
        rr_s = rr_df.rr_rls.copy()
        rr_data[run] = rr_s[rr_s.between(0, 1, inclusive="both")]
        # rr_data[run] = rr_s

        # Save paths in dict
        run_path = u.get_run_info(run)
        rr_paths.setdefault(run_path, []).append(run)

    rr_paths = {k: v for k, v in sorted(rr_paths.items())}

    ratios = [len(v) for v in rr_paths.values()]
    fig, subfigs = plt.subplots(ncols=len(rr_paths), sharey=True, gridspec_kw={"width_ratios": ratios})

    for (path, runs), ax in zip(rr_paths.items(), subfigs.reshape(-1)):

        ax.violinplot(dataset=[rr_data[r] for r in runs], showextrema=False)
        # ax.violinplot(dataset=[rr_data[r] for r in u.RUNS], showmeans=True, showmedians=True)
        ax.set_xticks([i + 1 for i in range(len(runs))])
        ax.set_xticklabels([r for r in runs])

        ax.set_ylim(0.1, 0.5)
        ax.grid(which="major", axis="y", color="k", ls="--")
        ax.minorticks_on()
        ax.grid(which="minor", axis="y", color="r", ls="--", alpha=0.5)
        ax.tick_params(axis="x", which="minor", bottom=False)

        ax.set_xlabel(path)

    fig.supxlabel("Runs")
    fig.supylabel("Rolling resistance")
    fig.subplots_adjust(bottom=0.15)

    return fig


def plot_bint_summary() -> plt.Figure:
    with open("metadata_out.json", "r", encoding="utf-8") as f:
        data: dict = json.load(f)

    sens_energ = {ru: r_dat["sensor_energy"] for ru, r_dat in sorted(data.items())}
    power = {run: se_en["power"] for run, se_en in sorted(sens_energ.items())}

    fig, ax = plt.subplots()

    pwr_df = pd.DataFrame(power.items(), columns=["run", "sens_pwr"])
    sens_pwr = pwr_df.sens_pwr

    ax.plot(pwr_df.run, pwr_df.sens_pwr, "ob", markersize=2, label="Sensor power")
    ax.set_xlabel("Runs")
    ax.set_ylabel(r"${b}_{int} [W]$")

    ax.set_ylim((0, 500))
    ax.set_title(
        r"Values of ${b}_{int} [W]$"
        + f"\nmin-(ave)-max : {sens_pwr.min():.2f}-({sens_pwr.mean():.2f})-{sens_pwr.max():.2f}"
    )

    return fig


def plot_summary():

    figdir = u.get_figdir("summary")
    rrfigs = figdir / "roll_res"

    fig = plot_rr_summary()
    for ext in ("png", "jpg", "pdf"):
        fig.savefig(rrfigs / f"summary_rr.{ext}")

    bintfigs = figdir / "sensor_energy"

    fig = plot_bint_summary()
    for ext in ("png", "jpg", "pdf"):
        fig.savefig(bintfigs / f"summary_bint.{ext}")

    # borfigs = figdir / "boreal"
    # boreal_runs = [r for r in u.RUNS if r not in ("R08", "R11")]
    # bor_dfs = {br: get_rr_df(br) for br in boreal_runs}
    # fig = plot_boreal_rr_mosaic(bor_dfs)
    # for ext in ("png", "jpg", "pdf"):
    #     fig.savefig(borfigs / f"summary_boreal.{ext}")


if __name__ == "__main__":
    plot_summary()
