from typing import Dict

import matplotlib as mpl
import matplotlib.pyplot as plt


def plot_bytes(frame_ID: str, can_dfs: Dict[str, dict], reltime_max: int = 1000) -> mpl.figure.Figure:
    frame_df = can_dfs[frame_ID]["csv"]
    packet_len = can_dfs[frame_ID]["len"]

    fig, subfigs = plt.subplots(nrows=packet_len, sharex=True)
    if packet_len > 1:
        for idx, ax in enumerate(subfigs.reshape(-1)):
            ax.plot(frame_df.time_rel, frame_df[f"b{idx}"])
            ax.set_ylabel(f"Byte ${{{idx}}}$")
            ax.yaxis.set_label_coords(-0.15, 0.5)
        ax.set_xlim((0, reltime_max))
    else:
        subfigs.plot(frame_df.time_rel, frame_df[f"b0"])
        subfigs.set_ylabel(f"Byte $0$")
        subfigs.yaxis.set_label_coords(-0.15, 0.5)
        subfigs.set_xlim((0, reltime_max))

    fig.supxlabel("Time (s)", fontsize=18)

    return fig
