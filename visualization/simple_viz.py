from typing import Dict

import matplotlib.pyplot as plt
import pandas as pd


def plot_signal(df: pd.DataFrame, topic: str, label: str, tag: str):
    rel_time = df.time_rel
    signal = df[topic]

    fig, ax = plt.subplots()

    ax.plot(rel_time, signal, "xkcd:orange", markersize=2)
    ax.set_xlabel("Duration (seconds)")
    ax.set_ylabel(label.title())

    ax.set_xlim(left=0)

    fig.suptitle(f"{tag} - {label.title()} signal")

    return fig


def plot_sig_xy(df: pd.DataFrame, xy: Dict[str, str], tag: str):
    signals = {"speed": "v", "acceleration": "a", "power": "P", "pitch": "theta"}

    assert "x" in xy and "y" in xy, f"Misformatted dictionary xy : {xy}"

    xsig = df[signals[xy["x"]]]
    ysig = df[signals[xy["y"]]]

    fig, ax = plt.subplots()

    cmap = plt.cm.get_cmap("viridis")
    idx = df.index.tolist()
    norm = plt.Normalize(vmin=min(idx), vmax=max(idx))
    ax.scatter(xsig, ysig, c=idx, norm=norm, cmap=cmap, s=2)
    clb = fig.colorbar(plt.cm.ScalarMappable(norm=norm, cmap=cmap), ax=ax)
    clb.ax.set_ylabel("Data index")

    ax.set_xlabel(xy["x"].title())
    ax.set_ylabel(xy["y"].title())

    fig.suptitle(f"{tag} - {xy['y'].title()} vs. {xy['x'].title()}")

    return fig
