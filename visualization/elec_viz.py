from pathlib import Path
from typing import Union

import matplotlib.pyplot as plt
import pandas as pd
import utils as u
from utils import electrical_processing as elecp
from utils import imu_processing as imup

from . import imu_viz as imuv


def plot_imupower(basename: Union[Path, str]):
    """Plot relation between Power and IMU data"""
    # Get data
    rundir = u.get_rundir(basename)
    merged_path = rundir / "merged"
    df_path = merged_path / f"{basename}_mcf.csv"
    merged_df = pd.read_csv(df_path)

    # Compute power
    elecp.warthog_pwr(merged_df)
    elecp.compute_energy(merged_df)

    # Retrieve topics of interest
    coi = [
        "time",
        "time_rel",
        "current",
        "voltage",
        "power",
        "/MTI_imu/data/orientation/pitch",
        "/imu_and_wheel_odom/orientation/pitch",
        "/MTI_imu/data/orientation/yaw",
        "/imu_and_wheel_odom/orientation/yaw",
        "/MTI_imu/data/orientation/roll",
        "/imu_and_wheel_odom/orientation/roll",
    ]
    imupow_df = merged_df[coi].copy()

    imup.shrink_topics(imupow_df)

    # Figure directory
    figdir = u.get_figdir(basename)
    imu_figs = figdir / "elecimu"
    abbr = u.abbreviate(basename)

    roll_figs = imu_figs / "roll"
    pitch_figs = imu_figs / "pitch"
    yaw_figs = imu_figs / "yaw"

    for d in (roll_figs, pitch_figs, yaw_figs):
        d.mkdir(parents=True, exist_ok=True)

    # Pitch
    for dtype in ("data", "wheel"):
        fig = imuv.compare_power_imu(imupow_df, dtype, abbr)
        fig.savefig(pitch_figs / f"{basename}_elecpitch_{dtype}.png")
        fig.savefig(pitch_figs / f"{basename}_elecpitch_{dtype}.jpg")
        fig.savefig(pitch_figs / f"{basename}_elecpitch_{dtype}.pdf")
        plt.close(fig)

    # Yaw
    for dtype in ("data", "wheel"):
        fig = imuv.compare_power_imu(imupow_df, dtype, abbr, rotax="yaw")
        fig.savefig(yaw_figs / f"{basename}_elecyaw_{dtype}.png")
        fig.savefig(yaw_figs / f"{basename}_elecyaw_{dtype}.jpg")
        fig.savefig(yaw_figs / f"{basename}_elecyaw_{dtype}.pdf")
        plt.close(fig)

    # Roll
    for dtype in ("data", "wheel"):
        fig = imuv.compare_power_imu(imupow_df, dtype, abbr, rotax="roll")
        fig.savefig(roll_figs / f"{basename}_elecroll_{dtype}.png")
        fig.savefig(roll_figs / f"{basename}_elecroll_{dtype}.jpg")
        fig.savefig(roll_figs / f"{basename}_elecroll_{dtype}.pdf")
        plt.close(fig)

    print(f"Exported figs for imu-power with {abbr}")
