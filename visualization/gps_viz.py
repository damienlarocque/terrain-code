from pathlib import Path
from typing import Union

import matplotlib.pyplot as plt
import pandas as pd
import utils as u
from utils import SIDES
from utils import electrical_processing as elecp
from utils import imu_processing as imup


def plot_elevation(df: pd.DataFrame, side: str, tag: str):
    plot_df = df[[f"{side}_altitude", "time_rel"]]

    elevation = plot_df[f"{side}_altitude"]
    rel_time = plot_df.time_rel

    colors = {"front": "b", "back": "r"}

    fig, ax = plt.subplots(figsize=(12, 4))
    ax.plot(rel_time, elevation, f"{colors[side]}", label=side.title())

    ax.set_xlabel("Relative time (seconds)")
    ax.set_ylabel("GPS - Elevation (m)")

    ax.set_xlim(rel_time.min(), rel_time.max())

    ax.legend()

    fig.suptitle(f"{tag} - GPS elevation from {side} GPS sensor")

    return fig


def plot_elevation_imupitch(df: pd.DataFrame, side: str, tag: str):
    plot_df = df[[f"{side}_altitude", "time_rel", "imu_wheel_pitch_deg"]]

    elevation = plot_df[f"{side}_altitude"]
    imu_pitch = plot_df["imu_wheel_pitch_deg"]
    rel_time = plot_df.time_rel

    fig, ax = plt.subplots(figsize=(12, 4))
    (p1,) = ax.plot(rel_time, elevation, "r", label=f"{side.title()} GPS elevation")

    ax2 = ax.twinx()
    (p2,) = ax2.plot(rel_time, imu_pitch, "g", label="IMU pitch")

    ax.set_xlabel("Relative time (seconds)")

    ax.set_ylabel("GPS - Elevation (m)")
    ax2.set_ylabel("Slope angle (°)")

    ax.set_xlim(left=0)
    ax.legend(handles=[p1, p2])

    fig.suptitle(f"{tag} - {side} GPS elevation / slope measurements from IMU")

    return fig


def plot_gps(basename: Union[Path, str]):
    """Plot GPS data"""
    # Get data
    rundir = u.get_rundir(basename)
    merged_path = rundir / "merged"
    df_path = merged_path / f"{basename}_mcf.csv"
    merged_df = pd.read_csv(df_path)

    # Compute power
    elecp.warthog_pwr(merged_df)
    elecp.compute_energy(merged_df)

    # Retrieve topics of interest
    coi = [
        "time",
        "time_rel",
        "front_latitude",
        "front_longitude",
        "front_altitude",
        "back_latitude",
        "back_longitude",
        "back_altitude",
        "/MTI_imu/data/orientation/pitch",
        "/imu_and_wheel_odom/orientation/pitch",
        "/MTI_imu/data/orientation/yaw",
        "/imu_and_wheel_odom/orientation/yaw",
        "/MTI_imu/data/orientation/roll",
        "/imu_and_wheel_odom/orientation/roll",
    ]
    geo_df = merged_df[coi].copy()

    # Compute distances and slopes
    imup.shrink_topics(geo_df)
    imup.compute_pitch_angles(geo_df)

    # Figure directory
    figdir = u.get_figdir(basename)
    slope_figs = figdir / "elevation"
    abbr = u.abbreviate(basename)

    # Power vs Time Slope
    for side in SIDES:
        fig = plot_elevation(geo_df, side, abbr)
        fig.savefig(slope_figs / f"{basename}_elevation_{side}.png")
        fig.savefig(slope_figs / f"{basename}_elevation_{side}.jpg")
        fig.savefig(slope_figs / f"{basename}_elevation_{side}.pdf")
        plt.close(fig)

    for side in SIDES:
        fig = plot_elevation_imupitch(geo_df, side, abbr)
        fig.savefig(slope_figs / f"{basename}_elevapitchimu_{side}.png")
        fig.savefig(slope_figs / f"{basename}_elevapitchimu_{side}.jpg")
        fig.savefig(slope_figs / f"{basename}_elevapitchimu_{side}.pdf")
        plt.close(fig)

    print(f"Exported figs for gps with {abbr}")
