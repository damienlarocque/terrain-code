from typing import Tuple

import matplotlib.pyplot as plt
import pandas as pd


def compare_timeseries(df1: pd.DataFrame, df2: pd.DataFrame):
    fig, (ax1, ax2) = plt.subplots(nrows=2, sharex=True)

    ax1.plot(df1.time, df1.iloc[:, 1])
    ax1.set_ylabel(df1.columns[1])

    ax2.plot(df2.time, df2.iloc[:, 1])
    ax2.set_ylabel(df2.columns[1])

    fig.supxlabel("Time [s]")

    return fig


def nearby(df: pd.DataFrame, index: int, span: int = 5, topic: str = "current", ylim: Tuple = None, **figargs):
    df_length = len(df.index)
    limits = (max(0, index - span), min(index + span, df_length))

    # if "plt" not in dir():
    #     import matplotlib.pyplot as plt
    # fig, ax = plt.subplots()
    # ax.axis("equal")

    # ylim = (0, 100) if ((topic == "current") and (ylim is None)) else None
    ylim = None

    series = df[topic]
    if "_altitude" in topic:
        series = series.diff()
    current_series = series.iloc[slice(*limits)]
    current_series.plot(xlim=limits, ylim=ylim, **figargs)
