import optuna
import pandas as pd
from sklearn.model_selection import train_test_split

import utils as u
from models import pentzer2014pow
from models.ML import terrain_MLP as tmlp

# papermode(plt, size=12)


class Objective:
    def __init__(self) -> None:
        terrain_abbrs = u.import_terrain_abbrs_yaml(("Sandy loam", "Asphalt", "Snow", "Flooring", "Ice"))
        terrain_abbrs = {
            terr: [abbr for abbr in terr_abbrs if u.get_ugv(abbr) == "husky"]
            for terr, terr_abbrs in terrain_abbrs.items()
        }
        abbrs = sorted([item for sublist in terrain_abbrs.values() for item in sublist])

        dfs = {}
        for abbr in abbrs:
            pentz_df = pentzer2014pow.open_model_df(abbr)
            dfs[abbr] = pentz_df
            dfs[abbr]["mass"] = pentz_df.meta.ugv_mass
            dfs[abbr]["terrain"] = u.get_terrain(abbr).lower()

        veloc = ("vx", "vy", "vz")
        acc_cor = ("ax_corr", "ay_corr", "az_corr")
        posit = ("px", "py", "pz")
        volts = ("V_L", "V_R")
        wheel = ("vL", "vR")
        morales = ("JS_sym", "JG")
        amps = ("I_L", "I_R")

        cols_x = [
            "mass",
            *veloc,
            *acc_cor,
            *posit,
            *volts,
            *wheel,
            *morales,
            *amps,
        ]

        cols_y = ["P_motion", "terrain"]

        terrain_dfs = {}
        for terr, terr_abbrs in terrain_abbrs.items():
            terr_dfs = [dfs[abbr] for abbr in terr_abbrs]
            terrain_dfs[terr] = pd.concat(terr_dfs, ignore_index=True)
            terrain_dfs[terr]["terrain"] = terr

        terr_df = pd.concat(terrain_dfs.values(), ignore_index=True)
        terr_df = terr_df[cols_x + cols_y].copy().dropna()

        X = terr_df[cols_x].copy()
        y = terr_df[cols_y].copy()
        self.terrains = sorted(terrain_dfs.keys())
        self.X_train, self.X_test, self.y_train, self.y_test = train_test_split(X, y)

    def __call__(self, trial: optuna.trial.Trial):
        X_train, y_train = self.X_train, self.y_train
        X_test, y_test = self.X_test, self.y_test

        n_iters = trial.suggest_int(
            "n_iters",
            1,
            3000,
            # log=True,
        )
        ter_mlp = tmlp.TerrainMLPRegressor(
            terrains=self.terrains,
            shuffle=True,
            max_iter=n_iters,
        )
        ter_mlp.fit(X_train, y_train)

        score = ter_mlp.score(X_test, y_test)
        return score


if __name__ == "__main__":
    objective = Objective()
    study = optuna.create_study(direction="maximize")
    study.optimize(objective, n_trials=100)
    print(study.best_params, study.best_trial)
    # n_iters = 2718
