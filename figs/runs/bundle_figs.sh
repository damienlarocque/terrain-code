# cp **/roll_*/**/*map_rr*.png collection/rolling_res/map
# cp **/roll_*/**/*map_rr*.jpg collection/rolling_res/map
# cp **/roll_*/**/*power_rr*.jpg collection/rolling_res/power
# cp **/roll_*/**/*boreal_rr.png collection/rolling_res/boreal
# cp **/roll_*/**/*boreal_rr_simple.png collection/rolling_res/boreal_simple
# cp **/roll_*/**/*rrd0_vs_power.png collection/rolling_res/power_vs_rrd0
# cp **/roll_*/**/*elevation_rr*.png collection/rolling_res/elevation
# cp **/roll*/**/*velocity*.png collection/rotvel/
# cp **/roll*/**/*magnitude*.png collection/rotvel/magnitude
# cp **/quann_*/**/*sig_powerpitch*.png collection/quann/powerpitch
# cp R*/naive*/**/*naive-energy*.png collection/naive/energy
# cp R*/naive*/**/*naive-time*.png collection/naive/time
# cp D*/k1d/*k1d_vel_power.jpg collection/k1d/power
# cp D*/k1d/*k1d_vel_rls.jpg collection/k1d/rls
# cp D*/k1d/*k1d_vel_estimate.jpg collection/k1d/naive

# cp D*/k1d_sigs/*k1d_ang_vel.jpg collection/k1d_sigs/ang_vel

# cp S*/ssmrk/*Perror_vx.jpg collection/ssmrk_sigs/errorvx/
# cp S*/ssmrk/*Perror_wz.jpg collection/ssmrk_sigs/errorwz/
# cp S*/ssmrk/*PEWMA.jpg collection/ssmrk_sigs/errorewma
# cp S*/ssmrk/*covasym.jpg collection/ssmrk_sigs/covasym
# cp S*/ssmrk/*covsym.jpg collection/ssmrk_sigs/covsym

# cp D*/ssmrk/*Perror_vx.jpg collection/ssmrk_sigs/errorvx/
# cp D*/ssmrk/*Perror_wz.jpg collection/ssmrk_sigs/errorwz/
# cp D*/ssmrk/*PEWMA.jpg collection/ssmrk_sigs/errorewma/
# cp D*/ssmrk/*covasym.jpg collection/ssmrk_sigs/covasym/
# cp D*/ssmrk/*covsym.jpg collection/ssmrk_sigs/covsym/
mkdir -p collection/ssmrk_sigs/powrad
mkdir -p collection/ssmrk_sigs/powrad_length
cp **/ssmrk/*powrad.jpg collection/ssmrk_sigs/powrad/
cp **/ssmrk/*powrad_*.jpg collection/ssmrk_sigs/powrad_length/

mkdir -p collection/ssmrk_sigs/gora_error
cp M*/ssmrk/*gora_Perror.jpg collection/ssmrk_sigs/gora_error/

mkdir -p collection/ssmrk_sigs/coeffssym
cp M*/ssmrk/*_m_sym.jpg collection/ssmrk_sigs/coeffssym/
cp M*/ssmrk/*_G_sym.jpg collection/ssmrk_sigs/coeffssym/
cp M*/ssmrk/*_B_sym.jpg collection/ssmrk_sigs/coeffssym/
mkdir -p collection/ssmrk_sigs/coeffs_gora
cp M*/ssmrk/*gora*_m.jpg collection/ssmrk_sigs/coeffs_gora/
cp M*/ssmrk/*gora*_G.jpg collection/ssmrk_sigs/coeffs_gora/
mkdir -p collection/ssmrk_sigs/coeffs_gora_asym
cp M*/ssmrk/*gora*_m_asym.jpg collection/ssmrk_sigs/coeffs_gora_asym/
cp M*/ssmrk/*gora*_G_asym.jpg collection/ssmrk_sigs/coeffs_gora_asym/
mkdir -p collection/ssmrk_sigs/coeffs_gora_sym
cp M*/ssmrk/*gora*_m_sym.jpg collection/ssmrk_sigs/coeffs_gora_sym/
mkdir -p collection/ssmrk_sigs/covcoeff
cp M*/ssmrk/*gora*_G_sym.jpg collection/ssmrk_sigs/coeffs_gora_sym/
cp M*/ssmrk/*_covariance-coefficients.jpg collection/ssmrk_sigs/covcoeff/


mkdir -p collection/ssmrk_sigs/pentzicr
cp **/pentzer2014icr/*_ICR_estimates.jpg collection/ssmrk_sigs/pentzicr/




mkdir -p collection/rotationnality/rho
mkdir -p collection/rotationnality/averho
cp **/rotationnality/*_rho.jpg collection/rotationnality/rho/
cp **/rotationnality/*_averho.jpg collection/rotationnality/averho/

mkdir -p collection/motors/eta
mkdir -p collection/motors/power
mkdir -p collection/motors/resistance
mkdir -p collection/motors/torque
cp **/elecmotmod/*_efficiency.jpg collection/motors/eta/
cp **/elecmotmod/*_motor-powers.jpg collection/motors/power/
cp **/elecmotmod/*_Rlosses.jpg collection/motors/resistance/
cp **/elecmotmod/*_torque.jpg collection/motors/torque/

# rm collection/ssmrk_sigs/coeffssym/*_comp_*_sym.jpg
